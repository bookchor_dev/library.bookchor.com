<?php 
	require 'vendor/autoload.php';
	require('library_classes/Connection/Connection.php');
	require('library_classes/Utility/Utility.php');
	require('library_classes/User/User.php');
	require('library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	if(isset($_REQUEST['isbn'])){
		$isbn = isset($_REQUEST['isbn'])?$_REQUEST['isbn']:0;
		$isbn = explode('/',$isbn)[0];
		$doc_info = [
		'query'=>$isbn,
		'type'=>'search',
		'doc_id'=>0,
		'limit'=>1
		];
		$product_details = ($product->booklist($user->user,$doc_info))['result'][0];
		$description = $product->product_description($product_details['product_id']);
		$user_book = $product->library_product_info($user->user['user_id'],$product_details['product_id']);
		$view = $product->product_view($user->user['user_id'],$product_details['product_id']);
		$div_info = [
		'column_id'=>2,
		'limit'=>10,
		];
		$recents = $product->book_div($user->user,$div_info);
		$div_info = [
		'column_id'=>3,
		'limit'=>10,
		];
		$likes = $product->book_div($user->user,$div_info);
		}else{
		header('Location: /');
	}
?>
<!DOCTYPE html>
<html>
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $product_details['title'];?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Montserrat:400' >
		<link rel="stylesheet" type="text/css" href="/assets/css/global.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" type="text/css" href="/assets/css/element.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/header.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/footer.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/OwlCarousel2/dist/assets/owl.carousel.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/OwlCarousel2/dist/assets/owl.theme.default.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/bookchor_icons/styles.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/book_detail.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/Rating/star.css?v=<?php echo $user->user['cache'];?>">
	</head>
	<body>
		<?php include "header.php";?>
		<section>
			<div class="container">
				<div class="overlay">
					<div class="loader"></div>
				</div>
				<div class="card test">
					<div class="card-body" style="padding:10px">
						<div class="grid">
							<div class="card-body">
								<img src="<?php echo $product_details['image'];?>" alt="<?php echo $product_details['title'];?>" class="img-fluid mx-auto d-block" style="height: 250px;">
							</div>
							<div class="card-body">
								<small class="text-muted float-left">Book Name</small>
								<small class="float-right right-content">
									<small>(<?php echo $product_details['reviews'];?> reviews)</small>
									<button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0">&nbsp;<?php echo $product_details['rating'];?>&nbsp;<span class="bc-bc-star star_icon"></button>
									</small>
									<br>
									<p class="font-weight-bold float-left" ><?php echo $product_details['title'];?></p>
									<div class="clearfix"></div>
									<small class="text-muted float-left">Author Name</small>
									<div class="clearfix"></div>
									<p class="font-weight-bold float-left" ><?php echo $product_details['author'];?></p>
									<?php if($user_book['wishlist']==1){?>
										<button data-pid="<?php echo $product_details['product_id'];?>" class="float-right btn add-to-wishlist btn-danger">Added to Wishlist&nbsp;&nbsp;<span><i class="fa fa-heart"></i></span></button>
										<?php }else{?>
										<button data-pid="<?php echo $product_details['product_id'];?>" class="float-right btn add-to-wishlist">Add to Wishlist&nbsp;&nbsp;<span><i class="fa fa-heart"></i></span></button>
									<?php }?>
									<div class="clearfix"></div>
									<small class="text-muted float-left">Publisher</small>
									<div class="clearfix"></div>
									<p class="font-weight-bold float-left" >Jaico Publishing House</p>
									<div class="clearfix"></div>
									<small class="text-muted float-left">Description</small>
									<div class="clearfix"></div>
									<p class="float-left">
										<?php 
										if(strlen($description)>150){
											echo substr($description,0,150);?>
										<span id="description" class="collapse"><?php echo substr($description,150);?></span>	
										<button class="btn btn-link text-danger" data-toggle="collapse" data-target="#description">View More</button>
										<?php 
										}else{
											echo $description;
										}?>
									</p>
									<br>
									<br>
									</div>
								</div>
							</div>
							<?php if($product_details['stock']){?>
								<div class="card book-availability">
									<div class="card-body " style="padding:15px !important">
										<p class="font-weight-bold">Book Availability</p>
										<div class="grid4" data-pid=<?php echo $product_details['product_id'];?>>
											<img src="/assets/images/loading.gif" class="img-fluid">
											<img src="/assets/images/loading.gif" class="img-fluid">
											<img src="/assets/images/loading.gif" class="img-fluid">
											<img src="/assets/images/loading.gif" class="img-fluid">
										</div>
									</div>
								</div>
								<div class="card bar-code">
									<div class="card-body">
									<input type="text" class="form-control barcode-input" id="usr" placeholder="Unique Bar-Code of the book">
								</div>
							</div>
						<?php } ?>
						<div class="card-body return-date">
							<div class="row mx-0">
								<?php if($product_details['stock']){?>
									<div class="col px-0">
										<button class="btn2 font-weight-bold issue p-2 "><a href="#">Issue Book</a></button>
									</div>
									<div class="col px-0">
										<button class="btn2 font-weight-bold find p-2"><a href="#">Find Book</a></button>
									</div>
									<?php }else{?>
									<div class="col px-0 py-2 text-center text-danger bg-light">
										This Book is Not available in your library
									</div>
									
								<?php }?>
								<?php if($product_details['ebook']){?> 
									<div class="col px-0">
										<button class="btn2 font-weight-bold p-2" bc-href="https://library.bookchor.com/web/viewer.html?file=/<?php echo $product_details['ebook'];?>"><a target="_blank" href="#" >View  E-Book</a></button>
									</div>
									<?php }else{?>
									<div class="col px-0 py-2 text-center text-danger bg-light">
										E-book Not Available 
									</div>
									
								<?php }?>
								<!--<div class="card-body three">
									<button class="btn2 font-weight-bold" type="button" data-toggle="modal" data-target="#exampleModalCenter"><a
									href="#">Recommend Book</a></button>
									<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
									aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
									<div class="modal-header">
									<h5 class="modal-title font-weight-bold" id="exampleModalLongTitle">Book</h5>
									</div>
									<div class="modal-body">
									<div class="form-group has-search">
									<input type="text" class="form-control" placeholder="Search for students..">
									</div>
									<div>
									<button class="student_profile_logo">K</button>
									</div>
									<p class="float-left font-weight-bold">Mayank Haldunia</p>
									<i class="bc-bc-tick-black float-right tick_sign"></i>
									</div>
									</div>
									</div>
									</div>
								</div>-->
							</div>
						</div>
					</div>
					<div class="card others">
						<div class="card-body" style="padding:10px">
							<p class="font-weight-bold float-left" >Others</p>
							<br>
							<div class="grid4">
								<div class="others-content" >
									<div class="grid7">
										<p class="text-muted float-left mr-3" >ISBN</p>
										<p class="font-weight-bold" ><?php echo $product_details['isbn'];?></p>
									</div>
								</div>
								<div class="others-content">
									<div class="grid7">
										<p class="text-muted float-left mr-3" >Pages</p>
										<p class="font-weight-bold"></p>
									</div>
								</div>
								<div class="others-content">
									<div class="grid7">
										<p class="text-muted float-left mr-3" >Cover</p>
										<p class="font-weight-bold" >Hard Cover</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="card">
						<div class="card-body">
							<div class="mb-3">
								<p class="font-weight-bold float-left">Reviews</p>
								<button class="btn1 float-right" type="button" data-toggle="modal" data-target="#reviewModal">Write
								Review</button>
							</div>
							<div class="card-body">
								<div class="" id="review-div">
									<center>No Review Available</center>
								</div>
							</div>
							<div class="modal fade" id="reviewModal">
								<div class="modal-dialog modal-dialog-centered">
									<div class="modal-content">
										<!-- Modal Header -->
										<div class="modal-header" >
											<h5 class="modal-title">Review This Book</h5>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<!-- Modal body -->
										<div class="modal-body">
											<section class='rating-widget'>
												
												<!-- Rating Stars Box -->
												<div class='rating-stars text-center'>
													<ul id='stars'>
														<li class='star' title='Poor' data-value='1'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='Fair' data-value='2'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='Good' data-value='3'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='Excellent' data-value='4'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='WOW!!!' data-value='5'>
															<i class='fa fa-star fa-fw'></i>
														</li>
													</ul>
												</div>
												
												<div class='success-box'>
													<div class='clearfix'></div>
													<img alt='tick image' width='32' src='data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA0MjYuNjY3IDQyNi42NjciIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQyNi42NjcgNDI2LjY2NzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+CjxwYXRoIHN0eWxlPSJmaWxsOiM2QUMyNTk7IiBkPSJNMjEzLjMzMywwQzk1LjUxOCwwLDAsOTUuNTE0LDAsMjEzLjMzM3M5NS41MTgsMjEzLjMzMywyMTMuMzMzLDIxMy4zMzMgIGMxMTcuODI4LDAsMjEzLjMzMy05NS41MTQsMjEzLjMzMy0yMTMuMzMzUzMzMS4xNTcsMCwyMTMuMzMzLDB6IE0xNzQuMTk5LDMyMi45MThsLTkzLjkzNS05My45MzFsMzEuMzA5LTMxLjMwOWw2Mi42MjYsNjIuNjIyICBsMTQwLjg5NC0xNDAuODk4bDMxLjMwOSwzMS4zMDlMMTc0LjE5OSwzMjIuOTE4eiIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K'/>
													<div class='text-message'></div>
													<div class='clearfix'></div>
												</div>
											</section>
											<br><br>
											<input type="text" name="review" class="write-review" maxlength="100" placeholder="Write Review">
											<span class="text-danger error-msg-review">No more than 100 Characters</span>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="button" class="btn font-weight-bold" data-dismiss="modal">Cancel</button>
											<button type="button"  class="btn font-weight-bold post-review" style="background:#F2A614" data-pid=<?php echo $product_details['product_id']?>>Post</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</section>
			<section class="slider-content" >
				<div class="container">
					<div class="card books-you-might-like">
						<div class="card-body books">
							<p class="font-weight-bold float-left" >Books you might like</p>
						</div>
					</div>
					<div id="owl-test" class="owl-carousel owl-theme">
						<?php foreach($likes as $like){?>
							<div class="item ">
								<a href="/book/<?php echo $like['details']['isbn'];?>/<?php echo str_replace(' ','-',$like['details']['title']);?>"><img src="<?php echo $like['details']['image'];?>" alt="<?php echo $like['details']['title'];?>" class="img-fluid " style="height:250px;"></a>
								<a href="/book/<?php echo $like['details']['isbn'];?>/<?php echo str_replace(' ','-',$like['details']['title']);?>"><p class="font-weight-bold book-title" title="<?php echo $like['details']['title'];?>"><?php echo substr($like['details']['title'],0,15).'..';?></p></a>
								<p class="text-muted " title="<?php echo $like['details']['author'];?>" >by <?php echo substr($like['details']['author'],0,15).'..';?></p>
								<div class="stars ">
									<?php for($i=0;$i<$like['details']['goodreads_avg_rating'];$i++){?>
										<i class="bc-bc-star star_icon"></i>
									<?php } ?>&nbsp;
									<small class="font-weight-bold">(<?php echo $like['details']['goodreads_total_reviews'];?>)</small>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</section>
			<section class="slider-content" >
				<div class="container">
					<div class="card books-you-might-like">
						<div class="card-body books">
							<p class="font-weight-bold float-left" >Recently Liked Books</p>
						</div>
					</div>
					<div id="owl-test" class="owl-carousel owl-theme">
						<?php foreach($recents as $recent){?>
							<div class="item ">
								<a href="/book/<?php echo $recent['details']['isbn'];?>/<?php echo str_replace(' ','-',$recent['details']['title']);?>"><img src="<?php echo $recent['details']['image'];?>" alt="<?php echo $recent['details']['title'];?>" class="img-fluid " style="height:250px;"></a>
								<a href="/book/<?php echo $recent['details']['isbn'];?>/<?php echo str_replace(' ','-',$recent['details']['title']);?>"><p class="font-weight-bold book-title" title="<?php echo $recent['details']['title'];?>"><?php echo substr($recent['details']['title'],0,15).'..';?></p></a>
								<p class="text-muted " title="<?php echo $recent['details']['author'];?>" >by <?php echo substr($recent['details']['author'],0,15).'..';?></p>
								<div class="stars ">
									<?php for($i=0;$i<$recent['details']['goodreads_avg_rating'];$i++){?>
										<i class="bc-bc-star star_icon"></i>
									<?php } ?>&nbsp;
									<small class="font-weight-bold">(<?php echo $recent['details']['goodreads_total_reviews'];?>)</small>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</section>
			<?php include "footer.php";?>
		</body>
		<script src="/assets/js/jquery.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/popper.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/OwlCarousel2/dist/owl.carousel.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/main.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/header.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/Rating/star.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/review.js?v=<?php echo $user->user['cache'];?>"></script>
	</html>
