<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../../library_classes/Connection/Connection.php');
require_once 'PHPExcel.php';
require_once 'PHPExcel/IOFactory.php';
$conn = new Connections();
$mysqli = $conn->main_db();
$filename = "birla open minds school.xlsx";
$type = PHPExcel_IOFactory::identify($filename);
$objReader = PHPExcel_IOFactory::createReader($type);
$objPHPExcel = $objReader->load($filename);
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
}
		$mm = urlencode('2<60%');
		$fq = '-mrp:0';
		$msg = '<table><tr><td>Title</td><td>Author</td><td>ISBN</td><td>MRP</td><td>Publisher</td></tr>';
		foreach($worksheets['children'] as $key=>$sheetinfo){
			if($key==0){
				continue;
			}
			$title= $sheetinfo[0];
			$pass = urlencode('Success^1218#');
			$json="http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/select?defType=dismax&fq=".$fq."&mm=".$mm."&indent=on&q=".$title."&rows=1&wt=json";
			$json_data = file_get_contents($json);
			$obj = json_decode($json_data,true);
			if($obj['response']['docs']){
				$content_array=$obj['response']['docs'][0];
				$title=$content_array['title'][0];
				$author=$content_array['author'][0];
				$mrp=$content_array['mrp'][0];
				$isbn=$content_array['isbn'][0];
				$pid = $content_array['id'];
				$query_pub = 'SELECT pub.name FROM product as p,publisher as pub WHERE p.publisher_id=pub.id AND p.id='.$pid;
				$stmt_pub = $mysqli->prepare($query_pub);
				$stmt_pub->execute();
				$row_pub = $stmt_pub->get_result();
				$result_pub = $row_pub->fetch_array();
				$publisher = $result_pub['name'];
				$msg .= '<tr><td>'.$title.'</td><td>'.$author.'</td><td>'.$isbn.'</td><td>'.$mrp.'</td><td>'.$publisher.'</td></tr>';
			}
		}
		$msg .= '</table>';
		$filename = 'BookList-Bookchor.xls';
		$file_handler = fopen ($filename,'w');
		fwrite ($file_handler,$msg);
		fclose ($file_handler);
?>