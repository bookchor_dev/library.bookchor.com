<?php
	require "../vendor/autoload.php";
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$library = new Library();
	$user = new User();
	$update_result=0;
	if(isset($_REQUEST['search'])){
		$result_details = $library->search_users($_REQUEST['search'],$user->user['library_id']);
	}
	if(isset($_REQUEST['update'])){
		unset($_REQUEST['update']);
		$user_data = $_REQUEST;
		/*  $name = $_REQUEST['name'];
			$father_name = $_REQUEST['father_name'];
			$mother_name = $_REQUEST['mother_name'];
			$class = $_REQUEST['class'];
			$contact = $_REQUEST['contact'];
			$email = $_REQUEST['email'];
			$gender = $_REQUEST['gender'];
			$dob = $_REQUEST['dob'];
			$address = $_REQUEST['address'];
			$username = $_REQUEST['username'];
		$user_data = ['name'=> $name,'class'=>$class,'email'=>$email,'gender'=>$gender,'dob'=>$dob,'father_name'=>$father_name,'mother_name'=>$mother_name,'address'=>$address,'contact'=>$contact,'username'=>$username]; */
	    $update_result = $user->update_user_details($user_data,1);
	}
?>

<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						<div class="panel panel-headline">
							<div class="panel-heading">
								<button type="button" class="btn btn-panel pull-right" uid="new_user">Add User</button>
								<h3 class="panel-title">Student Search</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form method="POST">
											<input type="text" class="form-control" name="search" placeholder="Student Code/Student Name/Student Email">
										</form>
									</div>
								</div>
							</div>
						</div>
						<?php if($update_result===1){?>
							<br><div class="alert alert-success"><strong>Update Successful!</strong></div>
						<?php }?>
						<?php 
							if(isset($_REQUEST['search'])){
								if(isset($result_details)){
									foreach($result_details as $details){?>
									<div class="col-md-4">
										<div class="panel panel-headline">
											<div class="panel-body">
												<div class="col-md-0 nopadding">
													<i class="fa fa-user" style="font-size:200px"></i>
												</div>
												<div class="col-md-12">
													<ul class="list-unstyled">
														<li><strong>Roll Number: </strong><?php echo $details['uid'];?></li>
														<li><strong>Name: </strong><?php echo $details['name'];?></li>
														<li><strong>Username: </strong><?php echo $details['username'];?></li>
														<?php echo isset($details['class'])?'<li><strong>Class: </strong>'.$details['class'].'</li>':'';?>
														<li><strong>Gender: </strong><?php echo $details['gender'];?></li>
														<?php echo isset($details['dob'])?'<li><strong>DOB: </strong>'.$details['dob'].'</li>':'';?>
														
														<li>
															<button type="button" class="btn btn-info" uid="<?php echo $details['uid']?>">Edit</button>
															<button type="button" class="btn btn-info issue-history" user_id="<?php echo $details['_id'];?>">Issue History</button>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								<?php }}else{ ?>
								<h3>No Result Found</h3>
							<?php }} ?>
							<!-- END OVERVIEW -->
							
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
			<!--Issue History Modal-->
			<div id="issueModal" class="modal fade" role="dialog">
				<div class="modal-dialog" style="width:1000px;">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title"><strong>Issue History</strong></h4>
						</div>
						<div class="modal-body" id="issue-history-content">
						</div>
						<div class="modal-footer">
							
						</div>
					</div>
				</div>
			</div>
			<div id="editModal" class="modal fade" role="dialog">
				
				<div class="modal-dialog" style="width:85vw">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title"><strong>Add New User</h4>
							</div>
							<div class="modal-body row">
								<form method="POST" id="user_form">
									<div class="col-sm-4">
										<label>Name: </label><input type="text" class="form-control" placeholder="Name" name="name" required>
										
										<label>Father's Name: </label><input type="text" class="form-control" placeholder="Father's Name" name="father_name">
										<label>Mother's Name: </label><input type="text" class="form-control" placeholder="Mother's Name" name="mother_name">
										
										<label>Gender: </label>
										<select class="form-control" name="gender" required>
											<option value="M">Male</option>
											<option value="F">Female</option>
											<option value="O">Other</option>
										</select>
										
										<label>Date Of Birth: </label><input type="date" class="form-control" placeholder="Date Of Birth" name="dob" required>
										<label>Contact: </label>
										<div class="input-group">
											<input type="text" class="form-control" name="contact[]" placeholder="Contact">
											<div class="input-group-btn" >
												<button class="btn btn-default add_input" type="button">
													<i class="fa fa-plus"></i>
												</button>
											</div>
										</div>	
										
									</div>
									<div class="col-sm-4">
										<label>User Type: </label>
										<select class="form-control" name="user_type_id" required>
											<option value="1">Student</option>
											<option value="3">Teacher</option>
										</select>
										<label>Username: </label><input type="text" class="form-control" placeholder="Username" disabled>
										<label>UID: </label><input type="text" class="form-control" placeholder="User ID" name="uid">
										<label>Class: </label><input type="text" class="form-control" placeholder="Class" name="class">
										<label>Address: </label>
										<input type="text" class="form-control" name="address" placeholder="Address">
										<label>Email: </label>
										<div class="input-group">
											<input type="text" class="form-control" name="email[]" placeholder="Email">
											<div class="input-group-btn">
												<button class="btn btn-default add_input" type="button">
													<i class="fa fa-plus"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="col-sm-3 text-center">
										<div class="well">
											<img src="" id="img-src-fp" width="145px" height="188px" alt="Finger Print">
											<input type="hidden" name="fp_hash" id="fpIsoTemplate">
										</div>
										<button type="button" onclick="Capture();" class="btn btn-info">Capture FP</button>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button class="btn btn-info" type="submit" form="user_form" name="update">Add User</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END WRAPPER -->
			<!-- Javascript -->
			<script>
				var json_user = new Array();
				<?php 
					if(isset($result_details)){
						foreach($result_details as $details){?>
							json_user['<?php echo $details['uid'];?>'] = <?php echo json_encode($details);?>;
					<?php }}?>
			</script>
			<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>?v=2"></script>
			<script src="assets/scripts/search_user.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="../assets/js/mfs100-9.0.2.6.js"></script>
			
		</body>
		
	</html>
