<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/User/Login.php');
	$user = new User();
	$login = new Login();
	$login->logout($user->user['token']);
?>