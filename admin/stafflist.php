<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/Library/Library.php');
	$user = new Library();
	$date = date('Y-m-d');
	$library_id = new MongoDB\BSON\ObjectId('5af409d76577193010d3f0c9');
	require_once('Classes/PHPExcel.php');
	$filenames = ["stafflist.xlsx"];
	foreach($filenames as $filename){
		$type = PHPExcel_IOFactory::identify($filename);
		$objReader = PHPExcel_IOFactory::createReader($type);
		$objPHPExcel = $objReader->load($filename);
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			$worksheets[$worksheet->getTitle()] = $worksheet->toArray();
		}
		unset($worksheets['Academics'][0]);
		unset($worksheets['Academics'][1]);
		unset($worksheets['Academics'][2]);
		//print_r($worksheets);die;
		foreach($worksheets['Academics'] as $stock){
			if(strpos($stock[5],'/')){
				$contact = explode('/',$stock[5]);
			}else{
				$contact = explode(',',$stock[5]);
			}
			$name_arr = explode(' ',$stock[2]);
			if(strlen($name_arr[0])<3){
				unset($name_arr[0]);
			}
			$name = implode(' ',$name_arr);
			$username = $name_arr[0].'_'.$stock[1];
			$user_data = [
				'username'=>$username,
				'emp_id'=>$stock[1],
				'uid'=>$stock[1],
				'name'=>$name,
				'department'=>$stock[3],
				'gender'=>$stock[4],
				'contact'=>$contact,
				'email'=>explode(',',$stock[6]),
				'password'=>$stock[1],
				'library_id'=>$library_id,
				'user_type_id'=>3,
				'status'=>1
			];
			$user->users_api($user_data);
		}
	}
?>	