<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	if(isset($_REQUEST['last'])){
		$count = $_REQUEST['last'];
		$barcodes = $library->barcode_reprint($user->user['library_id'],$count);
	}else if(isset($_REQUEST['barcode'])){
		$productlists = $library->search_out_book($_REQUEST['barcode'],$user->user['library_id'],1)[0];
		$barcode['sku'] = $productlists['stock']['sku'];
		$barcode['barcode'] = $productlists['stock']['barcode'];
		$barcode['isbn'] = $productlists['product']['isbn'];
		$barcodes[] = $barcode;
	}else{
		$barcodes = $library->barcode_print($user->user['library_id']);
	}
	if(!$barcodes){
			header('Location: /admin/books_in');
	}
	$i = 0;
?>
<style type="text/css">
.container
{
    vertical-align: middle;
    text-align: center;
    vertical-align: middle;
    margin-top: 1.5%;
    width: 100%;
    display: inline-block;
}   
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
	width:30%
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}
</style>
<html>
	<script>
		

    function reloadImage(pThis) {
        // To prevent this from being executed over and over
        setTimeout(function() {
			//your code to be executed after 1 second
			//pThis.onerror = null; 
     
        // Refresh the src attribute, which should make the
        // browsers reload the iamge.
        pThis.src = pThis.src;
		}, 1000);
		
    }


	</script>
	 <body> 
		<div class="no-print" style="text-align:center;">
			Press Done only After Printing<br>
			<button class="button button1" onclick="print_flag();" id="btndone">Done</button>
		</div>
		<?php while($i<count($barcodes)){
				$bar_arr[] = "'".$barcodes[$i]['barcode']."'";
		?>
		<div class="container">
			<div style="width:50%;float:left;">
				<span style="font-size:10px">
					<?php echo $barcodes[$i]['isbn'];?><br>
				</span>
				<img src="barcode.php?text=<?php echo $barcodes[$i]['barcode'];?>" onerror="reloadImage(this);" style="height:0.5in;width:1.9in" />
				<span style="font-size:10px">
					<br><?php echo $barcodes[$i]['barcode'];?>-<?php echo $barcodes[$i]['sku'];?>
				</span>
			</div>
			<?php $i++;	
			if(isset($barcodes[$i])){
			$bar_arr[] = "'".$barcodes[$i]['barcode']."'";	
			?>
			<div style="width:50%;float:left;">
				<span style="font-size:10px">
					<?php echo $barcodes[$i]['isbn'];?><br>
				</span>
				<img src="barcode.php?text=<?php echo $barcodes[$i]['barcode'];?>" onerror="reloadImage(this);" style="height:0.5in;width:1.9in" />
				<span style="font-size:10px">
					<br><?php echo $barcodes[$i]['barcode'];?>-<?php echo $barcodes[$i]['sku'];?>
				</span>
			</div>
			<?php }?>
		</div>
		<?php $i++;} ?>
	</body>
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script>
		window.print();
		function print_flag(){
			document.getElementById('btndone').innerHTML = '<img src="/admin/assets/img/loader.gif" width="50px">';
			var booklists = [<?php echo implode(', ',$bar_arr);?>];
			$.post('/admin/assets/ajax/print_flag.php',{booklists:booklists}, function(data){
				//window.location.href="/admin/books_in";
			});
		}
	</script>
	</html>
