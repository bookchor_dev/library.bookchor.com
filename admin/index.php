<?php 
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$product = new Product();
	$library = new Library();
	$page_id = '5b18255bebaeee137c70244d';
	$library_stats = $library->library_stats($user->user['library_id']);
	foreach($library_stats['graph'] as $library_stat){
			$dates[] = "'".date('d-M',$library_stat['_id']*24*60*60)."'";
			$issue_count[] = $library_stat['issue_count'];
	}
	$user_info = [
		'user_id' =>$user->user['user_id'],
		'library_id'=>$user->user['library_id']
	];
	$doc_info = [
		'column_id'=>4,
		'limit'=>5,
	];
	$list['title'] = 'Recently Issued';
	$list['product_data'] = $product->book_div($user_info,$doc_info);
	$lists[] = $list;
	$doc_info = [
		'column_id'=>7,
		'limit'=>5,
	];
	$list['title'] = 'Mostly Issued';
	$list['product_data'] = $product->book_div($user_info,$doc_info);
	$lists[] = $list;
	
?>
<!doctype html>
<html lang="en">

<head>
	<title>Dashboard | Library Admin</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
	<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include 'header.php';?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include 'sidebar.php';?>
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Overview</h3>
							<!--<p class="panel-subtitle">Period: May 14, 2018 - May 21, 2018</p>-->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-download"></i></span>
										<p>
											<span class="number"><?php echo isset($library_stats['books_issued'])?$library_stats['books_issued']:0;?></span>
											<span class="title">Books Issued</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-eye"></i></span>
										<p>
											<span class="number"><?php echo isset($library_stats['books_viewed'])?$library_stats['books_viewed']:0;?></span>
											<span class="title">Book Views</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span class="number">₹ <?php echo $library_stats['fine_due'];?></span>
											<span class="title">Fine Due</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-shopping-bag"></i></span>
										<p>
											<span class="number">₹ <?php echo $library_stats['fine_collected'];?></span>
											<span class="title">Fine Collected</span>
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="visits-chart" class="ct-chart"></div>
								</div>
								<div class="summary-custom" style="">
									<div class="weekly-summary text-right">
										<span class="number"><?php echo $library_stats['total_books'];?></span>
										<span class="info-label">Total Books</span>
									</div>
									<div class="weekly-summary text-right">
										<span class="number"><?php echo $library_stats['total_titles'];?></span> <!--<span class="percentage"><i class="fa fa-caret-up text-success"></i> 23%</span>-->
										<span class="info-label">Total Titles</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
					<div class="row">
						<?php foreach($lists as $list){?>
						<div class="col-md-6">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php echo $list['title'];?></h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Image</th>
												<th>ISBN</th>
												<th>Name</th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($list['product_data'] as $product_data){?>
											<tr>
												<td><img src="<?php echo $product_data['details']['image'];?>" height="100px"></td>
												<td><a href="#"><?php echo $product_data['details']['isbn'];?></a></td>
												<td><?php echo $product_data['details']['title'];?></td>
											</tr>
										<?php }?>	
										</tbody>
									</table>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 5 Issues</span></div>
										<div class="col-md-6 text-right"><a href="/admin/issue_logs" class="btn btn-primary">View All Issues</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<?php include 'footer.php';?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
	<script>
	$(function() {
		var data, options;

		
		// visits chart
		data = {
			labels: [<?php echo implode(', ',$dates);?>],
			series: [
				[<?php echo implode(', ',$issue_count);?>]
			]
		};

		options = {
			height: 300,
			width: 1000,	
			axisX: {
				showGrid: false
			},
		};
		new Chartist.Bar('#visits-chart', data, options);
	});
	</script>
</body>

</html>
