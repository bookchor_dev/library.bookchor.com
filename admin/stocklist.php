<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	$doc_id = 0;
	$limit = 0;
	$datalist = $library->all_books_data($user->user['library_id'],$doc_id,$limit);
	$productlists = $datalist['product'];
	/* $text = '<table><thead><tr><td>ISBN</td><td>Title</td><td>Author</td><td>Barcode</td><td>SKU</td><td>Condition</td><td>MRP</td></tr></thead><tbody>';
	foreach($productlists as $productlist){
		$text .= '<tr><td>'.$productlist["isbn"].'</td><td>'.$productlist["title"].'</td><td>'.$productlist["author"].'</td><td>'.$productlist["barcode"].'</td><td>'.$productlist["sku"].'</td><td>'.$productlist["condition"].'</td><td>'.$productlist["mrp"].'</td></tr>';
	}
	$text .= '</tbody></table>';
	$file = fopen("allbooksexcel.xls","w");
	fwrite($file,$text);
	fclose($file);
	//print_r($productlists);
	die; */
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
		
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						<table class="table" id="myTable">
							<thead>
								<tr>
									<th>Title</th>
									<th>Author</th>
									<th>ISBN</th>
									<th>MRP</th>
									<th>Barcode</th>
									<th>SKU</th>
									<th>Issued</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($productlists as $productlist){?>
								<tr>
									<td><?php echo $productlist['title'];?></td>
									<td><?php echo $productlist['author'];?></td>
									<td><?php echo $productlist['isbn'];?></td>
									<td>₹<?php echo $productlist['mrp'];?></td>
									<td><?php echo $productlist['barcode'];?></td>
									<td><?php echo $productlist['sku'];?></td>
									<td><?php echo ($productlist['out']==1)?'Yes':'No';?></td>
								</tr>
								<?php }?>
							</tbody>
						</table>
						<!-- END OVERVIEW -->
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>"></script>
		<script>
			$(document).ready( function () {
				$('#myTable').DataTable();
			} );
		</script>
	</body>
	
</html>
