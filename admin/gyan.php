<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/Library/Library.php');
	$user = new Library();
	$file = fopen("credentials.csv","w");
	$date = date('Y-m-d');
	$library_id = new MongoDB\BSON\ObjectId('5ba20c745f7aa506f801e637');
	require_once('Classes/PHPExcel.php');
	$filenames = ["only student details.xlsx"];
	foreach($filenames as $filename){
		$type = PHPExcel_IOFactory::identify($filename);
		$objReader = PHPExcel_IOFactory::createReader($type);
		$objPHPExcel = $objReader->load($filename);
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			$worksheets[$worksheet->getTitle()] = $worksheet->toArray();
		}
		unset($worksheets['Sheet1'][0]);
		unset($worksheets['Sheet1'][1]);
		foreach($worksheets['Sheet1'] as $stock){
			$contact = explode(PHP_EOL,$stock[5]);
			$email = explode(PHP_EOL,$stock[6]);
			$roll_no = sprintf("%04d",$stock[0]);
			$password = str_replace('/','',$stock[4]);
			$password = str_replace('-','',$password);
			$username = explode(' ',$stock[2])[0].'_'.$roll_no;
			$user_data = [
				'username'=>$username,
				'class'=>$stock[1],
				'regd_no'=>$stock[3],
				'uid'=>$roll_no,
				'roll_no'=>$roll_no,
				'name'=>$stock[2],
				'father_name'=>$stock[7],
				'mother_name'=>$stock[8],
				'address'=>$stock[10],
				'gender'=>'',
				'dob'=>$stock[4],
				'contact'=>	$contact,
				'email'=>$email,
				'blood_group'=>$stock[11],	
				'password'=>$password,
				'library_id'=>$library_id,
				'user_type_id'=>1,
				'status'=>1
			];
			$user_response = $user->users_api($user_data);
			print_r($user_response);
			echo '<br>';
			fputcsv($file,$user_response);
		}
		fclose($file);
	}
?>	