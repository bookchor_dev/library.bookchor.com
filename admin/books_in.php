<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	$barcodelists = $library->barcode_print($user->user['library_id']);
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						<div class="panel panel-headline">
							<div class="panel-heading">
								<h3 class="panel-title">Books In</h3>
							</div>
							<div class="panel-body">
								
								<div class="row book_in_content">
									<div class="col-md-5">
											<input type="number" class="form-control books_in_isbn books_in" placeholder="ISBN">
									</div>
									<div class="col-md-5">
											<input type="text" class="form-control books_in_sku books_in" placeholder="SKU">
									</div>
									<div class="col-md-2">
											<select class="form-control books_in_cond">
												<option value="New">New</option>
												<option value="Old">Old</option>
												<option value="Donated">Donated</option>
											</select>
									</div>
								</div>
							</div>
						</div>
						
								<div class="panel panel-headline book_in_content">
									<div class="panel-body">
										<table class="table table-striped">
											<thead>
												<tr>
													<td>Barcode</td>
													<td>ISBN/Product ID</td>
													<td>SKU</td>
													<td>Condition</td>
													<td>Action</td>
												</tr>
											</thead>
											<tbody id="book_in_data">
												<?php 
													if(isset($barcodelists)){
														foreach($barcodelists as $barcodelist){
														?>
															<tr id="BC<?php echo $barcodelist['barcode'];?>">
																<td><?php echo 'BC'.$barcodelist['barcode'];?></td>
																<td><?php echo $barcodelist['isbn'];?></td>
																<td><?php echo $barcodelist['sku'];?></td>
																<td><?php echo $barcodelist['condition'];?></td>
																<td>
																	<button type="button" class="btn btn-danger book_out" barcode="<?php echo $barcodelist['barcode'];?>"><i class="fa fa-trash-o"></i></button>
																</td>
															</tr>
												<?php }} ?>
											</tbody>
										</table>
										
									</div>
								</div>
								<div class="panel">
									<div class="panel-body">
										<center>
											<div class="col-md-6">
											<form action="/admin/print_barcode">
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="No of Barcodes" name="last">
											</div>
											<div class="col-md-6">
												<button class="btn btn-warning"><i class="fa fa-print"> Reprint Barcodes</i></button>
											</div>
											</form>
											</div>
											<div class="col-md-6">
											<a class="btn btn-info" href="/admin/print_barcode"><i class="fa fa-print"> Print</i></a>	
											</div>
										</center>
									</div>
								</div
						<!-- END OVERVIEW -->
						
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script>
			var in_count =  <?php echo count($barcodelists);?>;
		</script>
		<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/books_in.js?v=<?php echo $user->user['cache'];?>"></script>
		
	</body>
	
</html>
