<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	$alert = '';
	$limit = 10;
	if(isset($_REQUEST['barcode'])){
		$doc_id = isset($_REQUEST['doc_id'])?$_REQUEST['doc_id']:0;
		$barcode = $_REQUEST['barcode'];
		$barcode_info = $library->search_out_book($barcode,$user->user['library_id'],1)[0];
		$productlists = $library->barcode_history($barcode,$user->user['library_id'],$doc_id,$limit);
		//print_r($barcode_info);die;
	}
	
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
		
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<?php if($alert!=''){?>
							<div class="alert alert-info">
								<?php echo $alert;?>
							</div>
						<?php }?>
						<div class="panel panel-headline">
							<div class="panel-heading">
								<h3 class="panel-title">Search Barcode History</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form method="POST">
											<div class="input-group">
												<input class="form-control" name="barcode" placeholder="Barcode" type="text">
												<div class="input-group-btn">
													<button class="btn btn-default" type="submit">
														<i class="glyphicon glyphicon-search"></i>
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<?php if(isset($barcode_info)){?>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<div class="panel panel-headline">
								<div class="panel-body">
									<div class="col-xs-4 nopadding">
										<img src="<?php echo $barcode_info['product']['image'];?>" class="img-responsive" style="height:250px;">
									</div>
									<div class="col-xs-8">
										<p><?php echo substr($barcode_info['product']['title'],0,30).'...';?>
										<br>by <?php echo substr($barcode_info['product']['author'],0,30).'...';?></p>
										<strong>ISBN: </strong><?php echo $barcode_info['product']['isbn'];?>
										<br>
										<strong>SKU: </strong><?php echo $barcode_info['stock']['sku'];?>
										<br>
										<strong>Barcode: </strong><?php echo $barcode_info['stock']['barcode'];?>
										<br>
										<?php if(isset($productlist['user'])){?>
											<strong>Issued To: </strong><?php echo $barcode_info['user']['name'];?>
											<br>
											<strong>User ID: </strong><?php echo $barcode_info['user']['uid'];?>
										<?php } ?>
										<div class="panel-footer nopadding">
												<a class="btn btn-info col-xs-12" href="/admin/print_barcode.php?barcode=<?php echo $barcode_info['stock']['barcode'];?>">Reprint Barcode</a>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<?php } ?>
						<!-- OVERVIEW -->
						<?php if(isset($productlists)&&$productlists){?>
							
							<table class="table" id="myTable">
								<thead>
									<tr>
										<th>User</th>
										<th>User ID</th>
										<th>Date Issued</th>
										<th>Date Returned</th>
										<th>Fine</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($productlists as $productlist){?>
										<tr>
											<td><?php echo $productlist['name'];?></td>
											<td><?php echo $productlist['uid'];?></td>
											<td><?php echo date('d-M-Y',$productlist['issue']['date_issued']);?></td>
											<td><?php echo isset($productlist['issue']['date_returned'])?date('d-M-Y',$productlist['issue']['date_returned']):"N/A";?></td>
											<td><?php echo isset($productlist['issue']['fine'][0]['amount'])?$productlist['issue']['fine'][0]['amount']:'N/A';?></td>
										</tr>
									<?php }?>
								</tbody>
							</table>
							<!-- END OVERVIEW -->
							<center>
								<?php if(count($productlists)>=$limit){?>
									<form>
										<?php if($doc_id!=0){?>
											<button class="btn btn-warning" onclick="history.go(-1);">Previous</button>
										<?php }?>	
										<button class="btn btn-info" name="doc_id" value="<?php echo $doc_id + $limit;?>">Next</button>
										<form>
										<?php } ?>
									</center>
								<?php }?>
							</div>
						</div>
					</div>
					<!-- END MAIN CONTENT -->
					<!-- END MAIN -->
					<div class="clearfix"></div>
					<?php include 'footer.php';?>
				</div>
				<!-- END WRAPPER -->
				<!-- Javascript -->
				<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
				<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
				<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
				<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
				<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
				<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
				<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>"></script>
			</body>
			
		</html>
		