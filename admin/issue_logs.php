<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	$issue_logs = $library->issue_logs($user->user['library_id']);
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css?v=<?php echo $user->user['cache'];?>">
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						
							<div class="col-md-12">
								<div class="panel panel-headline">
									<div class="panel-heading">
										<h3 class="panel-title">Recently Issued</h3>
									</div>
									<div class="panel-body">
										<table class="table" id="myTable">
											<thead>
												<tr>
													<td>ID</td>
													<td>Name</td>
													<td>ISBN</td>
													<td>Book Title</td>
													<td>Date Issued</td>
													<td>Status</td>
													<td>Date Returned</td>
												</tr>
											<thead>
											<tbody>
												<?php foreach($issue_logs as $issue_log){
														if($issue_log['status']==1){
															$status = 'Issued';
														}else if($issue_log['status']==2){
															$status = 'Returned';
														}else{
															$status = 'N/A';
														}
												?>
												<tr>
													<td><?php echo $issue_log['uid'];?></td>
													<td><?php echo $issue_log['name'];?></td>
													<td><?php echo $issue_log['isbn'];?></td>
													<td><?php echo $issue_log['title'];?></td>
													<td><?php echo date('d-M-Y',$issue_log['date_issued']);?></td>
													<td><?php echo $status;?></td>
													<td><?php echo isset($issue_log['date_returned'])?date('d-M-Y',$issue_log['date_returned']):'N/A';?></td>
												</tr>
												<?php } ?>
											<tbody>
										</table>
									</div>
								</div>
							</div>
						<!-- END OVERVIEW -->
						
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
			
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>"></script>
		<script>
			$(document).ready( function () {
				$('#myTable').DataTable();
			} );
		</script>
	</body>
	
</html>
