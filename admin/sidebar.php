<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="/admin/" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<!--<li><a href="/admin/search_book" class=""><i class="fa fa-search"></i> <span>Search Book</span></a></li>-->
						<li><a href="/admin/book_return" class=""><i class="lnr lnr-code"></i> <span>Issue/Return Book</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Manage Inventory</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="/admin/stocklist" class="">All Books</a></li>
									<li><a href="/admin/books_in" class="">Books In</a></li>
									<li><a href="/admin/books_out" class="">Books Out</a></li>
									<li><a href="/admin/pending_books" class="">Pending Books</a></li>
								</ul>
							</div>
						</li>
						<li><a href="/admin/issue_logs" class=""><i class="lnr lnr-chart-bars"></i> <span>Books Issued</span></a></li>
						<li><a href="/admin/defaulters" class=""><i class="lnr lnr-chart-bars"></i> <span>Defaulters</span></a></li>
						<li><a href="/admin/search_users" class=""><i class="fa fa-users"></i> <span>Search Users</span></a></li>
						<li><a href="/admin/barcode_history" class=""><i class="fa fa-barcode"></i> <span>Barcodes</span></a></li>
						<li><a href="/admin/settings" class=""><i class="lnr lnr-dice"></i> <span>Settings</span></a></li>
						<li><a href="/admin/logout" class=""><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
					</ul>
				</nav>
			</div>
		</div>