<?php 

	$update_result = 0;
	if(isset($_REQUEST['update'])){
		$target_dir = "tempisbn/";
		$target_file = $target_dir . basename($_FILES["file_pdf"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if file already exists
		if (file_exists($target_file)) {
			echo "Sorry, file already exists.";
			$uploadOk = 0;
		}
		// Check file size
		/* if ($_FILES["file_pdf"]["size"] > 500000) {
			echo "Sorry, your file is too large.";
			$uploadOk = 0;
		} */
		// Allow certain file formats
		if($imageFileType != "pdf") {
			echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["file_pdf"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["file_pdf"]["name"]). " has been uploaded.";
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}
	}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content=""/>
<!-- Document Title -->
<title>Library @ Bookchor</title>
<!-- StyleSheets -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>



<!-- Wrapper -->
<div class="wrapper push-wrapper">

	<!-- Header -->
	<!-- Header -->

		<!-- MAIN -->
			<div class="main" style="margin-bottom: 10px; margin-top: 20px">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="panel panel-profile">
						<div class="clearfix">
							<!-- LEFT COLUMN -->
						  <div class="row">
							<div class="col-md-2 col-md-offset-0"></div>
							<div class="col-md-8">
								<!-- PROFILE DETAIL -->
								<div class="profile-detail">
									<div class="profile-info">
										<?php if($update_result === 1){?>
											<div class="alert alert-success"><strong>Update Successful</strong></div>
											<?php $update_result = 0;?>
										<?php }?>
										<form method="POST" enctype="multipart/form-data">
										<h4 class="heading">Submit Ebooks</h4>
										<ul class="list-unstyled list-justify">
											<li><label>ISBN: </label><input type="text" class="form-control" placeholder="ISBN" name="isbn"></li>
											<li><label>Upload PDF: </label><input type="file" class="form-control" placeholder="PDF" name="file_pdf"></li>
											<li><button class="btn btn-info" name="update" style="margin-top: 10px">Submit</button></li>
										</ul>
										</form>
										
									</div>
									<br>
								</div>
								<!-- END PROFILE DETAIL -->
							</div>
							<!-- END LEFT COLUMN -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>

</div>
<!-- Wrapper -->


<!-- Java Script -->
<script src="assets/vendor/jquery/jquery.js"></script>       		

</body>
</html>