<?php
	require "../vendor/autoload.php";
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	$results = $library->defaulters($user->user['library_id']);
	
?>
<!doctype html>
<html lang="en">
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" type="text/css" href="assets/css/month_search.css?v=<?php echo $user->user['cache'];?>">
	</head>
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						
						<div class="col-md-12">
							<div class="panel panel-headline">
								<div class="panel-heading">
									<h3 class="panel-title">Defaulters</h3>
								</div>
								<div class= "panel-body">
									<label id="month">Month:</label>
										<select id="mon" class="form-control" onchange="selectmonth()">
											<option value="none">None</option>
											<option value="Jan">January</option>
											<option value="Feb">Febraury</option>
											<option value="Mar">March</option>
											<option value="Apr">April</option>
											<option value="May">May</option>
											<option value="Jun">June</option>
											<option value="Jul">July</option>
											<option value="Aug">August</option>
											<option value="Sep">September</option>
											<option value="Oct">October</option>
											<option value="Nov">November</option>
											<option value="Dec">December</option>
										</select>
									<br><br>	
									<table class="table" id="myTable">
										
										<thead>
											<tr>
												<td>Student Name</td>
												<td>Student ID</td>
												<td>Book Title</td>
												<td>Barcode</td>
												<td>Issued Date</td>
												<td>Fine Amount</td>
												<td>Status</td>
												<td>Returned Date</td>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach ($results as $result) {
												if($result['status']==1){
													$status = 'Issued';
												}else{
													$status = 'Returned';
												}
												?>
												<tr>
													<td><?php echo $result['name'];?></td>
													<td><?php echo $result['uid'];?></td>
													<td><?php echo $result['title'];?></td>
													<td><?php echo $result['barcode'];?></td>
													<td><?php echo date('Y-M-d H:i:s',$result['date_issued']);?></td>
													<td><?php echo $result['fine'];?></td>
													<td><?php echo $status;?></td>
													<td><?php echo isset($result['date_returned'])?date('Y-M-d H:i:s',$result['date_returned']):'';?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- END OVERVIEW -->
						
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
			
		</div>
		<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/month_search.js?v=<?php echo $user->user['cache'];?>"></script>
		<script>
			$(document).ready( function () {
				$('#myTable').DataTable();
			} );
			
			
			
		</script>
	</body>
</html>