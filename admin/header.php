<nav class="navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="https://www.bookchor.com/assets/frontend/layout/img/logos/book-chor-logo-256.png" style="height: 22px;" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div class="navbar-text">
				<strong><?php echo $user->user['school'];?></strong>
				</div>
				<div id="navbar-menu">
					<ul class="nav  navbar-right">
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i></a>
							<ul class="dropdown-menu">
								<li><a href="/admin/book_return" class=""><i class="lnr lnr-code"></i> <span>Issue/Return Book</span></a></li>
								<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Books In</span></a></li>
								<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Books Out</span></a></li>
								<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
								<li><a href="/admin/logout"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>