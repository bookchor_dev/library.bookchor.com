<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	if(isset($_REQUEST['search'])){
		$search = $_REQUEST['search'];
		$productlists = $library->search_out_book($search,$user->library_id,1);
	}else if(isset($_REQUEST['books_out'])){
		$barcode = $_REQUEST['books_out'];
		$library->books_out($barcode,$user->library_id);
	}
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						<div class="panel panel-headline">
							<div class="panel-heading">
								<h3 class="panel-title">Books Out</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form method="POST">
										<div class="input-group">
											<input type="text" class="form-control" name="search" placeholder="ISBN/Barcode">
											<div class="input-group-btn">
													<button class="btn btn-default" type="submit">
														<i class="glyphicon glyphicon-search"></i>
													</button>
											</div>
										</div>	
										</form>
									</div>
								</div>
							</div>
						</div>
						<?php 
						if(isset($_REQUEST['search'])){
						if(isset($productlists)){
						foreach($productlists as $productlist){?>
							<div class="col-md-4">
								<div class="panel panel-headline">
									<div class="panel-body">
										<div class="col-md-4 nopadding">
											<img src="<?php echo $productlist['product']['image'];?>" height="150px">
										</div>
										<div class="col-md-offset-1 col-md-7">
											<p><?php echo $productlist['product']['title'];?>
											<?php if(isset($productlist['product']['author'])){?>
												<br>by <?php echo $productlist['product']['author'];?></p>
											<?php }?>
											<br>
											<strong>ISBN: </strong><?php echo $productlist['product']['isbn'];?>
											<br>
											<strong>SKU: </strong><?php echo $productlist['product']['stock']['sku'];?>
											<br>
											<strong>Barcode: </strong><?php echo $productlist['product']['stock']['barcode'];?>
											<br>
											<?php if(isset($productlist['user'])){?>
												<strong>Issued To: </strong><?php echo $productlist['user']['name'];?>
												<br>
												<strong>User ID: </strong><?php echo isset($productlist['user']['code'])?$productlist['user']['code']:0;?>
											<?php }else{?>
												<br>
												<br>
												<form method="POST">
													<button class="btn btn-info" name="books_out" value="<?php echo $productlist['product']['stock']['barcode'];?>">Book Out</button>
												</form>
											<?php }?>
										</div>
									</div>
								</div>
							</div>
						<?php }}else{ ?>
							<h3>No Result Found</h3>
						<?php }} ?>
						<!-- END OVERVIEW -->
						
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
			<div id="issueModal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" id="isbntitle"></h4>
						</div>
						<div class="modal-body">
							<input class="form-control" placeholder="Search User" type="text" id="user_input">
							<div class="">
							<form method="POST">
								<input type="hidden" name="issue[barcode]" id="barcode_input" value="">
								<div id="issue_content">
								
								</div>
							</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>"></script>
		
	</body>
	
</html>
