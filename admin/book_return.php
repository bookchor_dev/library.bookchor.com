<?php
	//error_reporting(0);
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	require('../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	$alert = '';
	if(isset($_REQUEST['search'])){
		$search = $_REQUEST['search'];
		$productlists = $library->search_out_book($search,$user->user['library_id'],$_REQUEST['search_type']);
		if(!(isset($productlists)&&$productlists)){
			$alert = 'No Result Found';
		}
		//print_r($productlists);die;
	}else if(isset($_REQUEST['issue'])){
		$alert = 'Book Issued';
		$library->issue_book($_REQUEST['issue'],$user->user['library_id']);
	}else if(isset($_REQUEST['return'])){
		$return = $_REQUEST['return'];
		if(isset($return['barcode'])&&$return['barcode']){
			$barcode = $return['barcode'];
			$alert = 'Book Returned';	
			//print_r($_REQUEST);die;
			$return['fine']['fine_type'] = new MongoDB\BSON\ObjectId($return['fine']['fine_type']);
			$library->return_book($barcode,$user->user['library_id'],$return['fine']);
		}
	}
	
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css?v=<?php echo $user->user['cache'];?>">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css?v=<?php echo $user->user['cache'];?>">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css?v=<?php echo $user->user['cache'];?>">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css?v=<?php echo $user->user['cache'];?>">
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						<?php if($alert!=''){?>
							<div class="alert alert-info">
								<?php echo $alert;?>
							</div>
						<?php }?>
						<div class="panel panel-headline">
							<div class="panel-heading">
								<h3 class="panel-title">Issue/Return Books</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form method="POST">
											<div class="input-group">
												<div class="input-group-addon">
													<select name="search_type">
														<option value="1" selected="">SKU/Barcode/ISBN</option>
														<option value="2">Users</option>
														<option value="3">Title/Author</option>
													</select>
												</div>
												<input class="form-control" name="search" placeholder="Search Books" type="text">
												<div class="input-group-btn">
													<button class="btn btn-default" type="submit">
														<i class="glyphicon glyphicon-search"></i>
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<?php 
							if(isset($_REQUEST['search'])){
								if(isset($productlists)){
									foreach($productlists as $productlist){
										$fine = isset($productlist['user']['issue']['fine'])?$productlist['user']['issue']['fine']:'';	
										
						?>
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
										<div class="panel panel-headline">
											<div class="panel-body">
												<div class="col-xs-4 nopadding">
													<img src="<?php echo $productlist['product']['image'];?>" class="img-responsive" style="height:250px;">
												</div>
												<div class="col-xs-8">
													<p><?php echo substr($productlist['product']['title'],0,30).'...';?>
													<br>by <?php echo substr($productlist['product']['author'],0,30).'...';?></p>
													<strong>ISBN: </strong><?php echo $productlist['product']['isbn'];?>
													<br>
													<strong>SKU: </strong><?php echo $productlist['stock']['sku'];?>
													<br>
													<strong>Barcode: </strong><?php echo $productlist['stock']['barcode'];?>
													<br>
													<?php if(isset($productlist['user'])){?>
														<strong>Issued To: </strong><?php echo $productlist['user']['name'];?>
														<br>
														<strong>User ID: </strong><?php echo $productlist['user']['uid'];?>
													<?php } ?>
												</div>
												
											</div>
											<div class="panel-footer nopadding">
												<?php if(isset($productlist['user'])){?>
													<button class="btn btn-primary return_book col-xs-12" isbn="<?php echo $productlist['product']['isbn'];?>" barcode="<?php echo $productlist['stock']['barcode'];?>" bookcost="<?php echo $productlist['product']['mrp'];?>" fine=<?php echo json_encode($fine);?>>Return</button>
												<?php }else{?>
													<button class="btn btn-info issuebook col-xs-12" isbn="<?php echo $productlist['product']['isbn'];?>" barcode="<?php echo $productlist['stock']['barcode'];?>">Issue</button>
												<?php }?>
											</div>
										</div>
									</div>
								<?php }}else{ ?>
								<h3>No Result Found</h3>
							<?php }} ?>
							<!-- END OVERVIEW -->
							
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
			<div id="issueModal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title" id="isbntitle"></h4>
						</div>
						<div class="modal-body">
							<input class="form-control" placeholder="Search User" type="text" id="user_input">
							<div class="">
								<form method="POST">
									<input type="hidden" name="issue[barcode]" id="barcode_input" value="">
									<div id="issue_content" class="text-center">
										
									</div>
								</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
				<div id="returnModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
					<!-- Modal content-->
						<div class="modal-content">
							<form method="POST">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Return Book</h4>
							</div>
							<div class="modal-body">
								<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#finecollapse">Fine</button>
								<div id="finecollapse" class="collapse">
										<input type="hidden" name="return[barcode]" class="barcode_input" value="">
										<div class="radio"><label class="form-control">&nbsp;<input type="radio" name="return[fine][fine_type]" value="5bb73614ddad260ff08f994e">Lost <span></span></label></div>
										<div class="radio"><label class="form-control">&nbsp;<input type="radio" name="return[fine][fine_type]" value="5bb73629ddad260ff08f994f">Damaged <span></span></label></div>
										<div class="radio"><label class="form-control">&nbsp;<input type="radio" name="return[fine][fine_type]" value="5bb7363bddad260ff08f9951">Late Fee <span></span></label></div>
										<b>Fine Amount</b>
										<input type="text" class="form-control" name="return[fine][amount]" value="">
										*MRP for this book is ₹ <span class="book-cost">0</span>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn btn-success">Return</button>
								<button class="btn btn-link" type="button" onclick="$('input:radio').removeAttr('checked');">Clear</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END WRAPPER -->
			<!-- Javascript -->
			<script src="assets/vendor/jquery/jquery.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/bootstrap/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/vendor/chartist/js/chartist.min.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/scripts/klorofil-common.js?v=<?php echo $user->user['cache'];?>"></script>
			<script src="assets/scripts/custom.js?v=<?php echo $user->user['cache'];?>"></script>
			
		</body>
		
	</html>
