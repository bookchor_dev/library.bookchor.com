$(".issue-history").on("click", function(t) {
	$('#issueModal').modal('toggle');
	if(ajaxcheck==true){
		ajaxcheck = false;
		document.getElementById('issue-history-content').innerHTML = '<img src="https://library.bookchor.com/assets/img/loading2.gif" width="100px">';
		var search = $(this).attr('user_id');
		$.post('/admin/assets/ajax/user_issue.php', { user_id:search }, function(data){
			document.getElementById('issue-history-content').innerHTML = data;
			ajaxcheck = true;
		});
	}
});
$('button[uid]').click(function(e){
	var uid = $(this).attr('uid');
	$('.temp_input').remove();
	if(uid=='new_user'){
		$('form#user_form :input').val('');
		var src = '';
		var sb_btn = 'Add New User';
	}else{
		data = json_user[uid];
		$('form#user_form :input').each(function( index, element) {
			var inputname = $(this).attr('name');
			if(inputname=='email[]'||inputname=='contact[]'){
				var datval = data[inputname.split('[]')[0]];
				var elem = $('input[name="'+inputname+'"]').closest('div.input-group');
				for(var i=0;i<datval.length;i++){
					elem.after('<input type="text" name="'+inputname+'" class="form-control temp_input" value="'+datval[i]+'">');
				}
			}else{
				var datval = data[inputname];
				$(this).val(datval);
			}
			
		});
		var sb_btn = 'Update User';
		if(data['fp_hash']){
			var src = '../assets/images/Touch_ID.png';
		}else{
			var src = '';
		}
	}
	$('button[name=update]').text(sb_btn);
	$('#img-src-fp').attr('src',src);
	$('#editModal').modal('toggle');
})
$('.add_input').click(function(){
	var elem = $(this).closest('div.input-group');
	//var elem1 = elem.closest('div');
	elem.after(elem.find('input[type=text]').first().clone());
	//alert(elem);
});


var quality = 60; //(1 to 100) (recommanded minimum 55)
var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

function GetInfo() {
	document.getElementById('tdSerial').innerHTML = "";
	document.getElementById('tdCertification').innerHTML = "";
	document.getElementById('tdMake').innerHTML = "";
	document.getElementById('tdModel').innerHTML = "";
	document.getElementById('tdWidth').innerHTML = "";
	document.getElementById('tdHeight').innerHTML = "";
	document.getElementById('tdLocalMac').innerHTML = "";
	document.getElementById('tdLocalIP').innerHTML = "";
	document.getElementById('tdSystemID').innerHTML = "";
	document.getElementById('tdPublicIP').innerHTML = "";
	
	
	var key = document.getElementById('txtKey').value;
	
	var res;
	if (key.length == 0) {
		res = GetMFS100Info();
	}
	else {
		res = GetMFS100KeyInfo(key);
	}
	
	if (res.httpStaus) {
		
		document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
		
		if (res.data.ErrorCode == "0") {
			document.getElementById('tdSerial').innerHTML = res.data.DeviceInfo.SerialNo;
			document.getElementById('tdCertification').innerHTML = res.data.DeviceInfo.Certificate;
			document.getElementById('tdMake').innerHTML = res.data.DeviceInfo.Make;
			document.getElementById('tdModel').innerHTML = res.data.DeviceInfo.Model;
			document.getElementById('tdWidth').innerHTML = res.data.DeviceInfo.Width;
			document.getElementById('tdHeight').innerHTML = res.data.DeviceInfo.Height;
			document.getElementById('tdLocalMac').innerHTML = res.data.DeviceInfo.LocalMac;
			document.getElementById('tdLocalIP').innerHTML = res.data.DeviceInfo.LocalIP;
			document.getElementById('tdSystemID').innerHTML = res.data.DeviceInfo.SystemID;
			document.getElementById('tdPublicIP').innerHTML = res.data.DeviceInfo.PublicIP;
		}
	}
	else {
		alert(res.err);
	}
	return false;
}

function Capture() {
	try {
		//document.getElementById('txtStatus').value = "";
		document.getElementById('img-src-fp').src = "data:image/bmp;base64,";
		/* document.getElementById('txtImageInfo').value = "";
			document.getElementById('txtIsoTemplate').value = "";
			document.getElementById('txtAnsiTemplate').value = "";
			document.getElementById('txtIsoImage').value = "";
			document.getElementById('txtRawData').value = "";
		document.getElementById('txtWsqData').value = ""; */
		
		var res = CaptureFinger(quality, timeout);
		if (res.httpStaus) {
			
			//document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
			
			if (res.data.ErrorCode == "0") {
				document.getElementById('img-src-fp').src = "data:image/bmp;base64," + res.data.BitmapData;
				document.getElementById('fpIsoTemplate').value = res.data.IsoTemplate;
				/* var imageinfo = "Quality: " + res.data.Quality + " Nfiq: " + res.data.Nfiq + " W(in): " + res.data.InWidth + " H(in): " + res.data.InHeight + " area(in): " + res.data.InArea + " Resolution: " + res.data.Resolution + " GrayScale: " + res.data.GrayScale + " Bpp: " + res.data.Bpp + " WSQCompressRatio: " + res.data.WSQCompressRatio + " WSQInfo: " + res.data.WSQInfo;
					document.getElementById('txtImageInfo').value = imageinfo;
					
					document.getElementById('txtAnsiTemplate').value = res.data.AnsiTemplate;
					document.getElementById('txtIsoImage').value = res.data.IsoImage;
					document.getElementById('txtRawData').value = res.data.RawData;
				document.getElementById('txtWsqData').value = res.data.WsqImage; */
			}
		}
		else {
			alert(res.err);
		}
	}
	catch (e) {
		alert(e);
	}
	return false;
}
