var sku = '';
var booklists = new Array();
var flag = true;
$(".books_in").on("keyup", function(e) {
	if(e.which==13){
		var validate = true;
		var isbn = $(".books_in_isbn").val();
		sku = $(".books_in_sku").val();
		var condition = $(".books_in_cond").val();
		if(isbn.toString().length<10){
			$(".books_in_isbn").focus();
			validate = false;
		}
		if(!sku){
			$(".books_in_sku").focus();
			validate = false;
		}
		if((flag==true)&&(validate==true)){
			if(in_count<=20){
				//Books IN
				flag = false;
				$(".books_in").prop('disabled', true);
				$.post('/admin/assets/ajax/books_in.php', { isbn:isbn,sku:sku,condition:condition }, function(data){
						flag = true;
						in_count++;
						var booklist = new Array(data,isbn,sku);
						booklists.push(booklist);
						$('#book_in_data').prepend('<tr id="BC'+data+'"><td>BC'+data+'</td><td>'+isbn+'</td><td>'+sku+'</td><td>'+condition+'</td><td><button type="button" class="btn btn-danger book_out" barcode="'+data+'"><i class="fa fa-trash-o"></i></button></td></tr>');
						$(".books_in_isbn").val('');
						$(".books_in").prop('disabled', false);
						$(".books_in_isbn").focus();
				});
			}else{
				alert('Print the Pending Barcodes First');
			}
		}
	}
});
$(document).on('click', '.book_out', function(e){
	var barcode = $(this).attr('barcode');
	$.post('/admin/assets/ajax/books_delete.php', { barcode:barcode }, function(data){
		if(data==1){
			$('#BC'+barcode).remove();
		}
	});
});