ajaxcheck = true;
$(".issuebook").on("click", function(t) {
	var barcode = $(this).attr('barcode');
	var isbn = $(this).attr('isbn');
	document.getElementById('issue_content').innerHTML = '';
	document.getElementById('isbntitle').innerHTML = isbn;
	document.getElementById('barcode_input').value = barcode;
	$("#issueModal").modal('toggle');
});
$(".return_book").on("click", function(t) {
	var barcode = $(this).attr('barcode');
	var isbn = $(this).attr('isbn');
	var bookcost = $(this).attr('bookcost');
	var rawfine = $(this).attr('fine');
	var modalelem = $("#returnModal");
	modalelem.find('input.barcode_input').val(barcode);
	modalelem.find('span.book-cost').text(bookcost);
	if(rawfine){
		var fine = JSON.parse(rawfine);
		modalelem.find(":radio[value="+fine.fine_type.$oid+"]").prop("checked", true);
		modalelem.find(":radio[value="+fine.fine_type.$oid+"]").parent('label').find('span').text('( ₹ '+fine.amount+')');
		modalelem.find("input[name='return[fine][amount]']").val(fine.amount);
	}
	$("#returnModal").modal('toggle');
});
$("#user_input").on("keyup", function(t) {
	if(ajaxcheck==true){
		ajaxcheck = false;
		document.getElementById('issue_content').innerHTML = '<img src="http://library.bookchor.com/assets/img/loading2.gif" width="250px">';
		var search = document.getElementById('user_input').value;
		$.post('/admin/assets/ajax/search_user.php', { search:search }, function(data){
			document.getElementById('issue_content').innerHTML = data;
			ajaxcheck = true;
		});
	}
});
$('.update_lib').click(function(){
	var x = confirm('This will take a while so you can switch to other windows and carry on. Are you sure you want to continue?');
	if(x){
		document.getElementById("overlay").style.display = "block";
		$.post('/admin/assets/ajax/update_library.php', {  }, function(data){
			document.getElementById("overlay").style.display = "none";
		});
	}
});
$('[name=search_type]').on('change',function(e){
	var placehold = $('[name=search_type] option:selected').html();
	$('[name=search]').attr('placeholder',placehold);
});