<?php
	require '../../../vendor/autoload.php';
	require('../../../library_classes/Connection/Connection.php');
	require('../../../library_classes/Utility/Utility.php');
	require('../../../library_classes/User/User.php');
	require('../../../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	if(isset($_REQUEST['isbn'])&&isset($_REQUEST['sku'])){
		$isbn = $_REQUEST['isbn'];
		$sku = $_REQUEST['sku'];
		$cond = $_REQUEST['condition'];
		$barcode = $library->books_in($sku,$isbn,$user->user['library_id'],$cond);
		if($barcode){
			echo (string) sprintf("%08d",$barcode);	
		}
	}
?>