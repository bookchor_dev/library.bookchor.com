<?php
	require '../../../vendor/autoload.php';
	require('../../../library_classes/Connection/Connection.php');
	require('../../../library_classes/Utility/Utility.php');
	require('../../../library_classes/User/User.php');
	require('../../../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	if(isset($_REQUEST['flag_clear'])){
		$barcodes = $library->barcode_print($user->library_id);
		$count = 0;
		foreach($barcodes as $barcodelabel){
			$barcode = (string) $barcodelabel['barcode'];
			echo $barcode.'<br>';
			$library->print_flag($barcode,$user->library_id);
			$count++;
		}
	}else{
	
		if(isset($_REQUEST['booklists'])){
			$booklists = $_REQUEST['booklists'];
				foreach($booklists as $barcode){
					$library->print_flag($barcode,$user->library_id);
				}
				
		}
	}
?>