<?php
	error_reporting(0);
	require '../../../vendor/autoload.php';
	require('../../../library_classes/Connection/Connection.php');
	require('../../../library_classes/Utility/Utility.php');
	require('../../../library_classes/User/User.php');
	require('../../../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	if(isset($_REQUEST['search'])){
		$search = $_REQUEST['search'];
		$lusers = $library->search_user($search,$user->user['library_id']);
		echo '<table class="table table-striped">
				<thead>
					<tr>
					<td>Name</td>
					<td>ID</td>
					<td>Action</td>
					</tr>
				</thead>
				<tbody>';
		foreach($lusers as $luser){
		echo	'<tr>
					<td>'.$luser['name'].'</td>
					<td>'.$luser['uid'].'</td>
					<td><button class="btn btn-info" name="issue[user_id]" value="'.$luser['_id'].'">Issue</button></td>
				</tr>';
		}
		echo	'</tbody>
		</table>';
	}
?>