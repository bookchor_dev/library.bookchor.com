<?php
	require '../../../vendor/autoload.php';
	require('../../../library_classes/Connection/Connection.php');
	require('../../../library_classes/Utility/Utility.php');
	require('../../../library_classes/User/User.php');
	require('../../../library_classes/Library/Library.php');
	$user = new User();
	$library = new Library();
	if(isset($_REQUEST['barcode'])&&$_REQUEST['barcode']){
		$barcode = $_REQUEST['barcode'];
		$delete_temp = $library->books_temp_delete($barcode,$user->user['library_id']);
		echo $delete_temp;
	}
?>