<?php
	error_reporting(0);
	require '../../../vendor/autoload.php';
	require('../../../library_classes/Connection/Connection.php');
	require('../../../library_classes/Utility/Utility.php');
	require('../../../library_classes/User/User.php');
	require('../../../library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	if(isset($_REQUEST['user_id'])&&$_REQUEST['user_id']){
		$user_id = $_REQUEST['user_id'];
		$user_info = [
			'user_id' =>new MongoDB\BSON\ObjectId($user_id),
			'library_id'=>$user->user['library_id']
		];
		$doc_info = [
			'userlist'=>'$issue',
			'doc_id'=>0,
			'limit'=>50,
		];
		$issues = $product->user_booklist($user_info,$doc_info);
		//print_r($issues);die;
		echo '<table class="table">
				<thead>
					<tr>
						<td>Title</td>
						<td>ISBN</td>
						<td>Date Issued</td>
						<td>Date Returned</td>
						<td>Fine</td>
						<td>Status</td>
					</tr>
				</thead>
				<tbody>';
		foreach($issues['result'] as $issue){	
			//print_r($issue['details']['title']);die;
			$status = ($issue['status']==1)?'Issued':'Returned';
			$fine = isset($issue['fine']['amount'])?$issue['fine']['amount']:'N/A';
			$date_returned  = isset($issue['date_returned'])?date('Y-M-d',$issue['date_returned']):'N/A';
			echo '<tr><td>'.$issue['details']['title'].'</td><td>'.$issue['details']['isbn'].'</td><td>'.date('Y-M-d',$issue['date_issued']).'</td><td>'.$date_returned.'</td><td>'.$fine.'</td><td>'.$status.'</td></tr>';
		}
								
		echo		'</tbody>
			</table>';
	}		
?>