<?php
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/User/User.php');
	require('../library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$stock_str = 'Unavailable';
	if(isset($_REQUEST['search'])){
		$user_info = array(
			'user_id'=>$user->user['user_id'],
			'library_id'=>$user->user['library_id'],
			'solr_library_id'=>$user->user['solr_library_id'],
		);
		$doc_info = array(
			'query'=>$_REQUEST['search'],
			'type'=>'search',
		);
		$productlists = $product->booklist($user_info,$doc_info)['result'];
		//print_r($productlists);die;
	}
	
?>
<!doctype html>
<html lang="en">
	
	<head>
		<title>Library Admin</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
		<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.css">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="assets/css/demo.css">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
		<link rel="stylesheet" href="assets/css/custom.css">
	</head>
	
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<?php include 'header.php';?>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<?php include 'sidebar.php';?>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container-fluid">
						<!-- OVERVIEW -->
						<div class="panel panel-headline">
							<div class="panel-heading">
								<h3 class="panel-title">Search Books</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<form method="POST">
											<input type="text" class="form-control" name="search" placeholder="ISBN/Title/Author">
										</form>
									</div>
								</div>
							</div>
						</div>
						<?php 
						if(isset($_REQUEST['search'])){
						if(isset($productlists)){
						foreach($productlists as $productlist){
							$stock_str = 'Available';	
						?>
							<div class="col-md-4">
								<div class="panel panel-headline">
									<div class="panel-body">
										<div class="col-md-4 nopadding">
											<img src="<?php echo $productlist['image'];?>" height="150px">
										</div>
										<div class="col-md-offset-1 col-md-7">
											<p><?php echo $productlist['title'];?>
											<br>by <?php echo $productlist['author'];?></p>
											<strong>ISBN: </strong><?php echo $productlist['isbn'];?>
											<br>
											<strong><?php echo $stock_str;?></strong>
										</div>
									</div>
								</div>
							</div>
						<?php }}else{ ?>
							<h3>No Result Found</h3>
						<?php }} ?>
						<!-- END OVERVIEW -->
						
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<?php include 'footer.php';?>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="assets/vendor/jquery/jquery.min.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="assets/vendor/chartist/js/chartist.min.js"></script>
		<script src="assets/scripts/klorofil-common.js"></script>
		<script src="assets/scripts/custom.js"></script>
		
	</body>
	
</html>
