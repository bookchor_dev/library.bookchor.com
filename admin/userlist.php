<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require '../vendor/autoload.php';
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	require('../library_classes/Library/Library.php');
	$user = new Library();
	$date = date('Y-m-d');
	$library_id = new MongoDB\BSON\ObjectId('5af409d76577193010d3f0c9');
	require_once('Classes/PHPExcel.php');
	$filenames = ["userlist.xlsx"];
	foreach($filenames as $filename){
		$type = PHPExcel_IOFactory::identify($filename);
		$objReader = PHPExcel_IOFactory::createReader($type);
		$objPHPExcel = $objReader->load($filename);
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			$worksheets[$worksheet->getTitle()] = $worksheet->toArray();
		}
		unset($worksheets['Sheet1'][0]);
		unset($worksheets['Sheet1'][1]);
		unset($worksheets['Sheet1'][2]);
		foreach($worksheets['Sheet1'] as $stock){
			if(strpos($stock[10],'/')){
				$contact = explode('/',$stock[10]);
			}else{
				$contact = explode(',',$stock[10]);
			}
			$roll_no = str_replace('-','',$stock[3]);
			$username = explode(' ',$stock[4])[0].'_'.$roll_no;
			$user_data = [
				'username'=>$username,
				'class'=>$stock[1],
				'regd_no'=>$stock[2],
				'uid'=>str_replace('-','',$stock[3]),
				'roll_no'=>str_replace('-','',$stock[3]),
				'name'=>$stock[4],
				'father_name'=>$stock[5],
				'mother_name'=>$stock[6],
				'address'=>$stock[7],
				'gender'=>$stock[8],
				'dob'=>$stock[9],
				'contact'=>	$contact,
				'email'=>explode(',',$stock[11]),
				'blood_group'=>$stock[12],	
				'password'=>str_replace('.','',$stock[9]),
				'library_id'=>$library_id,
				'user_type_id'=>1,
				'status'=>1
			];
			$user->users_api($user_data);
		}
	}
?>	