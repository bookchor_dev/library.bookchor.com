<?php
	if(isset($_REQUEST['auth_key'])&&$_REQUEST['auth_key']=='q8w9ert8y1s3c1vb3hd31fd3d'){
		$date = date('Y-m-d h:i:s');
		$user_info = $_REQUEST;
		$user_data = [];
		require '../vendor/autoload.php';
		require('../library_classes/Connection/Connection.php');
		require('../library_classes/Library/Library.php');
		$library = new Library();
		$library_id = new MongoDB\BSON\ObjectId('5af409d76577193010d3f0c9');
		if(is_array($user_info['contact'])&&is_array($user_info['email'])){
			$contact = $user_info['contact'];
			$email = $user_info['email'];
			if($user_info['user_type_id']==1){
				if(isset($user_info['class'])&&isset($user_info['roll_no'])&&isset($user_info['name'])&&isset($user_info['father_name'])&&isset($user_info['mother_name'])&&isset($user_info['address'])&&isset($user_info['gender'])&&isset($user_info['dob'])){
					$roll_no = str_replace('-','',$user_info['roll_no']);
					$username = explode(' ',$user_info['name'])[0].'_'.$roll_no;
					$user_data = [
						'username'=>$username,
						'class'=>$user_info['class'],
						'regd_no'=>$user_info['regd_no'],
						'roll_no'=>$roll_no,
						'name'=>$user_info['name'],
						'father_name'=>$user_info['father_name'],
						'mother_name'=>$user_info['mother_name'],
						'address'=>$user_info['address'],
						'gender'=>$user_info['gender'],
						'dob'=>$user_info['dob'],
						'contact'=>	$contact,
						'email'=>$email,
						'password'=>str_replace('.','',$user_info['dob']),
						'library_id'=>$library_id,
						'user_type_id'=>1,
						'date_modified'=>$date,
						'status'=>1
					];
				}
			}else if($user_info['user_type_id']==3){
				if(isset($user_info['emp_id'])&&isset($user_info['name'])&&isset($user_info['department'])&&isset($user_info['gender'])&&isset($user_info['address'])&&isset($user_info['gender'])){
					$username = $user_info['name'].'_'.$user_info['emp_id'];
					$user_data = [
						'username'=>$username,
						'emp_id'=>$user_info['emp_id'],
						'name'=>$user_info['name'],
						'department'=>$user_info['department'],
						'gender'=>$user_info['gender'],
						'contact'=>$contact,
						'email'=>$email,
						'address'=>$user_info['address'],
						'password'=>$user_info['emp_id'],
						'library_id'=>$library_id,
						'user_type_id'=>3,
						'date_modified'=>$date,
						'status'=>1
					];
				}
			}
			if($user_data){
				echo json_encode($library->users_api($user_data));
			}
		}
	}
?>	