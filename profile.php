<?php
	error_reporting(0);
	require_once 'vendor/autoload.php';
	require_once('library_classes/Connection/Connection.php');
	require_once('library_classes/Utility/Utility.php');
	require_once('library_classes/User/User.php');
	require_once('library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$user_info = [
	'user_id' =>$user->user['user_id'],
	'library_id'=>$user->user['library_id']
	];
	$user_details = $user->user_info($user->user['user_id']);
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Profile - <?php echo $user->user['name'];?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
    <link rel="stylesheet" type="text/css" href="/assets/css/global.css?v=<?php echo $user->user['cache'];?>">
    <link rel="stylesheet" type="text/css" href="/assets/css/element.css?v=<?php echo $user->user['cache'];?>">
    <link rel="stylesheet" href="/assets/css/header.css?v=<?php echo $user->user['cache'];?>">
    <link rel="stylesheet" href="/assets/css/footer.css?v=<?php echo $user->user['cache'];?>">
    <link rel="stylesheet" href="/assets/bookchor_icons/styles.css?v=<?php echo $user->user['cache'];?>">
    <link rel="stylesheet" href="/assets/css/profile_page.css?v=<?php echo $user->user['cache'];?>">
</head>
<body>
    <?php include "header.php";?>
    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="grid mt-3">
						<div class="card profile-card">
							<div class="card-body">
                            <h6 id="title" class="font-weight-bold align-center">My Profile</h6>
                                <img src="/assets/images/img_avatar1.png" class="mx-auto d-block rounded-circle" alt="image">
                            </div>
                        </div>
                        <div class="card student-info">
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td class="font-weight-bold"><?php echo $user->user['name'];?></td>
                                        </tr>
                                        <tr>
                                            <td>Class</td>
                                            <td class="font-weight-bold"><?php echo $user_details['class'];?> Standard</td>
                                        </tr>
                                        <tr>
                                            <td>School ID</td>
                                            <td class="font-weight-bold"><?php echo $user_details['uid'];?></td>
                                        </tr>
                                        <tr>
                                            <td>DOB</td>
                                            <td class="font-weight-bold"><?php echo $user_details['dob'];?></td>
                                        </tr>
                                        <tr>
                                            <td>Contact Info</td>
                                            <td class="font-weight-bold"><?php echo $user_details['contact'][0].'/'.$user_details['email'][0];?></td>
                                        </tr>
										<tr>
                                            <td>Total Read</td>
                                            <td class="font-weight-bold"><?php echo count($user_details['issue']);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="grid2">
                        <div>
                            <p class="font-weight-bold ">Issue Return</p>
                            <br>
							<a href="/issued">
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="/assets/images/Issue_Icon.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p class="float-left">Issue Cycle for the regular book is 7 days</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</a>
                        </div>
                        <div>
                            <p class="font-weight-bold ">Wishlist</p>
                            <br>
							<a href="/wishlist">
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="/assets/images/Wishlist-heart.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p>Your Personal Readlist</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</a>
                        </div>
						<div>
                            <p class="font-weight-bold ">Your Favourite Genre</p>
                            <br>
							<a href="javascript:void(0);" onclick="selectCategory();">
                            <div class="card">
                                <div class="card-body group-cards">
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="/assets/images/Club_Group.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p>Select the genre that you like</p>
											<small></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</a>
                        </div>
                        
                        <div>
                            <p class="font-weight-bold ">E-Books</p>
                            <br>
							<a href="/e-books/1">
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="/assets/images/E-book.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p> Access Ebooks from anywhere</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</a>
                        </div>
                        <div>
                            <p class="font-weight-bold ">Book Club</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards">
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="/assets/images/Club_Group.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p>Enjoy Reading with your peers</p>
											<small>Coming Soon</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div>
                            <p class="font-weight-bold ">Recommendation</p>
                            <br>
                            <div class="card">
                                <div class="card-body group-cards" >
                                    <div class="grid3">
                                        <div class="group-icons">
                                            <img src="/assets/images/Recommended.svg" class="img-fluid float-left" alt="image">
                                        </div>
                                        <div class="group-text">
                                            <p> Books recommended by your Peers</p>
											<small>Coming Soon</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include "footer.php";?>
    </body>
    <script src="/assets/js/jquery.js?v=<?php echo $user->user['cache'];?>"></script>
    <script src="/assets/js/popper.js?v=<?php echo $user->user['cache'];?>"></script>
    <script src="/assets/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
    <script src="/assets/js/header.js?v=<?php echo $user->user['cache'];?>"></script>
    </html>
