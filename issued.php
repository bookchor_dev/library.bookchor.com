<?php
	require_once 'vendor/autoload.php';
	require_once('library_classes/Connection/Connection.php');
	require_once('library_classes/Utility/Utility.php');
	require_once('library_classes/User/User.php');
	require_once('library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$user_info = [
	'user_id' =>$user->user['user_id'],
	'library_id'=>$user->user['library_id']
	];
	$doc_info = [
		'userlist'=>'$issue',
		'doc_id'=>isset($_REQUEST['doc_id'])?$_REQUEST['doc_id']:0,
		'limit'=>10,
	];
	$productlists = $product->user_booklist($user_info,$doc_info);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Issue History</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/issue_history_page.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
		<link rel="stylesheet" type="text/css" href="/assets/css/global.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" type="text/css" href="/assets/css/element.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/header.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/footer.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/bookchor_icons/styles.css?v=<?php echo $user->user['cache'];?>">
	</head>
	<body>
		<?php include "header.php";?>
		<section>
			<div class="container-fluid outer">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 my-4">
						<h5 class="font-weight-bold page-title pl-4">Issue History</h5>
					</div>
					<?php 
						if(isset($productlists['results'])&&$productlists['results']){
							foreach($productlists['results'] as $list){
								$productlist = $list['details'];
							?>
							<div class="col-md-6 col-lg-4 col-xl-4">
								<div class="card">
									<div class="card-body inner-content">
										<div class="grid2">
											<div class="card-body">
												<img src="<?php echo $productlist['image'];?>" alt="<?php echo $productlist['title'];?>" class="img-fluid" style="height:300px;">
											</div>
											<div class="card-body">
												<h6 class="font-weight-bold float-left book-title"><?php echo $productlist['title'];?></h6>
												<br>
												<small class="text-muted float-left">BY <?php echo $productlist['author'];?></small>
												<br>
												<button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0"><?php echo $productlist['goodreads_avg_rating'];?><span class="bc-bc-star star_icon"></button>
													<small>(<?php echo $productlist['goodreads_total_reviews'];?>)</small>
													<br>
													<small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos
														quis
													est nobis reiciendis officia voluptatum, et eligendi ipsa....</small>
													<div class="clearfix"></div>
													<small class="font-weight-bold float-right" style="color:#108690"> Issue
													Date</small>
													<br>
													<p class="font-weight-bold float-right"><?php echo date('d-M-y',$list['date_issued']);?></p>
												</div>
											</div>
										</div>
										<div class="card-footer return-date">
											<p class="align-center font-weight-bold">Return Date: <?php echo date('d-M-y',$list['date_returned']);?></p>
										</div>
									</div>
								</div>
							<?php } ?>
							<?php if($doc_info['limit']<=count($productlists['results'])){?>
								<div class="col-md-12 col-lg-12 col-xl-12">
									<form>
										<div class="btn-group d-flex justify-content-center">
											<?php if($productlists['pdoc']){?>
												<button type="button" onclick="window.history.go(-1);" class="btn btn-warning">Previous</button>
											<?php } ?>
											<button name='doc_id' value=<?php echo $productlists['ndoc'];?> class="btn btn-success">Next</button>
										</div> 
									</form>	
								</div>
							<?php }?>
							<?php }else{?>
							<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<img src="/assets/images/Issue_History.svg" alt="logo" class="img-fluid  w-100 h-50">
								<p class="align-center pb-2">Your Issue History is Empty</p>
								
								<div class="align-center">
									<button type="submit" class="btn btn-outline-dark rounded"><a href="/category/5bb11c0670d9bd2d0883262e/All-Books" class="theme-text-color">Continue Browsing Books</a></button>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</section>
			
			<?php include "footer.php";?>
		</body>
		<script src="/assets/js/jquery.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/popper.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/main.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/header.js?v=<?php echo $user->user['cache'];?>"></script>
	</html>
