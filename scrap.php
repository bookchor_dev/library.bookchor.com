<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	if(isset($_REQUEST['city'])){
		header('Content-Type: text/plain');
		$ch = curl_init();
		$city_arr[] = $_REQUEST['city'];
		foreach($city_arr as $city_name){
			$text = '<table><thead><tr><td>School</td><td>Address1</td><td>Address2</td><td>Pincode</td><td>City</td><td>State</td><td>Mobile</td><td>Phone</td></tr></thead><tbody>';
			$doc_id = 0;
			$empty_flag = 0;
			do {
				if($doc_id){
					$url = "https://targetstudy.com/school/schools-in-".$city_name.".html?recNo=".$doc_id;
				}else{
					$url = "https://targetstudy.com/school/schools-in-".$city_name.".html";
				}
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
				$page = curl_exec($ch);
				preg_match_all("/<h2[^>]*class=\'heading1\'>(.*?)<\\/h2>/si", $page, $school); //School Name
				preg_match_all("/<tr><td>(.*?)<br/si",$page,$address); //Address
				preg_match_all("/Mobile[^>]*: (.*?)</si",$page,$mobile); //Mobile
				preg_match_all("/Phone[^>]*: (.*?)</si",$page,$phone); //Phone
				if($school[1]){
					for($i=0;$i<count($school[1]);$i++){
						$address_arr = explode("<div style='line-height:10px;clear:both'></div>",$address[1][$i]);
						$add_arr = explode(',',$address_arr[1]);
						$a_arr = explode('-',$add_arr[0]);
						$pincode = $a_arr[1];
						$city = $a_arr[0];
						$state = $add_arr[1];
						$text .= '<tr><td>'.$school[1][$i].'</td><td>'.$address_arr[0].'</td><td>'.$address_arr[1].'</td><td>'.$pincode.'</td><td>'.$city.'</td><td>'.$state.'</td><td>'.$mobile[1][$i].'</td><td>'.$phone[1][$i].'</td></tr>';
					}
				}else{
					$empty_flag = 1;
				}
				$doc_id = $doc_id + 25;
			} while($empty_flag==0);
			$text .= '</tbody></table>';
			$file = fopen($city_name.".xls","w");
			fwrite($file,$text);
			fclose($file);
		}
		curl_close($ch);
	}
?>