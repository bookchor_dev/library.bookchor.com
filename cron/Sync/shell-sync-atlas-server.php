<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	ini_set('memory_limit','2048M');
	require_once('/home/bchor/public_html/library.bookchor.com/bc_autoload_shell.php');
	$type = $argv[1];
	if($type){
		if($type==1){
			$sync = new \Utility\Sync();
			$sync->syncIssueHistory();
		}else if($type==2){
			$sync = new \Utility\Sync();
			$sync->syncBooksStock();
		}
	}
?>