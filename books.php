<?php
	require_once 'vendor/autoload.php';
	require_once('library_classes/Connection/Connection.php');
	require_once('library_classes/Utility/Utility.php');
	require_once('library_classes/User/User.php');
	require_once('library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$user_info = $user->user;
	$doc_info = [
	'id'=>explode('/',$_REQUEST['id'])[0],
	'query'=>isset($_REQUEST['query'])?$_REQUEST['query']:'',
	'type'=>$_REQUEST['type'],
	'doc_id'=>isset($_REQUEST['doc_id'])?$_REQUEST['doc_id']:0,
	'limit'=>10,
	'ebook'=>isset($_REQUEST['ebook'])?$_REQUEST['ebook']:0,
	];
	$productlists = $product->booklist($user_info,$doc_info,$user->user['bc_db_access']);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $productlists['title'];?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/categories_page.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" type="text/css" href="/assets/css/global.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" type="text/css" href="/assets/css/element.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/header.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/footer.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/bookchor_icons/styles.css?v=<?php echo $user->user['cache'];?>">
	</head>
	<body>
		<?php include "header.php";?>
		<section>
			<div class="container outer">
				<div class="overlay">
					<div class="loader"></div>
				</div>
				<div class="row mb-3">
					<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 my-4">
						<h5 class="font-weight-bold page-title pl-4"><?php echo $productlists['title'];?></h5>
					</div>
					<?php 
						if(isset($productlists['result'])&&$productlists['result']){
							foreach($productlists['result'] as $productlist){
								if(strlen($productlist['title'])>25){
									$title = substr($productlist['title'],0,25).'...';
									}else{
									$title = $productlist['title'];
								}
								if(strlen($productlist['author'])>35){
									$author = substr($productlist['author'],0,35).'...';
									}else{
									$author = $productlist['author'];
								}
							?>
							<div class="col-md-6 col-lg-6 col-xl-6 mb-3">
								<div class="card">
									<div class="card-body">
										<div class="grid2">
											<div class="card-body">
												<a href="<?php echo $productlist['link'];?>"><img src="<?php echo $productlist['image'];?>" alt="<?php echo $productlist['title'];?>" class="img-fluid"></a>
											</div>
											<div class="card-body">
												<a href="<?php echo $productlist['link'];?>" title="<?php echo $productlist['title'];?>"><h6 class="font-weight-bold float-left book-title"  ><?php echo $title;?></h6></a>
												<div class="clearfix"></div>
												<small class="text-muted float-left" title="<?php echo $productlist['author'];?>">BY <?php echo $author;?></small>
												<div class="clearfix"></div>
												<small>(<?php echo $productlist['reviews'];?> reviews)</small>
												<button type="button" class="btn btn-success rating_button pl-0 pt-0 pb-0"><?php echo $productlist['rating'];?><span class="bc-bc-star star_icon"></button>
													<div class="clearfix"></div>
													<div class="product_user mt-3" data-pid=<?php echo $productlist['product_id'];?> url-ebook="<?php echo $productlist['ebook'];?>">
														<div class="linear-background mt-5"></div>
														<div class="linear-background mt-3" style="width:80%"></div>
														<div class="linear-background mt-3" style="width:60%"></div>
														<div class="linear-background mt-3" style="width:40%"></div>
														<div class="linear-background mt-3" style="width:20%"></div>
													</div>
												</div>
											</div>
										</div>
										<?php if($productlist['stock']==1){?>
										<div class="card book-availability">
											<div class="card-body " style="padding:15px !important">
												<p class="font-weight-bold">Book Availability</p>
												<div class="grid4" data-pid=<?php echo $productlist['product_id'];?>>
													<img src="/assets/images/loading.gif" class="img-fluid">
													<img src="/assets/images/loading.gif" class="img-fluid">
													<img src="/assets/images/loading.gif" class="img-fluid">
													<img src="/assets/images/loading.gif" class="img-fluid">
												</div>
											</div>
										</div>
										<div class="card bar-code">
											<div class="card-body">
												<input type="text" class="form-control barcode-input" id="usr" placeholder="Unique Bar-Code of the book">
											</div>
										</div>
										<div class="card-body return-date">
											<div class="grid3">
												<div class="card-body one">
													<button class="btn2 align-center font-weight-bold issue"><a href="#">Issue Book</a></button>
												</div>
												<div class="card-body two">
													<button class="btn2 align-center font-weight-bold find"><a href="#">Find Book</a></button>
												</div>
											</div>
										</div>
										<?php }?>
									</div>
								</div>
							<?php } ?>
							<?php if($doc_info['limit']<= count($productlists['result'])){ ?>
								<div class="col-md-12 col-lg-12 col-xl-12">
									<form>
										<div class="btn-group d-flex justify-content-center">
											<?php if($productlists['pdoc']){?>
												<button type="button" onclick="window.history.go(-1);" class="btn btn-warning">Previous</button>
											<?php } ?>
											<?php if($doc_info['ebook']==1){?>
												<a href="/e-books/<?php echo $productlists['ndoc'];?>" class="btn btn-success">Next</a>
											<?php }else{?>
												<button name='doc_id' value=<?php echo $productlists['ndoc'];?> class="btn btn-success">Next</button>
											<?php }?>
										</div> 
									</form>	
								</div>
							<?php }?>
							<?php }else{?>
							<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<img src="/assets/images/Wishlist_mobile.svg" alt="logo" class="img-fluid  w-100 h-50">
								<p class="align-center pb-2">No Books Here</p>
								
								<div class="align-center">
									<button class="btn btn-outline-dark rounded"><a href="/category/5bb11c0670d9bd2d0883262e/All-Books" class="theme-text-color">Continue Browsing Books</a></button>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</section>
			<?php include "footer.php";?>
		</body>
		<script src="/assets/js/jquery.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/popper.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/main.js?v=<?php echo $user->user['cache'];?>"></script>
		<script src="/assets/js/header.js?v=<?php echo $user->user['cache'];?>"></script>
	</html>
