<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	if(isset($_REQUEST['type'])){
		include $_SERVER["DOCUMENT_ROOT"].'/bc_autoload.php';
		$type = $_REQUEST['type'];
		if($type == 'booksToAdd'){
			$quiz = new Users\IssueHistory();
			echo $quiz->issue_history(0,3);
		}else if($type == 'addQuiz'&&isset($_REQUEST['product_id'])&&isset($_REQUEST['json'])){
			$quiz = new Product\Quiz();
			echo $quiz->addQuiz($_REQUEST['product_id'],$_REQUEST['json']);
		}else if($type == 'viewQuiz'&&isset($_REQUEST['query'])){
			$quiz = new Product\Quiz();
			echo $quiz->viewQuiz($_REQUEST['query']);
		}else if($type == 'deleteQuestion'&&isset($_REQUEST['product_id'])&&isset($_REQUEST['question_id'])){
			$quiz = new Product\Quiz();
			echo $quiz->deleteQuestion($_REQUEST['product_id'],$_REQUEST['question_id']);
		}else if($type == 'viewProducts'){
			$quiz = new Product\Quiz();
			if(isset($_REQUEST['skip'])){
				$skip = $_REQUEST['skip'];
			}else{
				$skip = 0;
			}
			if(isset($_REQUEST['callback'])){
				echo $_REQUEST['callback'].'('.$quiz->viewProducts($skip).');';
			}else{
				echo $quiz->viewProducts($skip);
			}
		}
	}
?>