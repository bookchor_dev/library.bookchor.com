<?php
	namespace Product;
	use Connection\Connection;
	class Category{
		public function __construct(){
			$con = new Connection();
			$this->mongo_bookchor = $con->mongo_bookchor();
		}
		public function category_listing(){
			$pipeline = [
				[
					'$match' => [
						'id' => ['$lte'=>10]
					]
				],
				[
					'$project' => [
						'_id' => ['$toString' => '$_id'],
						'name' => 1,
						'image' => ['$arrayElemAt'=>['$image',0]],
					]
				]
			];
			$query = $this->mongo_bookchor->category->aggregate($pipeline);
			$data = iterator_to_array($query);
			return json_encode($data);
		}
	}
?>