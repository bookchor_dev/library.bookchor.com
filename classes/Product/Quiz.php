<?php
	namespace Product;
	use Connection\Connection;
	use MongoDB\BSON\ObjectId;
	class Quiz{
		public function __construct(){
			$con = new Connection();
			$this->mongo_bookchor = $con->mongo_bookchor();
		}
		public function addQuiz($product_id,$quiz){
			$quiz = json_decode($quiz);
			for($i=0;$i<sizeof($quiz);$i++){
				$question = $quiz[$i]->question;
				$query = $this->mongo_bookchor->product_quiz->find(['product_id'=>new ObjectId($product_id),'quiz.question'=>$question]);
				$data = iterator_to_array($query);
				if(!isset($data[0]['_id'])){
					$quiz[$i]->_id = new ObjectId();
					$quiz[$i]->date_added = date("Y-m-d H:i:s");
					$this->mongo_bookchor->product_quiz->updateOne(['product_id'=>new ObjectId($product_id)],['$addToSet'=>['quiz'=>$quiz[$i]]],['upsert'=>true]);
				}
			}
			return '{"error":0}';
		}
		public function deleteQuestion($product_id,$question_id){
			$query = $this->mongo_bookchor->product_quiz->find(['product_id'=>new ObjectId($product_id),'quiz'=>['$not'=>['$size'=>1]]]);
			$data = iterator_to_array($query);
			if(isset($data[0]['_id'])){
				$this->mongo_bookchor->product_quiz->updateOne(['product_id'=>new ObjectId($product_id)],['$pull'=>['quiz'=>['_id'=>new ObjectId($question_id)]]]);
			}else{
				$this->mongo_bookchor->product_quiz->deleteOne(['product_id'=>new ObjectId($product_id)]);			
			}
			return '{"error":0}';
		}
		public function viewProducts($skip){
			$pipeline = [
				[
					'$skip' => (int)$skip
				],
				[
					'$lookup' => [
						'from' => 'product',
						'localField' => 'product_id',
						'foreignField' => '_id',
						'as' => 'product'
					]
				],
				[
					'$project' => [
						'_id' => ['$toString'=>'$_id'],
						'product_id' => ['$toString'=>'$product_id'],
						'title' => ['$arrayElemAt'=>['$product.title',0]],
						'ean' => ['$arrayElemAt'=>['$product.ean',0]],
					]
				],
				[
					'$limit' => 20
				]
			];
			$query = $this->mongo_bookchor->product_quiz->aggregate($pipeline);
			$data = iterator_to_array($query);
			return json_encode($data);
		}
		public function viewQuiz($search){
			if(is_numeric($search)){
				$pipeline = [
					[
						'$match' => [
							'ean' => $search
						]
					],
					[
						'$lookup' => [
							'from' => 'product_quiz',
							'localField' => '_id',
							'foreignField' => 'product_id',
							'as' => 'product_quiz'
						]
					],
					[
						'$project' => [
							'_id' => ['$arrayElemAt'=>['$product_quiz._id',0]],
							'product_id' => '$_id',
							'title' => 1,
							'ean' => 1,
							'quiz' => ['$arrayElemAt'=>['$product_quiz.quiz',0]],
						]
					]
				];
				$query = $this->mongo_bookchor->product->aggregate($pipeline);
			}else{
				$pipeline = [
					[
						'$match' => [
							'product_id' => new ObjectId($search)
						]
					],
					[
						'$lookup' => [
							'from' => 'product',
							'localField' => 'product_id',
							'foreignField' => '_id',
							'as' => 'product'
						]
					],
					[
						'$project' => [
							'product_id' => 1,
							'title' => ['$arrayElemAt'=>['$product.title',0]],
							'ean' => ['$arrayElemAt'=>['$product.ean',0]],
							'quiz' => 1,
						]
					]
				];
				$query = $this->mongo_bookchor->product_quiz->aggregate($pipeline);
			}
			$data = iterator_to_array($query);
			if(isset($data[0])){
				for($i=0;$i<sizeof($data[0]->quiz);$i++){
					$data[0]->_id = (string) $data[0]->_id;
					$data[0]->product_id = (string) $data[0]->product_id;
					$data[0]->quiz[$i]->_id = (string)$data[0]->quiz[$i]->_id;
				}
			}
			return json_encode($data);
		}
	}
?>