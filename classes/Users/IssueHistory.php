<?php
	namespace Users;
	use Connection\Connection;
	class IssueHistory{
		public function __construct(){
			$con = new Connection();
			$this->mongo_bookchor = $con->mongo_bookchor();
		}
		public function issue_history($id,$type){
			if($type == 1){
				//issue_history according to user
			}else if($type == 2){
				//issue_history according to library
			}else{
				$pipeline=[
					[
						'$group'=>
						[
							'_id'=>'$product_id',
						]
					],
					[
						'$project'=>
						[
							'_id'=>0,
							'product_id'=>'$_id'
						]
					],
					[
						'$lookup' => [
							'from' => 'product_quiz',
							'localField' => 'product_id',
							'foreignField' => 'product_id',
							'as' => 'matched_products'
						]
					],
					[
						'$match'=>['matched_products'=> ['$eq'=>[] ]]
					],
					[
						'$lookup'=>
						[
							'from' => 'product',
							'localField' => 'product_id',
							'foreignField' => '_id',
							'as' => 'product_data'
						]
					],
					[
						'$project'=>
						[
							'product_id'=> ['$toString'=>['$arrayElemAt'=>['$product_data._id',0]]],
							'ean'=> ['$arrayElemAt'=>['$product_data.ean',0]],
							'title'=> ['$arrayElemAt'=>['$product_data.title',0]],
							'image'=> ['$concat'=>["https://www.bookchor.com/images/",['$arrayElemAt'=>[['$arrayElemAt'=>['$product_data.image',0]],0]]]],
						]
					]
				];
				$query = $this->mongo_bookchor->issue_history->aggregate($pipeline);
				$data = iterator_to_array($query);
			}
			return json_encode($data);
		}
	}
?>