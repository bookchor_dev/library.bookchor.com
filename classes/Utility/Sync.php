<?php
	namespace Utility;
	use Connection\Connection;
	use MongoDB\BSON\ObjectId;
	class Sync{
		public function __construct(){
			$con = new Connection();
			$this->mongo_bookchor = $con->mongo_bookchor();
			$this->mongo_bookchor_library = $con->mongo_bookchor_library();
		}
		public function cronEbooks(){
			//$abs_path = dirname(__DIR__);
			$abs_path = '/home/bchor/public_html/library.bookchor.com';
			$target_dir = "/tempisbn/";
			$folder_dir = scandir($abs_path.$target_dir,1);
			$i = 0;
			foreach($folder_dir as $key=>$isbn_file){
				$isbn = explode('.',$isbn_file)[0];
				$pass = urlencode('Success^1218#');
				echo $isbn."<br>";
				$url = "http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/select?indent=on&q=".$isbn."&wt=json";	
				$json = file_get_contents($url);
				$data = json_decode($json);
				$responses = $data->response->docs;
				if($responses){
					foreach($responses as $response){
						$pid = $response->id;
					}
					if($i%1000==0){
						$i=0;
						$timestamp = (new \DateTime())->getTimestamp();
						mkdir($abs_path.'/ebooks/'.$timestamp);
					}
					$i++;
					rename($abs_path.$target_dir.$isbn_file, $abs_path.'/ebooks/'.$timestamp."/".$isbn_file);
					$data = array(
						"add" => array(
							"doc" => [
								"id" => $pid,
								"ebook" => array(
									"set" => '/ebooks/'.$timestamp."/".$isbn_file,
								),
							],
						),
					);
					$data_string = json_encode($data);
					$pass = urlencode('Success^1218#');
					do{
						$ch = curl_init("http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/update?wt=json");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
						curl_setopt($ch, CURLOPT_POST, TRUE);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
						$responses = curl_exec($ch);
						echo $responses." ".$pid." ".$isbn;
					}while(!$responses);
				}else{
					rename($abs_path.$target_dir.$isbn_file, $abs_path.$target_dir.'/notInDatabase/'.$isbn_file);
				}
			}
		}
		public function syncBooksStock(){
			$this->mongo_bookchor->library_books->deleteMany([]);
			$query = $this->mongo_bookchor_library->books->find([]);
			$data = iterator_to_array($query);
			foreach($data as $values){
				$mysql_product_id = $values['product_id'];
				$query_pid = $this->mongo_bookchor->product->find(['mysql_product_id'=>(int)$mysql_product_id],['projection'=>['_id'=>1]]);
				$data_pid = iterator_to_array($query_pid);
				$product_id = $data_pid[0]['_id'];
				$library_id = $values['library_id'];
				for($i=0;$i<sizeof($values['stock']);$i++){
					$sku = $values['stock'][$i]->sku;
					$barcode = $values['stock'][$i]->barcode;
					$condition = $values['stock'][$i]->condition;
					if($condition == 'New'){
						$condition_id = 1;
					}else if($condition == 'Old'){
						$condition_id = 2;
					}else{
						$condition_id = 3;
					}
					$status = (int) $values['stock'][$i]->status;
					$date_added = date("Y-m-d H:i:s",$values['stock'][$i]->date_added);
					$out = (int) $values['stock'][$i]->out;
					$print = (int) $values['stock'][$i]->print;
					if($status == 0){
						$barcode = $bacode."-OUT";
					}
					$books = array(
						'library_id' => $library_id,
						'product_id' => $product_id,
						'sku' => $sku,
						'barcode' => $barcode,
						'condition_id' => $condition_id,
						'status' => $status,
						'out' => $out,
						'print' => $print,
						'date_added' => $date_added
					);
					try{
						$this->mongo_bookchor->library_books->insertOne($books);
					}catch(\Exception $e){
						
					}
				}
			}
		}
		public function syncIssueHistory(){
			/* $query_max = $this->mongo_bookchor->issue_history->find([],['projection'=>['_id'=>1],'sort'=>['_id'=>-1],'limit'=>1]);
			$data_max = iterator_to_array($query_max);
			$max_id = $data_max[0]['_id']; */
			$this->mongo_bookchor->issue_history->deleteMany([]);
			$pipeline=[
				[
					'$unwind'=>'$issue'
				],
				[
					'$project'=>
					[
						'_id'=>'$issue._id',
						'user_id'=>'$_id',
						'library_id'=>'$library_id',
						'mysql_product_id'=>'$issue.product_id',
						'barcode'=>'$issue.barcode',
						'date_issued'=>'$issue.date_issued',
						'date_returned'=>'$issue.date_returned',
						'status'=>1,
					]
				],
			];
			$result = $this->mongo_bookchor_library->users->aggregate($pipeline);
			$data = iterator_to_array($result);
			if(isset($data[0])){
				$mysql_product_id=[];
				foreach ($data as $key => $value) {
					$mysql_product_id[$key]=(int)$value['mysql_product_id'];
				}
				$pipeline_pid=[
					[
						'$match'=>
						[
							'mysql_product_id'=>['$in'=>$mysql_product_id]
						]
					],
					[
						'$project'=>
						[
							'_id'=>0,
							'product_id'=>'$_id',
							'mysql_product_id'=>'$mysql_product_id'
						]
					]
				];
				$result_pid = $this->mongo_bookchor->product->aggregate($pipeline_pid);
				$data_pid = iterator_to_array($result_pid);				
				foreach ($data as $key => $value) {
					if(isset($value['date_issued'])){
						$data[$key]['date_issued'] = date('Y-m-d H:i:s', $value['date_issued']);
					}
					if(isset($value['date_returned'])){
						$data[$key]['date_returned'] = date('Y-m-d H:i:s', $value['date_returned']);
					}
					foreach ($data_pid as $iterator => $content) {
						if($value['mysql_product_id'] == $content['mysql_product_id']){
							$data[$key]['product_id'] = $content['product_id'];
							$data[$key]['mysql_product_id'] = (int)$data[$key]['mysql_product_id'];
						}
					}
				}
				foreach ($data as $key => $value) {
					$data=$this->mongo_bookchor->issue_history->insertOne($value);
				}
			}
		}
	}
?>