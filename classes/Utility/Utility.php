<?php
	namespace Utility;
	use Connection\Connection;
	class Utility{
		public function convertIsbn13($isbn_10){
			$isbn_10 = "978".substr($isbn_10,0,9);
			$digits  = array_map('intval', str_split($isbn_10));
			$x_13 = 0;
			for($i=1;$i<sizeof($digits)+1;$i++){
				if($i%2!=0){
					$x_13 += $digits[$i-1];
					//echo $digits[$i-1]."=>".$x_13."<br>";
				}else{
					$x_13 += 3*$digits[$i-1];
					//echo "3*".$digits[$i-1]."=>".$x_13."<br>";
				}
			}
			$x_13 = 10-($x_13%10);
			$isbn_13 = $isbn_10.$x_13;
			return $isbn_13;
		}
		public function xml_request($url){
			libxml_use_internal_errors(true);
			$file_headers = @get_headers($url);
			if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
				$response = 0;
			}else{
				$response = simplexml_load_file($url);
			}
			return $response;
		}
		public function sendSms($phone,$sms_message){
			$apikey = "fPKIQEAuh0q6D3JeZBTENw";
			$apisender = "BKCHOR";
			$curl = curl_init();
			$sms_message = rawurlencode($sms_message);
			$url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey='.$apikey.'&senderid='.$apisender.'&channel=2&DCS=0&flashsms=0&number='.$phone.'&text='.$sms_message.'&route=1';
			$ch=curl_init($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,"");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,2);
			$data = curl_exec($ch);
		}
	}
?>