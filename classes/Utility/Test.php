<?php
	namespace Utility;
	use Connection\Connection;
	class Test{
		public function __construct(){
			$con = new Connection();
			$this->utility = new \Utility\Utility();
			$this->mongo_bookchor = $con->mongo_bookchor();
			$this->main_server_mongo_bookchor = $con->main_server_mongo_bookchor();
			$this->main_server_mongo_bookchor_bf = $con->main_server_mongo_bookchor_bf();
		}
		public function goodreadsCategoryRating($start,$end){
			$query = $this->main_server_mongo_bookchor->product->find(['$and'=>[['mysql_product_id'=>['$gte'=>$start]],['mysql_product_id'=>['$lte'=>$end]],['details.goodreads'=>['$exists'=>true]],['details.goodreads'=>['$not'=>['$size'=>0]]],['product_type_id'=>1]]],['projection'=>['ean'=>1,'mysql_product_id'=>1],'sort'=>['mysql_product_id'=>1]]);
			$data = iterator_to_array($query);
			foreach($data as $values){
				$ean = $values['ean'];
				$product_url = 'https://www.goodreads.com/book/isbn/'.$ean.'?key=miP8pLw9o158giwm9UA';
				$xml = $this->utility->xml_request($product_url);
				if($xml){
					$tag_arr = [];
					$book = $xml->children();
					for($i=0;$i<count($book->book->popular_shelves->shelf);$i++){
						$count = $book->book->popular_shelves->shelf[$i]['count'];
						$g_cat = $book->book->popular_shelves->shelf[$i]['name'];
						//$g_cat = str_replace("-"," ",$g_cat);
						$tags = ucwords(str_replace('-',' ',$g_cat));	
						$query_tags = $this->mongo_bookchor->tags->find(['tag'=>$tags,'type_id'=>1],['limit'=>1]);
						$tags_info = iterator_to_array($query_tags);
						if(isset($tags_info[0])){
							$tag_arr[] = [
								'_id'=>$tags_info[0]['id'],
								'name'=>$tags_info[0]['tag'],
								'count'=>(int)$count
							];
						}
					}
					$query = $this->main_server_mongo_bookchor->product->updateOne(['ean'=>$ean],['$set'=>['tags'=>$tag_arr]]);
				}
				echo $values['mysql_product_id'].'<br>';
			}
		}
		public function ebookFileNames(){
			$dir = "/home/bchor/public_html/library.bookchor.com/new_ebooks/";
			if (is_dir($dir)){
				if ($dh = opendir($dir)){
					while (($filename = readdir($dh)) !== false){
						if($filename=='.' || $filename== '..') continue;
						$file = trim(preg_replace('/\s*\([^}]*\)/', '', $filename));
						$file=str_replace(["-", "–"], '', $file);
						$file=str_replace(' ', '', $file);
						echo $file."<br>";
						$file = pathinfo($file, PATHINFO_FILENAME) . '.' . strtolower(pathinfo($file, PATHINFO_EXTENSION));
						rename($dir.$filename,$dir.$file);
					}
					closedir($dh);
				}
			}else{
				echo 'not';
			}
		}
		public function productModify(){
            $pipeline=[
                /* [
					'$match' => [
						'details.author' => [
							'$not' => [
								'$size' => 0
							]
						],
						'details.authors' => [
							'$exists' => false
						]
					]
				], */
				[
                    '$sort'=>[
                        'mysql_product_id'=>-1
                    ]
                ],
                [
                    '$limit'=>100000
                ],
                [
                    '$lookup' => [
                        'from' => 'author',
                        'localField' => 'details.author',
                        'foreignField' => '_id',
                        'as' => 'author_data'
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'tags',
                        'localField' => 'tag_id',
                        'foreignField' => '_id',
                        'as' => 'tags_arr'
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'category',
                        'localField' => 'category_id',
                        'foreignField' => '_id',
                        'as' => 'category_arr'
                    ]
                ],
            ];
            $result=$this->mongo_bookchor->product->aggregate($pipeline);
            $data=iterator_to_array($result);
            $product_data = [];
            foreach ($data as $value) {
                $product_data = [];
                $product_id = new \MongoDB\BSON\ObjectId((string)$value['_id']);
                foreach ($value['author_data'] as $k => $v) {
                    $product_data['details.authors'][] = [
                        '_id'=> $v['_id'],
                        'name'=> $v['name']
                    ];
                }
                if(isset($value['tags_arr'])&&$value['tags_arr']){
                    foreach ($value['tags_arr'] as $k => $v) {
                        $product_data['tags'][] = [
                            '_id'=>$v['id'],
                            'name'=>$v['tag'],
                        ];
                        if(isset($product_data['keyword_tags'])){
                            $product_data['keyword_tags'] = $product_data['keyword_tags'].' '.$v['tag']; 
                        }else{
                            $product_data['keyword_tags'] = $v['tag'];
                        }
                    }
                }
                if(isset($value['category_arr'])&&$value['category_arr']) {
                    foreach ($value['category_arr'] as $k => $v) {
                        $product_data['category'][] = [
                            '_id' => $v['id'],
                            'name'=> $v['name']
                        ];
                        if(isset($product_data['keyword_category'])){
                            $product_data['keyword_category'] = $product_data['keyword_category'].' '.$v['name'];
                        }else{
                            $product_data['keyword_category'] = $v['name'];
                        }
                    }
                }
                if($product_data){
                    $update_arr =  array(
                            '$set' => $product_data
                    );
                    $query_modify = $this->mongo_bookchor->product->updateOne(["_id" => $product_id],$update_arr);
                    if($query_modify->getModifiedCount()){
                        echo $product_id.'<br>';
                    }
                }
            }
            // $query_unset = $this->mongo_bookchor->product->updateMany([],['$unset'=>['category_id'=>true,'tag_id'=>true,'details.publisher_id'=>true]]);
        }
		public function addProduct(){
			$skip = 1100000;
			$limit = 10000;
			for($i=0;$i<100;$i++){
				$query_all_products = $this->mongo_bookchor->product->find([],['skip'=>$skip,'limit'=>$limit]);	
				$all_products = iterator_to_array($query_all_products);
				//print_r($all_products);die;
				$query_insert = $this->main_server_mongo_bookchor->product->insertMany($all_products);
				$check = $query_insert->getInsertedCount();
				if($check){
					if($check==10000){
						echo $skip.'|||||';
					}else{
						echo $skip.'Error|||||';
					}
					$skip = $skip + $limit;
				}
			}
			echo 'Final : '.$skip;
		}
		public function addAuthor(){
			$skip = 0;
			$limit = 10000;
			for($i=0;$i<100;$i++){
				$query_all_products = $this->mongo_bookchor->author->find([],['skip'=>$skip,'limit'=>$limit]);	
				$all_products = iterator_to_array($query_all_products);
				//print_r($all_products);die;
				$query_insert = $this->main_server_mongo_bookchor->author->insertMany($all_products);
				$check = $query_insert->getInsertedCount();
				if($check){
					if($check==10000){
						echo $skip.'|||||';
					}else{
						echo $skip.'Error|||||';
					}
					$skip = $skip + $limit;
				}
			}
			echo 'Final : '.$skip;
		}
		public function addPublisher(){
			$skip = 0;
			$limit = 10000;
			for($i=0;$i<10;$i++){
				$query_all_products = $this->mongo_bookchor->publisher->find([],['skip'=>$skip,'limit'=>$limit]);	
				$all_products = iterator_to_array($query_all_products);
				//print_r($all_products);die;
				$query_insert = $this->main_server_mongo_bookchor->publisher->insertMany($all_products);
				$check = $query_insert->getInsertedCount();
				if($check){
					if($check==10000){
						echo $skip.'|||||';
					}else{
						echo $skip.'Error|||||';
					}
					$skip = $skip + $limit;
				}
			}
			echo 'Final : '.$skip;
		}
		public function bookfair_live_correct(){
			$this->main_server_mongo_bookchor_bf->bookfair_sell->updateMany(['bookfair_id'=>['$ne'=>35]],['$set'=>['live'=>1]]);
		
		}
	}
?>