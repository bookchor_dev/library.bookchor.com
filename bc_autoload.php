<?php
	define('SITE_ROOT', __DIR__);
	require_once(SITE_ROOT.'/classes/MongoDB/autoload.php');
	spl_autoload_register(function($className) {
		$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
		include_once $_SERVER['DOCUMENT_ROOT'] . '/classes/' . $className . '.php';
	});
?>