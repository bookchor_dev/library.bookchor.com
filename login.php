<?php
	require_once 'vendor/autoload.php';
	require_once('library_classes/Connection/Connection.php');
	require_once('library_classes/Utility/Utility.php');
	require_once('library_classes/User/User.php');
	require_once('library_classes/User/Login.php');
	$user = new User();
	if(isset($_REQUEST['credentials'])){
		$login = new Login();
		$lg_flag = $login->login($_REQUEST['credentials'],$user->token);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
		<link rel="stylesheet" href="/assets/css/login.css?v=<?php echo $user->user['cache'];?>">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row ">
				<div class="col-md-6 col-lg-7 col-xl-7 pl-0">
					<img src="/assets/images/landing page banner.png" class="display_img mx-auto img-fluid w-100" alt="logo">
					<h5 class="title">MY SMART LIBRARY</h5>
				</div>
				<div class=" col-md-6 col-lg-5 col-xl-5 mt-5">
					<div class="mobile">
						<img src="/assets/images/Tab_iMAGE.svg" class="tab_img mx-auto img-fluid pt-1" alt="logo" style="width:100%;">
					</div>
					<div class="main-content">
						<center><a href="https://www.bookchor.com"><img src="https://www.bookchor.com/assets/frontend/layout/img/logos/book-chor-logo-256.png" class="mx-auto img-fluid" alt="logo" style="height:120px;"></a></center>
						<div class="card ">
							<div class="card-body">
								
								<div class="container">
									<div class="my-5 toggle_option">
										<ul class="nav nav-pills" role="tablist">
											<li class="nav-item ml-0">
												<a class="nav-link active" data-toggle="pill" href="#credlogin">Credential Login</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="pill" href="#biometric">Biometric Login</a>
											</li>
										</ul>
									</div>
									<div class="tab-content">
										<div class="container tab-pane active" id="credlogin">
											<form class="form" method="POST">
												<div class="form-group">
													<label for="exampleInputEmail1">Username</label>
													<input type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name='credentials[username]' placeholder="Enter username...">
												</div>
												<br>
												<div class="form-group">
													<label for="exampleInputPassword1">Password</label>
													<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name='credentials[password]'>
													<!--<a href="#">
														<small id="emailHelp" class="form-text ">FORGET PASSWORD ?</small>
													</a>-->
												</div>
												<br>
												<div class="text-center">
													<button type="submit" class="btn ">Login</button>
													<?php if(isset($lg_flag)){
														if(!$lg_flag){?>
													<br><br>
													<span class="text-danger">Wrong Credentials</span>
													<?php }}?>
												</div>
												<br>
											</form>
										</div>
										<div class="container tab-pane" id="biometric">
												<div class="form-group mt-5">
													<label for="biometricusername">Username</label>
													<input type="name" class="form-control input_inline" id="biometricusername" name="user_name" aria-describedby="emailHelp" placeholder="Enter username...">
												</div>
												<br />
												<div class="form-group mt-5">
													<img src="/assets/images/Touch_ID.png" alt="Touc ID" name="fingerprint" width="48" height="48" id="" class="fingerprint biometricmatch" accept="image/*" >
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="/assets/js/jquery.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/popper.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/jquery.validate.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/login.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/mfs100-9.0.2.6.js?v=<?php echo $user->user['cache'];?>"></script>
	<script>
		var isotemplate = '';
		var ajaxcheck = true;
		$('.biometricmatch').on('click',function(e){
				var username = $('#biometricusername').val();
				if((username.length>2)&&(ajaxcheck==true)){
					ajaxcheck = false;
					$.post("/assets/ajax/get_fingerprint.php",{username:username},function(data){
						var response = JSON.parse(data);
						if(response.fp==1){
							isotemplate = response.hash;
							Match(username);
						}else{
							ajaxcheck = true;
						}
					});
				}
		});
		
		function Match(username){
			var quality = 60; //(1 to 100) (recommanded minimum 55)
			var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
			try {
				var res = MatchFinger(quality, timeout, isotemplate);
				
				if (res.httpStaus) {
					if (res.data.Status) {
						alert("Finger matched");
						$.post("/assets/ajax/login_biometric.php",{username:username},function(data){
							var response = JSON.parse(data);
							var url = response.url;
							window.location = url;
						});
					}
					else {
						ajaxcheck = true;
						if (res.data.ErrorCode != "0") {
							alert(res.data.ErrorDescription);
						}
						else {
							alert("Finger not matched");
						}
					}
				}
				else {
					ajaxcheck = true;
					alert(res.err);
				}
			}
			catch (e) {
				ajaxcheck = true;
				alert(e);
			}
			return false;
			
		}
	</script>
</body>

</html>