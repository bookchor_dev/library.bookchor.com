<?php
	class Product{
		public function __construct(){
				$con = new Connections();
				$this->mongo_bookchor_merchant = $con->mongo_bookchor_merchant();
				$this->mongo_bookchor=$con->mongo_bookchor();
				$this->mongo_bookchor_test=$con->mongo_bookchor_test();
				$this->mongo_bookchor_product = $con->mongo_bookchor_product();
				$this->timestamp = (new DateTime())->getTimestamp();
		}
		public function getallProductType(){
			$get_product_type=$this->mongo_bookchor_product->bookchor_product_type->find(['status'=>1]);
			$product_type_array = iterator_to_array($get_product_type);
			return $product_type_array;
		}
		public function getallProductCondition(){
			$get_product_type=$this->mongo_bookchor_product->bookchor_product_condition->find(['status'=>1]);
			$product_type_array = iterator_to_array($get_product_type);
			return $product_type_array;
		}
		public function get_product_condition($id){
			$id = new MongoDB\BSON\ObjectId($id);
			$product_condition=$this->mongo_bookchor_product->bookchor_product_type->aggregate(
				array(
					     array( '$match' => array('_id' => $id)),
					       array('$lookup' => array(
					         'from' => 'bookchor_product_condition',
					         'localField' => 'condition_id',
					         'foreignField' => '_id',
					         'as' => 'condition'
					       ))));
			$product_condition=iterator_to_array($product_condition);
			return $product_condition;
		}
		public function getProducts(){
			$pipeline=[
				[	
					'$lookup'=>[
						'from'=>'bookchor_product_condition',
						'localField'=> 'condition_id',
						'foreignField'=> '_id',
						'as'=> 'condition'	
					]
				],
				[
					'$project'=>[
						'condition_id'=>0
					]
				]
			];
			$query_product_type = $this->mongo_bookchor_product->bookchor_product_type->aggregate($pipeline);
			$product_types = iterator_to_array($query_product_type);
			return $product_types;
		}
		public function product_search($search,$increment=0){
			$search['category']='5ae834807ae0d9538e45ab45';
			if(isset($search['skip'])){
				$increment = $search['skip'];
			}
			if($search['input']){
				$regex_search = new MongoDB\BSON\Regex ($search['input']);
				$condition=array(
							   array('$lookup' => array(
								 'from' => 'author',
								 'localField' => 'detail.author_id',
								 'foreignField' => '_id',
								 'as' => 'user'
							   )),
							   array( '$unwind' => array( 
								 'path' => '$user', 'preserveNullAndEmptyArrays' => True
							   )),
							   array( '$match' => array(
								'$and' => array(
											array('product_type_id' =>new MongoDB\BSON\ObjectId($search['category'])),
											array('$or' =>
													 array(
															array('title'=> array('$regex' => $regex_search)),
															array('user.name'=> array('$regex' => $regex_search)),
															array('detail.isbn'=> array('$regex' => $regex_search)),
															array('detail.isbn_10'=> array('$regex' => $regex_search)),
														 ))
							   ))),
							   array('$skip'=>(int)$increment),
							   array('$limit'=>10)
						   );
				$product_details=$this->mongo_bookchor_product->product->aggregate($condition);
				$product_details_array = iterator_to_array($product_details);
				return $product_details_array;
			}
		}
		
		/*Fetch Product Details For merchant.bookchor.com*/
		public function getMerchantProductDetail($product_arr){
			$query_product_detail = $this->mongo_bookchor_product->product->aggregate([
				[
					'$match'=>['_id'=>$product_arr['product_id']],
				],
				[
					'$lookup'=>[
						'from'=>'bookchor_product_type',
						'localField'=>'product_type_id',
						'foreignField'=>'_id',
						'as'=>'product_type'
					]	
				],
				[
					'$lookup'=>[
						'from'=>'bookchor_product_condition',
						'localField'=>'product_type.condition_id',
						'foreignField'=>'_id',
						'as'=>'product_condition'
					]
				],
				[
					'$project'=>[
						'title'=>1,
						'ean'=>1,
						'image'=>1,
						'mrp'=>'$detail.mrp',
						'description'=>'$detail.description',
						'product_type'=>['$arrayElemAt'=>['$product_type.name',0]],
						'product_condition'=>1,
					
					]
				]
			]);
			$product_details = (array) iterator_to_array($query_product_detail);
			return $product_details[0];
		}
		public function getProductDetail($product_arr){
			$query_product_detail = $this->mongo_bookchor_product->product->find($product_arr);
			$product_details = (array) iterator_to_array($query_product_detail);
			return $product_details;
		}
		
		/*Fetch Product Details For Bookchor.com*/
		public function getVendorProducts(){
			$product_id = new MongoDB\BSON\ObjectId('5b9f6c0ac4407821e75284ec');
			$query_product_detail = $this->mongo_bookchor_product->product->aggregate([
				[
					'$match'=>['_id'=>$product_id],
				],
				[
					'$lookup'=>[
						'from'=>'vendor_product',
						'let'=>['pid'=>'$_id'],
						'pipeline'=>[
							[
								'$match'=>['$expr'=>['$eq'=>['$product_id','$$pid']],'status'=>1],
							],
							[
								'$lookup'=>[
									'from'=>'bookchor_product_condition',
									'localField'=>'condition_id',
									'foreignField'=>'_id',
									'as'=>'condition'
								]
							],
							[
								'$project'=>[
									'merchant_id'=>1,
									'condition'=>['$arrayElemAt'=>['$condition.type',0]],
									'condition_id'=>1,
									'quantity'=>1,
									'selling_price'=>['$arrayElemAt'=>['$sell.selling_price',-1]],
									'shipping_charge'=>['$arrayElemAt'=>['$sell.shipping_price',-1]],
									'status'=>['$arrayElemAt'=>['$sell.status',-1]],
								]
							],
							[
								'$sort'=>['selling_price'=>1]
							],
							[
								'$group'=>[
									'_id'=>'$merchant_id',
									'vendor_product'=>['$push'=>'$$ROOT'],
								]
							],
							[
								'$sort'=>['vendor_product.selling_price'=>1]
							],
						],
						'as'=>'vendor_product'
					]
				],
			]);
			$vendor_product_detail = iterator_to_array($query_product_detail);
			//print_r($vendor_product_detail[0]['vendor_product']);die;
			foreach($vendor_product_detail[0]['vendor_product'] as $key=>$vendor_product){
				$vendor[] = $vendor_product['_id'];
			}
			$query_vendor = $this->mongo_bookchor_merchant->merchant->find(['_id'=>['$in'=>$vendor]],['projection'=>['rating'=>1,'store_details.display_name'=>1]]);
			$vendor_data = iterator_to_array($query_vendor);
			foreach($vendor_product_detail[0]['vendor_product'] as $key=>$vendor_product){
				foreach($vendor_data as $vendor_dt){
					if($vendor_product['_id']==$vendor_dt['_id']){
						$vendor_product_detail[0]['vendor_product'][$key]['merchant_name'] = $vendor_dt['store_details']['display_name']; 
						$vendor_product_detail[0]['vendor_product'][$key]['rating'] = 5;//$vendor_dt['rating']; 
					}
				}
			}
			//print_r($vendor_product_detail);die;
			return $vendor_product_detail[0];
		}
		public function categoryListing($category_arr){
			$query_category_listing =  $this->mongo_bookchor_product->product->aggregate([
				[
					'$match'=>['category.category_id'=>$category_id],
				],
				[
					'$skip'=>$category_arr['doc_id'],
				],
				[
					'$limit'=>$category_arr['limit'],
				],
				[
					'$lookup'=>[
						'from'=>'',
					
					]
				]
			]);
		
		}
		
		public function get_all_product_condition(){
			$conditions=$this->mongo_bookchor_product->bookchor_product_condition->find();	
			$result=iterator_to_array($conditions);
			return $result;
		}
		public function get_product_stats($mode){
			$pipeline=[
							[
								'$group'=>
									 [
									 	'_id'=>'$product_id',
									 	'minPrice'=> ['$min'=> '$sell.selling_price'],
									 	'count'=>
									 			[
									 				'$sum'=> 1,
									 			],
									 ]
							],
							['$lookup'=> ['from'=> 'product','localField'=> '_id','foreignField'=> '_id','as'=> 'data']],
							['$unwind'=>'$minPrice'],
							[
								'$unwind'=> 
									[
            							'path'=> '$data',
           								'preserveNullAndEmptyArrays'=> true
        							]
							],
							['$lookup'=> ['from'=> 'bookchor_product_type','localField'=> 'data.product_type_id','foreignField'=> '_id','as'=> 'type']],
							['$unwind'=>'$type'],
							[
								'$sort'=> 
									[
										'count'=> (int)$mode,
									]
							],
							[
								'$limit'=> 5
							],
							[
								'$project'=>
								[
									'minPrice'=>1,
									'count'=>1,
									'data'=>1,
									'type.name'=>1
								]

							]

					];
			$data=$this->mongo_bookchor_product->vendor_product->aggregate($pipeline);
			$result=iterator_to_array($data);
			return $result;
		}
		public function graph_stats($start_date,$end_date){
			$pipeline=[
						['$match'=>['date_added'=> ['$lte'=>(int)$end_date,'$gte'=> (int)$start_date ]]],
						['$lookup'=>['from'=> 'product',  'localField'=> 'product_id', 'foreignField'=> '_id',  'as'=> 'data']
						],
						['$lookup'=>['from'=> 'bookchor_product_type','localField'=> 'data.product_type_id','foreignField'=> '_id','as'=> 'type']],
						['$unwind'=>'$type'],
						['$unwind'=>'$data'],
						[
								'$group'=>
									 [
									 	'_id'=>['product_type_id'=>'$data.product_type_id','name'=>'$type.name'],
									 	'count'=>
									 			[
									 				'$sum'=> 1
									 			],
									 ],
						],
						[
							'$project'=>[
											'data'=>'$_id',
											'_id'=>0,
											'count'=>1
										]	
						]
				];
			$data=$this->mongo_bookchor_product->vendor_product->aggregate($pipeline);
			$result=iterator_to_array($data);
			return $result;
		}
		public function search($data){
			$input=array('search'=>'search string','range'=>array('lower_limit'=>100,'upper_limit'=>200));
			$id=new MongoDB\BSON\ObjectId('5c16ca8040c1c428c4674942');
			$pipeline=
			[
     			[
     					'$match'=>
     							[
     								'$and'=>$data
	     						 		
     							]
     			],
     			[
     					'$sort'=>
     								[
     									'score'=>['$meta'=> "textScore" ] 
     								]
     			],
     			[
     					'$project'=> 
     								[
     									'title'=> 1,
     									'mrp'=>1,
     									'_id'=> 0,
     								]
     			],
     			[
     				'$limit'=>5
     			]

    		];
			$data=$this->mongo_bookchor_test->product->aggregate($pipeline);
			$result=iterator_to_array($data);
			return $result;
		}


	}
?>

