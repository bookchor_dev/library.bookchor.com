<?php
	class Test{
		public function __construct(){
			$con = new Connection();
			$this->mongo_bookchor = $con->mongodb();
			$this->main_db = $con->main_db();
			$this->timestamp = (new DateTime())->getTimestamp();
		}
		public function __destruct(){
			$this->main_db->close();
		}
		public function flipkartScrap($isbn){
			$url='https://www.flipkart.com/fg/p/itmdygexstefdz3x?pid='.$isbn.'&lid=LSTBOK9781428897656SD4YMI&marketplace=FLIPKART&srno=s_1_1&otracker=search&fm=SEARCH&iid=e3d880bb-2379-474e-b273-f154c0020c90.9781428897656.SEARCH&ppt=Homepage&ppn=Homepage&ssid=j3njhr83i80000001550040068087&qH=6496117322fd76fa';
			$data=file_get_contents($url);
			$title = '/<span class="_35KyD6">(.*?)<\/span>/';
			preg_match_all($title,$data,$value);
			if(empty($value[1])){
				return '{"error":1}';
			}else{
				$author='/<div class="_3cpW1u">(.*?)<\/div>/';
				preg_match_all($author,$data,$value1);
				if(isset($value1[1][0])){
					$author=strip_tags($value1[1][0]);	
					if($author=='unknown'){
						$author='';
					}
				}else{
					$author='';
				}
				$description='';
				$n1='Summary Of The Book';
				$n2='About the Author';
				$n3='\n\n';
				$doc = new DOMDocument();
				@$doc->loadHTML($data);
				$classname = "_3la3Fn";
				$finder = new DomXPath($doc);
				$tmp_dom = new DOMDocument();
				$innerHTML='';
				$spanner = $finder->query("//div[contains(@class, '_3la3Fn')]");
				foreach ($spanner as $node) {
					$tmp_dom->appendChild($tmp_dom->importNode($node,true));
				}
				$description.=trim($tmp_dom->saveHTML()); 
				$description = trim(preg_replace('/\s+/', ' ', $description));
				if(isset($description)){
					$pos1 = stripos($description, $n1);
					if($pos1 > 0){
						$description = substr($description,$pos1);
						$description = str_ireplace($n1,'',$description);
					}
					$about_author = '/<strong>About(.*?)<\/strong>/';
					preg_match($about_author,$description,$match, PREG_OFFSET_CAPTURE);
					if(isset($match[0][1])){
						$description = substr($description,0,$match[0][1]);
					}
					$about_author_bold = '/<b>About(.*?)<\/b>/';
					preg_match($about_author_bold,$description,$match, PREG_OFFSET_CAPTURE);
					if(isset($match[0][1])){
						$description = substr($description,0,$match[0][1]);
					}else{
						$description = $description;
					}
					$description=trim(strip_tags($description));
				}else{
					$description='';
				}
				$genre = '/<li class="_2-riNZ">Genre:(.*?)<\/li>/';
				preg_match_all($genre,$data,$value2);
				if(isset($value2[1][0])){
					$genre=$value2[1][0];
				}else{
					$genre='';
				}
				$spanner = $finder->query("//div[contains(@class, '_3qQ9m1')]");
				foreach ($spanner as $entry) {
					$selling_price=$entry->nodeValue;
				}
				$spanner = $finder->query("//div[contains(@class, '_1POkHg')]");
				foreach ($spanner as $entry) {
					$mrp=$entry->nodeValue;
				}
				if(!isset($selling_price)){
					$selling_price='';
				}else{
					$selling_price = preg_replace('/[^0-9]/', '', $selling_price);
				}
				if(!isset($mrp)){
					$mrp='';
				}else{
					$mrp = preg_replace('/[^0-9]/', '', $mrp);
				}
				$highlight='/<div class="_3WHvuP"><ul>(.*?)<\/ul><\/div>/';
				preg_match_all($highlight, $data, $value_highlight);
				preg_match_all('/<li class="_2-riNZ">(.*?)<\/li>/', $value_highlight[1][0],$match);
				$highlights=[];
				if(isset($match)){
					foreach ($match[1] as $key => $value) {
						$content=explode(':', $value);
						$highlights[$content[0]]=trim(mb_convert_encoding($content[1], "UTF-8", "HTML-ENTITIES"));
					}
				}
				$specifications='';
				$tmp_dom = new DOMDocument();
				$innerHTML='';
				$spanner = $finder->query("//div[contains(@class, 'MocXoX')]");
				foreach ($spanner as $node) {
					$tmp_dom->appendChild($tmp_dom->importNode($node,true));
				}
				$innerHTML.=trim($tmp_dom->saveHTML()); 
				$innerHTML = trim(preg_replace('/\s+/', ' ', $innerHTML));
				if(isset($innerHTML)){
					preg_match_all('/<td class="_2k4JXJ col col-9-12">(.*?)<\/td>/', $innerHTML,$sp_value);
					preg_match_all('/<td class="_3-wDH3 col col-3-12">(.*?)<td class="_2k4JXJ col col-9-12">/', $innerHTML,$sp_head);
					foreach ($sp_head[1] as $key => $value) {
							$value=strip_tags($value);
							$specifications[$value]=strip_tags($sp_value[1][$key]);
						}
				}else{
					$specifications='';
				}
				$script_data='';
				$script_d = $finder->query('//script[contains(@id, "jsonLD")]'); 
				foreach($script_d as $tag) {
					$script_data=json_decode($tag->nodeValue,TRUE);
				}
				if(isset($script_data[0]['name'])){
					$title=$script_data[0]['name'];
				}else{
					$title='';
				}
				$breadcrumbs=[];
				if(isset($script_data[1]['itemListElement'])){
					$breadcrumbs_data=$script_data[1]['itemListElement'];
					foreach ($breadcrumbs_data as $key => $value) {
						$breadcrumbs[]=$value['item']['name'];
					}
				}
				$rating_value=0;
				$review_count=0;
				if(isset($script_data[0]['aggregateRating']['ratingValue'])){
					$rating_value=$script_data[0]['aggregateRating']['ratingValue'];
				}
				if(isset($script_data[0]['aggregateRating']['reviewCount'])) {
					$review_count=$script_data[0]['aggregateRating']['reviewCount'];
				}
				$image=[];
				$image_match = '/<div class="_2_AcLJ"(.*?)<\/div>/';
				preg_match_all($image_match,$data,$image_data);
				if(!empty($image_data[1])) {
					foreach ($image_data[1] as $key => $value) {
						$value=str_replace('128/128','800/800',$value);
						preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $value, $match);
						$image[]=$match[0][0];
					}
				}else if(isset($script_data[0]['image'])){
					$image[]=str_replace('128/128','800/800',$script_data[0]['image']);
				}
				$related_isbn=[];
				$related_isbn_data = $finder->query('//a[contains(@class, "Zhf2z")]');
				if(isset($related_isbn_data)){
					foreach ($related_isbn_data as $key => $value) {
						$data=$value->getAttributeNode('href')->nodeValue;
						$rel_match = '/pid(.*?)lid/';
						preg_match_all($rel_match,$data,$isbn_strings);
						if(!empty($isbn_strings[0][0])) {
							preg_match_all('!\d+!', $isbn_strings[0][0], $matches);
							$related_isbn[]=$matches[0][0];
						}
					}
				}
				$isbn_10=[];
				$isbn_13=[];
				$data=[];
				$data['title']=mb_convert_encoding($title, "UTF-8", "HTML-ENTITIES");
				$data['author']=mb_convert_encoding($author, "UTF-8", "HTML-ENTITIES");
				$data['genre']=trim(mb_convert_encoding($genre, "UTF-8", "HTML-ENTITIES"));
				$data['description']=html_entity_decode($description);
				$data['selling_price']=$selling_price;
				$data['mrp']=$mrp;
				$data['highlights']=$highlights;
				if($data['highlights']['ISBN']){
					$isbns=explode(',', $data['highlights']['ISBN']);
					foreach ($isbns as $key => $value) {
						$value=trim($value);
						if(strlen($value)==13){
							$isbn_13[]=$value;
						}else if(strlen($value)==10){
							$isbn_10[]=$value;
						}
					}
				}
				$data['highlights']['ISBN']=array('isbn_13'=>$isbn_13,'isbn_10'=>$isbn_10);
				$data['specifications']=$specifications;
				$data['breadcrumbs']=$breadcrumbs;
				$data['image']=$image;
				$data['rating_value']=$rating_value;
				$data['review_count']=$review_count;
				$data['related_isbn']=$related_isbn;
				return json_encode($data);
			}
		}
		public function flipkartData($start,$end){
			$query_mongo = $this->mongo_bookchor->product->find(['$and'=>[['mysql_product_id'=>['$gt'=>$start]],['mysql_product_id'=>['$lte'=>$end]]]]);
			$data_mongo = iterator_to_array($query_mongo);
			foreach($data_mongo as $values){
				$isbn = $values['ean'];
				$data = json_decode($this->flipkartScrap($isbn));
				if(!isset($data->error)){
					$pid = $values['_id'];
					$product_id = $values['mysql_product_id'];
					$description = $values['description'];
					$pages = $values['details']['pages'];
					$mrp = $values['mrp'];
					$language_id = $values['details']['language_id'];
					$binding_id = $values['details']['binding_id'];
					$edition = $values['details']['edition'];
					$similar_books = (array) $values['similar_product'];
					if($language_id == new MongoDB\BSON\ObjectId('5c51477e18ba852ce4ea64c8')&&isset($data->highlights->Language)){
						$language = $data->highlights->Language;
						$query_language_regex = $this->mongo_bookchor->product_languages->find(['$or'=>[['$text'=>['$search'=>$language]],['codes'=>$language]]]);
						$language_info = iterator_to_array($query_language_regex);
						if(isset($language_info[0])){
							$language_id = $language_info[0]['_id'];
						}
					}
					if($binding_id == new MongoDB\BSON\ObjectId('5c52a476c86b0c311488c701')&&isset($data->highlights->Binding)){
						$binding = $data->highlights->Binding;
						$query_binding = $this->mongo_bookchor->product_binding->find(['$or'=>[['$text'=>['$search'=>$binding]],['codes'=>$binding]]]);
						$binding_info = iterator_to_array($query_binding);
						if(isset($binding_info[0])){
							$binding_id = $binding_info[0]['_id'];
						}
					}
					if($edition==''&&isset($data->highlights->Edition)){
						$edition = $data->highlights->Edition;
					}
					if(($values['description']==''||strlen($values['description'])<strlen($data->description))&&isset($data->description)){
						$description = utf8_encode($data->description);
					}
					if(($pages == '0'||$pages == '')&&isset($data->pages)){
						$pages = $data->pages;
					}
					if(isset($data->mrp)&&$data->mrp!=''){
						$mrp = (int) $data->mrp;
					}
					if(sizeof($data->related_isbn)>0){
						for($i=0;$i<sizeof($data->related_isbn);$i++){
							if(!in_array($data->related_isbn[$i],$similar_books)){
								$similar_books[] = $data->related_isbn[$i];
							}
						}
					}
					$flipkart = new stdClass();
					if($data->rating_value!=0){
						$flipkart->rating = (int)$data->rating_value;
					}
					if($data->review_count!=0){
						$flipkart->review = (int)$data->review_count;
					}
					if($data->selling_price!=0){
						$flipkart->selling_price = (int) $data->selling_price;
					}
					$product_update_arr = [
						'description'=>$description,
						'mrp'=>$mrp,
						'details.pages'=>$pages,
						'details.language_id'=>$language_id,
						'details.binding_id'=>$binding_id,
						'details.edition'=>$edition,
						'similar_product'=>$similar_books
					];
					if($flipkart){
						$product_update_arr['details.flipkart'] = $flipkart;
					}
					$this->mongo_bookchor->product->updateOne(['_id'=>$pid],['$set'=>$product_update_arr]);
					echo $product_id."<br>";
				}
			}
		}
		public function productDescriptionScrap(){
			$query_mongo = $this->mongo_bookchor->product->find(['details.pages'=>''],['mysql_product_id'=>1,'ean'=>1],['$limit'=>500]);
			$query_mongo = iterator_to_array($query_mongo);
			foreach($query_mongo as $data_mongo){
				$isbn = $data_mongo['ean'];
				$data = $this->open_library($isbn);
				$pages = 0;
				if(isset($data['ISBN:'.$isbn.'']['details']['number_of_pages'])){
					$pages = $data['ISBN:'.$isbn.'']['details']['number_of_pages'];
				}
				if($pages){
					$query_mongo = $this->mongo_bookchor->product->updateOne(['ean'=>$isbn],['$set'=>['details.pages'=>(string)$pages]]);
					$query_update = "UPDATE product set pages='$pages' where isbn='$isbn'";
					$stmt_update = $this->main_db->prepare($query_update);
					$stmt_update->execute();
					$stmt_update->close();
				}
				echo $data_mongo['mysql_product_id']."<br>";
			}
		}
		public function open_library($isbn){
			$url='https://openlibrary.org/api/books?bibkeys=ISBN:'.$isbn.'&jscmd=details&format=json';
			$data=file_get_contents($url);
			$data=json_decode($data,TRUE);
			if(count($data)>0) {
				return $data;
			}else{
				return false;
			}
		}
		public function cartAdd(){
			$query_cart = "SELECT * from cart order by id desc";
			$stmt_cart = $this->main_db->prepare($query_cart);
			$stmt_cart->execute();
			$row_cart = $stmt_cart->get_result();
			$stmt_cart->free_result();
			$stmt_cart->close();
			while($result_cart = $row_cart->fetch_array()){
				$query_check = $this->mongo_bookchor->cart->find(['id'=>(int)$result_cart['id']]);
				$query_check = iterator_to_array($query_check);
				if(!isset($query_check[0]['id'])){
					echo $result_cart['id']."<br>";
					$query_mongo = $this->mongo_bookchor->vendor_products->find(['vendor_product_id'=>(int)$result_cart['vendor_product_id']],['projection'=>['_id'=>1],'limit'=>1]);
					$query_mongo = iterator_to_array($query_mongo);
					$vendor_product_id = $query_mongo[0]['_id'];
					$cart = array(
						"id" => (int) $result_cart['id'],
						"customer_id" => (int) $result_cart['customer_id'],
						"session_id" => $result_cart['session_id'],
						"vendor_product_id" => new MongoDB\BSON\ObjectId($vendor_product_id),
						"quantity" => (int) $result_cart['quantity'],
						"date_added" => $result_cart['date_added'],
						"timestamp" => strtotime($result_cart['date_added']),
						"status" => 1
					);
					$query_mongo = $this->mongo_bookchor->cart->insertOne($cart);
				}
			}
		}
		public function vendorProductsAdd(){
			$query_vendor_product = "SELECT * from vendor_products order by id desc";
			$stmt_vendor_product = $this->main_db->prepare($query_vendor_product);
			$stmt_vendor_product->execute();
			$row_vendor_product = $stmt_vendor_product->get_result();
			$stmt_vendor_product->free_result();
			$stmt_vendor_product->close();
			while($result_vendor_product = $row_vendor_product->fetch_array()){
				$query_check = $this->mongo_bookchor->vendor_products->find(['vendor_product_id'=>(int)$result_vendor_product['id']]);
				$query_check = iterator_to_array($query_check);
				if(!isset($query_check[0]['vendor_product_id'])){
					echo $result_vendor_product['id']."<br>";
					$query_mongo = $this->mongo_bookchor->product->findOne(['mysql_product_id'=>(int)($result_vendor_product['product_id'])]);
					$mongo_product_id = $query_mongo['_id'];
					$sell = new stdClass();
					$sell->selling_price = $result_vendor_product['selling_price'];
					$sell->quantity = $result_vendor_product['quantity'];
					$vendor_product = array(
						"product_id" => new MongoDB\BSON\ObjectId($mongo_product_id),
						"vendor_id" => new MongoDB\BSON\ObjectId('5c5419e909307d3358e2e938'),
						"vendor_product_id" => $result_vendor_product['id'],
						"mysql_product_id" => $result_vendor_product['product_id'],
						"condition_id" => $result_vendor_product['condition_id'],
						"sell" => $sell,
						"date_added" => $result_vendor_product['date_added'],
						"status" => 1
					);
					$query_mongo = $this->mongo_bookchor->vendor_products->insertOne($vendor_product);
				}
			}
		}
		public function languageCorrect(){
			$query_product = "SELECT id,language from product where language!='' and date_added>'2019-02-10'";
			$stmt_product = $this->main_db->prepare($query_product);
			$stmt_product->execute();
			$row_product = $stmt_product->get_result();
			$stmt_product->free_result();
			$stmt_product->close();
			while($result_product = $row_product->fetch_array()){
				$product_id = $result_product['id'];
				$language = $result_product['language'];
				$query_language_regex = $this->mongo_bookchor->product_languages->find(['$or'=>[['$text'=>['$search'=>$language]],['codes'=>$language]]]);
				$language_info = iterator_to_array($query_language_regex);
				if(isset($language_info[0])){
					echo $product_id."<br>";
					$language_id = $language_info[0]['_id'];
					$query_update = $this->mongo_bookchor->product->updateOne(['mysql_product_id'=>(int)$product_id],['$set'=>['details.language_id'=>new MongoDB\BSON\ObjectId($language_id)]]);
				}
			}
		}
	}
?>