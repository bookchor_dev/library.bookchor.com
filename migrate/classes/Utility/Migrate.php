<?php
	class Migrate{
		public function __construct(){
			$con = new Connection();
			//$this->mongo_bookchor=$con->mongo_bookchor();
			$this->mongo_bookchor_product = $con->mongodb();
			$this->main_db = $con->main_db();
			$this->timestamp = (new DateTime())->getTimestamp();
		}
		
		public function migrateProducts(){
			$query_product = 'SELECT p.*,a.name as author,a.description as author_description,a.image as author_image,pub.name as publisher FROM product as p,author as a,publisher as pub where p.author_id=a.id AND pub.id=p.publisher_id order by p.id asc';
			$stmt_product = $this->main_db->prepare($query_product);
			$stmt_product->execute();
			$row_product = $stmt_product->get_result();
			$stmt_product->free_result();
			$stmt_product->close();
			$this->main_db->close();
			while($result_product = $row_product->fetch_array()){
				echo $result_product['id'].'<br>';
				$cat_arr = [];
				$author_array = [];
				$goodreads_total_rating = [];
				$goodreads_product_id = 0;
				$edition = null;
				$similar = [];
				$primary_work_count = "0";
				$user_position = "0";
				$publisher = $result_product['publisher'];
				$published_date = $result_product['published_date'];
				$author_info = ['author_image'=>$result_product['author_image'],'author_description'=>$result_product['author_description']];
				/*GoodReads ISBN API*/
				$product_url = 'https://www.goodreads.com/book/isbn/'.$result_product['isbn'].'?key=miP8pLw9o158giwm9UA';
				$xml = $this->xml_request($product_url);
				if($xml){
					$books = $xml->children()->book;
					$edition = utf8_encode((string) $books->edition_information);
					$format = (string) $books->format;
					$goodreads_product_id =(string) $books->id;
					/*Author*/
					if($books->authors->author){	
						foreach($books->authors->author as $author){
							$author_info['goodreads_author_id'] = (string)$author->id;
							$author_array[] = $this->goodreads_author($author_info);
						}
					}
					if((string)$books->publisher){
						$publisher = (string)$books->publisher;
					}
					if($books->publication_year&&$books->publication_month){
							$published_date = [
								'year'=>$books->publication_year,
								'month'=>$books->publication_month,
								'day'=>$books->publication_day
							];
					}	
					if($books->work->rating_dist){
						$goodreads_ratings = $books->work->rating_dist;
						$total_ratings = explode('|',$goodreads_ratings);
						foreach($total_ratings as $key=>$rating){
							$rates = explode(':',$rating);
							$goodreads_total_rating[$key]['rating'] = $rates[0];
							$goodreads_total_rating[$key]['rating_count'] = $rates[1];
						}
					}
					if($books->similar_books->book){
						foreach($books->similar_books->book as $book){
							$isbn13 = (string)$book->isbn13;
							if($isbn13){
								$similar[] = $isbn13;
							}
						}
					}
					if($books->series_work){
						$primary_work_count = (string) $books->series_work->series_work[0]->series->primary_work_count;
						$user_position = (string) $books->series_works->series_work[0]->user_position;
					}
				}else{
					$author_arr = explode(',',$result_product['author']);
					foreach($author_arr as $author_name){
						$author_info['name'] = $author_name;
						$author_array[] = $this->goodreads_author($author_info,1);
					}
				}
				
				
				/*Publisher*/
				if($publisher){
					$pub_search = new MongoDB\BSON\Regex(utf8_encode($publisher));
					$query_find_publisher = $this->mongo_bookchor_product->publisher->find(['name'=>['$regex'=>$pub_search,'$options'=>'ix']]);
					$find_publisher = iterator_to_array($query_find_publisher);
					if(isset($find_publisher[0])){
						$publisher_id = $find_publisher[0]['_id'];
					}else{
						$publisher_data = [
							'name'=>utf8_encode($publisher),
							'status'=>1
						];
						$query_publisher = $this->mongo_bookchor_product->publisher->insertOne($publisher_data);
						if($query_publisher->getInsertedCount()){
							$publisher_id = $query_publisher->getInsertedId();
						}
					}
				}
				
				/*Category Fetch*/
				/* $category_array = [];
				$tags = [];
				if(isset($books->popular_shelves->shelf)){
					foreach($books->popular_shelves->shelf as $shelf){
						$tags[] = ucwords(str_replace("-"," ",((string) $shelf['name'])));
						$shelves = ucwords(str_replace("-"," ",((string) $shelf['name'])));
						$shelves = explode(" ",$shelves);
						for($i=0;$i<sizeof($shelves);$i++){
							$temp_tags[] = $shelves[$i];
						}
					}
					for($j=0;$j<16;$j++){
						if($shelf['count']>50){
							for($i=0;$i<sizeof($tags);$i++){
								$tag_name = strtolower(preg_replace('/[^a-zA-Z0-9]/', ' ', (string)$temp_tags[$i]));
								if($tag_name == $cat[$j]['name']){
									$category_array[] = $cat[$j]['id'];
								}
							}
						}
					}
				} */
				/* $query_category_product = 'SELECT c.id,c.name as category,t.tag from category as c,tags as t,product_category_tags as pc where pc.product_id='.$result_product["id"].' 
				and pc.tag_id=t.id and pc.category_id=c.id and c.id<11';
				$stmt_category_product = $this->main_db->prepare($query_category_product);
				$stmt_category_product->execute();
				$row_category_product = $stmt_category_product->get_result();
				while($result_category_product = $row_category_product->fetch_array()){
					if(!in_array($result_category_product['id'],$category_array)){
						$category_array[] = "".$result_category_product['id'];
					}
					if(!in_array($result_category_product['tag'],$tags)){
						$tags[] = $result_category_product['tag'];
					}
				}
				$stmt_category_product->close(); */
				/* $tags = array_values(array_unique($tags));
				$category_array = array_values(array_unique($category_array)); */
				
				/*Description*/
				if(isset($result_product['description'])&&!empty($result_product['description'])){
					$description = utf8_encode($result_product['description']);
				}else{
					if(isset($books->description)){	
						$description = utf8_encode($books->description);
					}else{
						$description = '';
					}
				}
				
				/*Image*/
				$image = [];	
				if(isset($result_product['image'])&&!empty($result_product['image'])){
					$image[] = $result_product['image'];
				}else{
					if(isset($books->image_url)){
						$image[] = $books->image_url;
					}
				}
				
				/*Other Editions*/
				$other_editions = [];
				$url = 'http://www.librarything.com/api/thingISBN/'.$result_product['isbn'];
				$response = $this->xml_request($url);	
				if($response->children()->isbn){
					foreach($response->children()->isbn as $isbn){
						$other_editions[] = $this->convertIsbn13($isbn);
					}
				}
				
				$product = [
					'title'=>utf8_encode($result_product['title']),
					'ean'=>$result_product['isbn'],
					'image'=>$image,
					'mongo_product_type_id'=>new MongoDB\BSON\ObjectId('5ae834807ae0d9538e45ab45'),
					'product_type_id'=>1,
					'mrp'=>$result_product['mrp'],
					'description'=>$description,
					'weight'=>$result_product['weight'],
					'height'=>$result_product['height'],
					'width'=>$result_product['width'],
					'mysql_product_id'=>$result_product['id'],
					'goodreads_product_id'=>$goodreads_product_id,
					'other_editions'=>$other_editions,
					'details'=>[
						'author'=>$author_array,
						'isbn'=>$result_product['isbn'],
						'isbn10'=>isset($books->isbn)?((string) $books->isbn):str_replace("#","",$result_product['isbn_10']), //remove # from mysql
						'edition'=>($result_product['edition'])?$result_product['edition']:$edition,
						'pages'=>isset($books->num_pages)?(string) $books->num_pages:$result_product['pages'],
						'spine_width'=>$result_product['spine_width'],
						'binding'=>($result_product['binding'])?utf8_encode($result_product['binding']):utf8_encode($format),
						'language'=>isset($books->language_code)?(string) $books->language_code:$result_product['language'],
						'publisher_id'=>$publisher_id,
						'published_date'=>$published_date,
						'goodreads_avg_rating'=>isset($books->average_rating)?(string) $books->average_rating:'0',
						'goodreads_total_reviews'=>isset($books->work->text_reviews_count)?(string) $books->work->text_reviews_count:'0',
						'goodreads_total_rating'=>$goodreads_total_rating,
						'amazon_rank'=>$result_product['amazon_rank'],
						'category_rank'=>$result_product['category_rank'],
						'amazon_sp'=>$result_product['amazon_sp'],
						'primary_work_count'=>$primary_work_count,
						'book_work_position'=>$user_position
					],
					'similar_product'=>$similar,
					'date_added'=>$result_product['date_added'],
					'timestamp'=>$this->timestamp,
				];
				try{
					$query_product = $this->mongo_bookchor_product->product->insertOne($product);
					if($query_product->getInsertedCount()){
					}else{
						echo $result_product['id'].'||';
					}
				}catch(MongoDB\Driver\Exception\Exception $e){
					$file = fopen("error.txt","a");
					fwrite($file,$result_product['id']." ".$e."\n");
				}
			}
			$this->bindingMapping();
			$this->languageCollection();
			$this->addProductCategoryTags();
			fclose($file);
		}
		public function goodreads_author($author_info,$name_flag=0){
			/*Check if Author ID exists in MongoDB*/
			if($name_flag!=0){
				//$author_id = $author_info['goodreads_author_id'];
				$author_name_url = 'https://www.goodreads.com/api/author_url/'.$author_info['name'].'?key=GXiNhauiAEcPXadMXaH9A';
				$xml = $this->xml_request($author_name_url);
				if($xml){
					$author_id = (string) $xml->children()->author['id']; 
					$author_info['goodreads_author_id'] = $author_id;
				}
			}
			if($author_info['goodreads_author_id']){
				$query_find_author = $this->mongo_bookchor_product->author->find(['goodreads_author_id'=>$author_info['goodreads_author_id']]);
				$find_author = iterator_to_array($query_find_author);
				if(isset($find_author[0])){
					return $find_author[0]['_id'];
				}else{
					$author_id = $author_info['goodreads_author_id'];
				}
			}else{
				$author_id = $author_info['goodreads_author_id'];
			}
			if(isset($author_id)&&!empty($author_id)){
				/*Author ID API*/
				$author_url = 'https://www.goodreads.com/author/show/'.$author_id.'.xml?key=GXiNhauiAEcPXadMXaH9A';
				$xml = $this->xml_request($author_url);
				if($xml&&!isset($xml->children)){
					$author_data = $xml->children()->author;
					$author_arr = [
						'goodreads_author_id'=>$author_id,
						'name'=>(string)$author_data->name,
						'image'=>($author_data->large_image_url)?(string)$author_data->large_image_url:(string)$author_data->image_url,
						'description'=>utf8_encode((string)$author_data->about),
						'works_count'=>(string)$author_data->works_count,
					];
					if($author_data->gender){
						$author_arr['gender'] = (string)$author_data->gender;
					}
					if($author_data->hometown){
						$author_arr['hometown'] = (string)$author_data->hometown;
					}
					if($author_data->born_at){
						$author_arr['dob'] = (string)$author_data->born_at;
					}
					if($author_data->died_at){
						$author_arr['dod'] = (string)$author_data->died_at;
					}
					foreach($author_data->books->book as $book){
						$author_isbn = (string) $book->isbn13;
						if($author_isbn!=''){
							$author_arr['books'][] = $author_isbn;
						}
					}
				}
			}
			
			if(!isset($author_arr)&&!empty($author_info['name'])){
				$author_arr = [
					'goodreads_author_id'=>0,
					'name'=>utf8_encode($author_info['name']),
					'image'=>$author_info['author_image'],
					'description'=>utf8_encode($author_info['author_description']),
				];
			}
			
			
			/*Insertion into the MongoDB Collection*/
			if(!isset($author_arr)){
				$query_author = $this->mongo_bookchor_product->author->insertOne($author_arr);
				if($query_author->getInsertedCount()){
					return $query_author->getInsertedId();
				}
			}else{
				return $namongoid;
			}
		}
		public function correctAuthor(){
			$query_duplicate = $this->mongo_bookchor_product->author->aggregate([
				[
					'$group'=>[
						'_id'=>'$goodreads_author_id',
						'count'=>['$sum'=>1]
					]
				],
				[
					'$match'=>[
						'count'=>['$gt'=>1],
						'_id'=>['$ne'=>0]
					]
				]
			]);
			$duplicate_author = iterator_to_array($query_duplicate);
			foreach($duplicate_author as $author){
				$goodreads_author_id = $author['_id'];
				$query_duplicate = $this->mongo_bookchor_product->author->find(['goodreads_author_id'=>$goodreads_author_id]);
				$duplicate_entry = iterator_to_array($query_duplicate);
				$author_id = $duplicate_entry[0]['_id'];
				foreach($duplicate_entry as $entry){
					$query_updateResult = $this->mongo_bookchor_product->product->updateMany(['details.author'=>$entry['_id']],['$set'=>['details.author.$'=>$author_id]]);
					if($query_updateResult->getModifiedCount()){
						$query_author_delete = $this->mongo_bookchor_product->author->deleteOne(['_id'=>$entry['_id']]);
					}
				}
			}
		}
		public function correctPublisher(){
			$query_duplicate = $this->mongo_bookchor_product->pubisher->aggregate([
				[
					'$project'=>[
						'name'=>[
							'$trim'=>['$input'=>'$name']
						]
					]
				],
				[
					'$group'=>[
						'_id'=>'$name',
						'pub_id'=>['$addToSet'=>'$_id'],
						'count'=>['$sum'=>1],
					]
				],
				[
					'$match'=>[
						'count'=>['$gt'=>1],
					]
				]
			]);
			$duplicate_pub = iterator_to_array($query_duplicate);
			foreach($duplicate_pub as $pub){
				$duplicate_entry = $pub['pub_id'];
				$publisher_id = $duplicate_entry[0];
				unset($duplicate_entry[0]);
				foreach($duplicate_entry as $pub_id){
					$query_updateResult = $this->mongo_bookchor_product->product->updateMany(['details.publisher_id'=>$pub_id],['$set'=>['details.publisher_id'=>$publisher_id]]);
					if($query_updateResult->getModifiedCount()){
						$query_author_delete = $this->mongo_bookchor_product->publisher->deleteOne(['_id'=>$pub_id]);
					}
				}
			}
		}
		private function xml_request($url){
			$file_headers = @get_headers($url);
			if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
				$response = 0;
			}else{
				$response = simplexml_load_file($url);
			}
			return $response;
		}
		public function convertIsbn13($isbn_10){
			$isbn_10 = "978".substr($isbn_10,0,9);
			$digits  = array_map('intval', str_split($isbn_10));
			$x_13 = 0;
			for($i=1;$i<sizeof($digits)+1;$i++){
				if($i%2!=0){
					$x_13 += $digits[$i-1];
					//echo $digits[$i-1]."=>".$x_13."<br>";
				}else{
					$x_13 += 3*$digits[$i-1];
					//echo "3*".$digits[$i-1]."=>".$x_13."<br>";
				}
			}
			$x_13 = 10-($x_13%10);
			$isbn_13 = $isbn_10.$x_13;
			return $isbn_13;
		}
		public function add_mysql_tags(){
			$query_product = 'SELECT p.id FROM product as p,product_category_tags as pct WHERE pct.product_id=p.id';
			$stmt_product = $this->main_db->prepare($query_product);
			$stmt_product->execute();
			$row_product = $stmt_product->get_result();
			$stmt_product->free_result();
			$stmt_product->close();
			while($result_product = $row_product->fetch_array()){
				$query_category_product = 'SELECT c.id,c.name as category,t.tag from category as c,tags as t,product_category_tags as pc where pc.product_id='.$result_product["id"].'and pc.tag_id=t.id and pc.category_id=c.id';
				$stmt_category_product = $this->main_db->prepare($query_category_product);
				$stmt_category_product->execute();
				$row_category_product = $stmt_category_product->get_result();
				$stmt_category_product->free_result();
				$stmt_category_product->close();
				$category_array = [];
				$tags_arr = [];
				while($result_category_product = $row_category_product->fetch_array()){
					if(!in_array($result_category_product['id'],$category_array)){
						$category_array[] = "".$result_category_product['id'];
					}
					if(!in_array($result_category_product['tag'],$tags_arr)){
						$tags_arr[] = $result_category_product['tag'];
					}
				}
				$query_mongo_product = $this->mongo_bookchor_product->product->updateOne(['mysql_product_id'=>$result_product["id"]],['$addToSet'=>['category'=>['$each'=>$category_arr],'tags'=>['$each'=>$tags_arr]]]);
				if($query_mongo_product->getModifiedCount()){
					
				}else{
					echo $result_product["id"].'/n';
					/*Log Product ID*/
				}
				
			}
			//$this->main_db->close();
		}
		public function tagsCollection(){
			$query_tags_creation = $this->mongo_bookchor_product->product->aggregate([
				[
					'$unwind'=>'$tags'
				],
				[
					'$group'=>[
						'_id'=>'$tags',
						'count'=>[
							'$sum'=>1
						]
					]
				],
				[
					'$project'=>[
						'name'=>'$_id',
						'_id'=>0
					]
				]
			]);
			$tags_creation = iterator_to_array($query_tags_creation);
			$this->mongo_bookchor_product->tags->insertMany((array) $tags_creation,['ordered'=>false]);
		}
		public function languageCollection(){
			$query_language = $this->mongo_bookchor_product->product_languages->find([]);
			$language_data = iterator_to_array($query_language);
			foreach($language_data as $language){
				$codes = [];
				$final_codes = [];
				$codes = (array) $language['codes'];
				$final_codes = $codes;
				for($i=0;$i<sizeof($codes);$i++){
					$final_codes[] = strtolower($language['codes'][$i]);
					$final_codes[] = strtoupper($language['codes'][$i]);
					$final_codes[] = ucwords($language['codes'][$i]);
				}
				$final_codes[] = strtolower($language['name']);
				$final_codes[] = strtoupper($language['name']);
				$final_codes[] = ucwords($language['name']);
				array_unique($final_codes);
				$this->mongo_bookchor_product->product->updateMany(['details.language'=>['$in'=>$final_codes]],['$set'=>['details.language_id'=>$language['_id']]]);
			}
			$query_new_language = $this->mongo_bookchor_product->product->aggregate([
				[
					'$match'=>[
						'details.language'=>['$exists'=>true]
					]
				],
				[
					'$group'=>[
						'_id'=>'$details.language',
					]
				]
			]);
			$new_language_data = iterator_to_array($query_new_language);
			foreach($new_language_data as $language_data){  
				$query_lang_insert = $this->mongo_bookchor_product->product_languages->insertOne(['name'=>$language_data['_id']]);
				if($query_lang_insert->getInsertedCount()){
					$language_id = $query_lang_insert->getInsertedId();
					$this->mongo_bookchor_product->product->updateMany(['details.language'=>$language_data['_id']],['$set'=>['details.language_id'=>$language_id]]);
				}
			}
			$this->mongo_bookchor_product->product->updateMany([],['$unset'=>['details.language'=>true]]);
		}
		public function tagsAddMongo(){
			$query_tags = 'SELECT * FROM tags';
			$stmt_tags = $this->main_db->prepare($query_tags);
			$stmt_tags->execute();
			$row_tags = $stmt_tags->get_result();
			$stmt_tags->free_result();
			$stmt_tags->close();
			$tags = [];
			while($result_tags = $row_tags->fetch_array()){
				$tags[] = ['tag'=>$result_tags['tag']];
			}
			$this->mongo_bookchor_product->tags->insertMany($tags,['ordered'=>false]);
		}
		public function tagidCreation(){
			$query_tags = $this->mongo_bookchor_product->tags->find([]);
			$tags = iterator_to_array($query_tags);
			foreach($tags as $tag){
				$this->mongo_bookchor_product->product->updateMany(['tags'=>$tag['tag']],['$addToSet'=>['tag_id'=>$tag['_id']]]);
				echo $tag."<br>";
			}
		}
		public function bindingMapping(){
			//$this->mongo_bookchor_product->product->updateMany([],['$unset'=>['details.binding_id'=>true]]);
			
			$query_binding = $this->mongo_bookchor_product->product_binding->find(['id'=>['$lt'=>8]]);
			$binding_data = iterator_to_array($query_binding);
			foreach($binding_data as $binding){
				$binding_id = $binding['_id'];
				foreach($binding['codes'] as $codes){
					echo $codes;
					$search = new MongoDB\BSON\Regex($codes);
					$this->mongo_bookchor_product->product->updateMany(['details.binding'=>['$regex'=>$search,'$options'=>'i']],['$set'=>['details.binding_id'=>$binding_id]]);
				}
			} 
			
			$no_binding_id = new MongoDB\BSON\ObjectId('5c52a476c86b0c311488c701');
			$query_na_binding = $this->mongo_bookchor_product->product->updateMany(['details.binding_id'=>['$exists'=>false]],['$set'=>['details.binding_id'=>$no_binding_id]]);
			$this->mongo_bookchor_product->product->updateMany([],['$unset'=>['details.binding'=>true]]);
		}
		public function productCategory(){
			/*All Category*/
			$cat = array(
				0 => array("id" => "1","name"=>"literature"),
				1 => array("id" => "1","name"=>"fiction"),
				2 => array("id" => "2","name"=>"crime"),
				3 => array("id" => "2","name"=>"thriller"),
				4 => array("id" => "3","name"=>"romance"),
				5 => array("id" => "4","name"=>"classic"),
				6 => array("id" => "4","name"=>"classics"),
				7 => array("id" => "5","name"=>"science fiction"),
				8 => array("id" => "6","name"=>"fantasy"),
				9 => array("id" => "7","name"=>"young"),
				10 => array("id" => "7","name"=>"young adult"),
				11 => array("id" => "7","name"=>"adult"),
				12 => array("id" => "8","name"=>"child"),
				13 => array("id" => "8","name"=>"children"),
				14 => array("id" => "9","name"=>"nonfiction"),
				15 => array("id" => "10","name"=>"textbooks"),
				16 => array("id" => "8","name"=>"kid"),
				17 => array("id" => "5","name"=>"sci-fi"),
				18 => array("id" => "5","name"=>"scifi"),
				19 => array("id" => "2","name"=>"suspense"),
				20 => array("id" => "1","name"=>"lit"),
			);
			foreach($cat as $category){
				$search = new MongoDB\BSON\Regex($category['name']);
				$query_cat_mongo = $this->mongo_bookchor_product->category->find(['id'=>$category['id']]);
				$cat_mongo = iterator_to_array($query_cat_mongo);
				$category_id = $cat_mongo['_id'];
				print_r($cat_mongo);
				echo $category_id;
				die;
				/* $this->mongo_bookchor_product->product->updateMany(['tags'=>['$regex'=>$search,'$options'=>'ix']],['$addToSet'=>['category'=>$category_id]]);
				echo $category_id."<br>"; */
			}
		}
		public function addProductCategoryTags(){
			/*All Category*/
			$cat = array(
				0 => array("id" => "1","name"=>"literature"),
				1 => array("id" => "1","name"=>"fiction"),
				2 => array("id" => "2","name"=>"crime"),
				3 => array("id" => "2","name"=>"thriller"),
				4 => array("id" => "3","name"=>"romance"),
				5 => array("id" => "4","name"=>"classic"),
				6 => array("id" => "4","name"=>"classics"),
				7 => array("id" => "5","name"=>"science fiction"),
				8 => array("id" => "6","name"=>"fantasy"),
				9 => array("id" => "7","name"=>"young"),
				10 => array("id" => "7","name"=>"young adult"),
				11 => array("id" => "7","name"=>"adult"),
				12 => array("id" => "8","name"=>"child"),
				13 => array("id" => "8","name"=>"children"),
				14 => array("id" => "9","name"=>"nonfiction"),
				15 => array("id" => "10","name"=>"textbooks"),
				16 => array("id" => "8","name"=>"kid"),
				17 => array("id" => "5","name"=>"sci-fi"),
				18 => array("id" => "5","name"=>"scifi"),
				19 => array("id" => "2","name"=>"suspense"),
				20 => array("id" => "1","name"=>"lit"),
			);
			$query_isbn = "SELECT distinct(isbn) from category_xbyte";
			$stmt_isbn =  $this->main_db->prepare($query_isbn);
			$stmt_isbn->execute();
			$row_isbn = $stmt_isbn->get_result();
			$file = fopen("error_mongo_category_pid.txt","a");
			while($result = $row_isbn->fetch_array()){
				$isbn = $result['isbn'];
				$query_cat = "SELECT category from category_xbyte WHERE isbn='$isbn'";
				$stmt_cat =  $this->main_db->prepare($query_cat);
				$stmt_cat->execute();
				$row_cat = $stmt_cat->get_result();
				if($row_cat->num_rows()){
					$regex_tags = [];
					$size  = $row_cat->num_rows();
					while($result_cat = $row_cat->fetch_array()){
						$tag =  $result_cat['category'];
						//$tag_search = new MongoDB\BSON\Regex($tag);
						foreach($cat as $cat_arr){
							if(stripos($tag,$cat_arr['name'])!==FALSE){
								$cat_ids[] = $cat_arr['id'];
							}
						}
						$regex_tags[] = ['name'=>['$regex'=>$tag,'$options'=>'i']];
					}
					$tag_id = [];
					$query_tags = $this->mongo_bookchor_product->tags->find(['$or'=>$regex_tags]);
					$tags_info = iterator_to_array($query_tags);
					if(isset($tags_info[0])){
						foreach($tags_info as $tagid){
							$tag_id[] = $tagid['_id'];
						}
					}
					$category_id = [];
					$query_category = $this->mongo_bookchor_product->category->find(['id'=>['$in'=>$cat_ids]]);
					$category_info = iterator_to_array($query_category);
					if(isset($category_info[0])){
						foreach($category_info as $catid){
							$category_id[] = $catid['_id'];
						}
					}
					try{
						$query_update_product = $this->mongo_bookchor_product->product->updateOne(['ean'=>(string)$isbn],['$addToSet'=>['tag_id'=>['$each'=>$tag_id],'category_id'=>['$each'=>$category_id]]]);
						if($query_update_product->getModifiedCount()){
						}else{
							fwrite($file,$isbn."\n");
						}
					}catch(MongoDB\Driver\Exception\Exception $e){
						fwrite($file,$isbn."".$e."\n");
					}
				}
			}
			fclose($file);
		}
		public function conditionMigrate(){
			$query_migrate = 'SELECT * FROM product_condition';
			$stmt_migrate = $this->main_db->prepare($query_migrate);
			$stmt_migrate->execute();
			$row_migrate = $stmt_migrate->get_result();
			$condition_arr = [];
			while($result_migrate = $row_migrate->fetch_array()){
				$query_condition_detail = 'SELECT * FROM product_condition_detail WHERE condition_id='.$result_migrate['id'];
				$stmt_condition_detail = $this->main_db->prepare($query_condition_detail);
				$stmt_condition_detail->execute();
				$row_condition_detail = $stmt_condition_detail->get_result();
				$condition_detail = [];
				while($result_condition_detail = $row_condition_detail->fetch_array()){
					$condition_detail[] = [
						'image'=>$result_condition_detail['image'],
						'description' => $result_condition_detail['description']
					];
				}
				$condition_arr[] = ['id'=>$result_migrate['id'],'name'=>$result_migrate['type'],'details'=>$condition_detail];
			}
			$this->mongo_bookchor_product->product_condition->insertMany($condition_arr,['ordered'=>false]);
		}
	}
	
?>