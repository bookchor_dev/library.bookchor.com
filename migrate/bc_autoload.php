<?php
	require_once('classes/vendor/autoload.php');
	spl_autoload_register('bcAutoloader');
	
	function bcAutoloader($className){
		$path = 'classes';
		$dir = scandir($path);
		foreach($dir as $dir_name){
			$include_file = $path.'/'.$dir_name.'/'.$className.'.php';
			if(file_exists($include_file)){
				include $include_file;
			}
		}
	}
?>