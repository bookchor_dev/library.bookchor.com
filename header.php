<nav class="navbar navbar-expand-sm header_bar">
    <div class="container-fluid header-div" bc-search-open="close">
		<div class="col-2 col-md-5 col-lg-4 pl-0 pr-0 logo-div">
        <a href="/"><h6 id="brand-name" class="float-left font-weight-bold mb-0 school_title">
			<img src="<?php echo $user->user['logo'];?>" height="50px;">
			<span class="d-none d-md-inline-block"><?php echo $user->user['school'];?></span>
		</h6></a>
		</div>
		<div class="d-none d-sm-block col-8 col-md-6 pt-2 pl-0 pr-0">
			<form action="/search/">
            <div class="input-group">
				<div class="input-group-prepend">
					<input type="hidden" name="ebook">
					<button type="button" data-toggle="dropdown" class="btn btn-outline-secondary search-btn search-type dropdown-toggle">Books</button>
					<div class="dropdown-menu">
						<a class="dropdown-item bc-options" href="#0">Books</a>
						<a class="dropdown-item bc-options" href="#1">E-books</a>
					</div>
				</div>
				<input type="text" class="form-control" name="query" placeholder="Search Books">
				<span class="bc-bc-search-icon bc-input-addon"></span>
			</div> 
			</form>
		</div>
		<div class="d-block d-sm-none col-8 col-md-6 pt-2 search-mob-div">
			<form action="/search/">
            <div class="input-group">
				<div class="input-group-prepend d-none">
					<button type="button" data-toggle="dropdown" class="btn btn-outline-secondary search-btn dropdown-toggle">Books</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">Books</a>
						<a class="dropdown-item" href="#">E-books</a>
					</div>
				</div>
				<input type="text" name="query" placeholder="Search Books" class="form-control mob-search">
				<span class="bc-bc-search-icon bc-input-addon"></span>
			</div> 
			</form>
		</div>
		<div class="col-2 col-md-1 col-lg-2 account-div">
        <ul class="nav float-right">
            <li class="nav-item">
                <button class="profile float-right" onclick="logout();"><?php echo substr($user->user['name'],0,1); ?></button>
                <div class="log-out log-display" id="log-out">
                    <a class="dropdown-item" href="/profile">My Account</a>
					<a class="dropdown-item" href="/wishlist" >Wishlist</a>
					<a class="dropdown-item" href="/issued" >Issue History</a>
					<a class="dropdown-item" href="/e-books/1" >View E-books</a>
                    <a class="dropdown-item" href="/logout">Logout</a>
				</div>
			</li>
		</ul>
		</div>
	</div>
</nav>
<div class="container-fluid">
	<div class="row">
		<div class="col-6 col-sm-9 col-md-9 col-lg-8 px-0">
			<div class="d-none d-md-flex mt-2 mb-2">
				<div class="pl-1">
					<a class="btn btn-info rounded header-category" href="/category/5bb11c0670d9bd2d0883262e/All-Books">All Books</a>
					<a class="btn btn-info rounded header-category" href="/recommended-books">Recommended</a>
				</div>
				<?php 
					$category = $product->category;
					for($i=1;$i<5;$i++){?>
					<div class="pl-1">
						<a class="btn btn-info rounded  header-category" href="/category/<?php echo $category[$i]['_id'];?>/<?php echo str_replace(' ','-',$category[$i]['category']);?>" title="<?php echo $category[$i]['category'];?>"><?php echo $category[$i]['category'];?></a>
					</div>
				<?php } ?>
				<div class="dropdown pl-1">
					<button type="button" class="btn btn-info header-category" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
					<div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
						<?php for(;$i<count($category);$i++){?>
							<a class="dropdown-item py-2" href="/category/<?php echo $category[$i]['_id'];?>/<?php echo $category[$i]['category'];?>"><?php echo $category[$i]['category'];?></a>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="dropdown categories d-block d-md-none pt-2">
				<button class=" dropdown-toggle category-button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">
					Categories
				</button>
				<div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="/recommended-books">Recommended</a>
					<?php for($i=0;$i<count($category);$i++){?>
						<a class="dropdown-item" href="/category/<?php echo $category[$i]['_id'];?>/<?php echo str_replace(' ','-',$category[$i]['category']);?>"><?php echo $category[$i]['category'];?></a>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="col-6 col-sm-3 col-md-3 col-lg-4 mt-2 pr-3">
			<a type="button" class="btn btn-info header-category float-right" href="/e-books/1">E-books</a>
		</div>
	</div>
</div>