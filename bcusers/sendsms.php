<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require('../vendor/autoload.php');
	require('../library_classes/Connection/Connection.php');
	require('../library_classes/Utility/Utility.php');
	$cron = new Cron();
	$cron->cronEbooks();
?>