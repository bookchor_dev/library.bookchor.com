$(document).ready(function(){
    getTitle();
$('body').on('click', '.add_question', function(){
  var count = $(".quiz_block").children().length;
  var key=count+1;
  $('.quiz_block').append('<div class="form-group" id='+count+'> <div class="card shadow"><div class="card-header">Question '+key+'</div><div class="card-body question_block"><div class="input-group mb-3 col-md-12 question"> <input type="text" class="form-control" name="data['+count+'][question]" placeholder="Enter your question" required> <div class="input-group-append"> <button class="btn btn-success add_question" type="button">+</button> <button class="btn btn-danger remove_question" type="button">-</button></div> </div> <div class="input-group mb-3 col-md-10 answers"> <input type="text" class="form-control" name="data['+count+'][options][0]" placeholder="Answer" required> <div class="input-group-append"> <div class="input-group-append"> <span class="input-group-text"><input type="checkbox" class="check_'+count+'" name="data['+count+'][answer][0]" value="0"></span> </div> <button class="btn btn-success add_answer" type="button">+</button> <button class="btn btn-danger remove_answer" type="button">-</button> </div> </div></div></div> </div>');
  });
$('body').on('click', '.add_answer', function(){
  var quiz_block_count=$(this).parents('.quiz_block').children().length;
  quiz_block_count--;
  var answer_count=$(this).parents('.form-group').children().length;
  var answer=$(this).parents('.question_block').children('.answers').length;
 /* alert('answer_count'+answer_count);
  alert('answer'+answer);*/
  if($(this).parents('.question_block').children().length > 4){
    alert('cannot add more answers');
  }else{
  answer_count--;
  $(this).closest('.input-group-append').closest('.input-group').after(' <div class="input-group mb-3 col-md-10 answers"> <input type="text" class="form-control" name="data['+quiz_block_count+'][options]['+answer+']" placeholder="Answer" required> <div class="input-group-append"> <div class="input-group-append"> <span class="input-group-text"><input type="checkbox" class="check_'+quiz_block_count+'" name="data['+quiz_block_count+'][answer]['+answer+']" value='+answer+'></span> </div> <button class="btn btn-success add_answer" type="button">+</button> <button class="btn btn-danger remove_answer" type="button">-</button> </div> </div>');
  }
});

$('body').on('click', '.chip', function(){
    $(".isbn_checkbox").each(function(i) {
         $(this).removeAttr('checked');
    });
    var product_id=$(this).attr('p_id');
    $('#product_id').val(product_id);
    $(this).find('input:checkbox:first').attr('checked', 'checked');
    $(".chip").removeClass('selected');
    var isbn=$(this).text();
    $(this).addClass('selected');
    document.getElementsByClassName('isbn_checkbox').value = isbn.trim() ;
    getTitle();

});

$('body').on('click', '.remove_answer', function(){
  if($(this).parents('.question_block').children('.answers').length <= 1){
    alert('there should be atleast one answer');
  }
  else{
     $(this).parents('.answers').remove();
  }
});
$('body').on('click', '.remove_question', function(){
  $(this).parents('.form-group').remove();
});
$(".submit_quiz_form").click(function(e){
var count = $(".quiz_block").children().length;
var flag=true;
  for(var k=0;k<count;k++){
      var key=k+1;
      var checkboxs=document.getElementsByClassName("check_"+k);
      var okay=false;
      for(var i=0,l=checkboxs.length;i<l;i++)
      {
          if(checkboxs[i].checked)
          {
              okay=true;
              break;
          }
      }
      if(!okay){
        alert('Please select an answer for Question :'+key);
        flag=false;
        e.preventDefault();
      }
    }

    if(flag){
      $(document).on('submit','form',function(){
    });
    }
//e.preventDefault();
});
});
function getTitle(){
  var isbn=($('input[class="isbn_checkbox"]:checked').val());
  var title=$('input[class="isbn_checkbox"]:checked').attr('title');
  $("#add_question_message").html("ISBN : "+isbn + "<br>Title :"+title);
} 
