var next_counter = 0;
getData(0);
$(".next").click(function (e) {
  next_counter = +next_counter + +20;
  getData(next_counter, 1);

});
$(".previous").click(function (e) {
  next_counter = +next_counter - +20;
  getData(next_counter, 0);
});

function getData(skip, direction) {
  if (skip == 0) {
    $('.previous').addClass('disabled');
  } else {
    $('.previous').removeClass('disabled');
  }
  $('#isbn_data').empty();
  $.ajax({
    type: "GET",
    url: "https://library.bookchor.com/webservices/library.bookchor.com/Product/quiz.php?type=viewProducts&skip=" + skip + "",
    dataType: "jsonp",
    success: function (response) {
      if (response.length == 0) {
        $('#isbn_data').append('<tr><td>No data</td></tr>');
        if (direction == 1) {
          $('.next').addClass('disabled');
        }
      } else {
        $('.next').removeClass('disabled');
        appendData(response);
      }
    }
  });
}
$(".search_bar").click(function (e) {
  var query=$('#search_input').val();
  if(!query){
    getData(0,1);  
  }
  else{
  $.ajax({
    type: "GET",
    url: "http://library.bookchor.com/webservices/library.bookchor.com/Product/quiz.php?type=viewQuiz&query="+query,
    dataType: "json",
    success: function (response) {
      if(response.length==0){
         $('#isbn_data').append('<tr><td>No result</td></tr>');
      }
      else{
      console.log(response);
        appendData(response);
      }
    },
  });
  }
}); 


function appendData(response){
  $('#isbn_data').empty();
  for (var i = 0; i < response.length; i++) {
          $('#isbn_data').append('<tr><td>' + response[i].ean + '</td> <td>' + response[i].title + '</td> <td><a href="edit_quiz.php?query=' + response[i].product_id + '" class="btn btn-primary">View</a></td> </tr>');
        } 
}
