<?php
/*$skip=1;
if(!isset($_REQUEST['page'])){
  $skip=0;
}
else{
  $skip=$_REQUEST['page'];
  $skip--;
}*/
/*$skip=0;
$url=file_get_contents('http://library.bookchor.com/webservices/library.bookchor.com/Product/quiz.php?type=viewProducts&skip='.$skip);
$isbn_data=json_decode($url,TRUE);*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Search Questions</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/custom.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style type="text/css">
    .disabled {
    pointer-events: none;
    cursor: default;
  }
  </style>
</head>
<body>
<?php include 'navbar.php';?>
<div class="container mt-3">
  <div class="col-md-4">
   <div class="input-group mb-3">
    <input type="text" class="form-control" id="search_input" placeholder="Search Query">
    <div class="input-group-append">
      <button type="button" class="input-group-text search_bar">Search</button>
    </div>
  </div>
  </div>
  <table class="table" id="isbn_table">
    <thead>
      <tr>
        <th>ISBN</th>
        <th>Title</th>
        <th>View</th>
      </tr>
    </thead>
    <tbody id="isbn_data">
    </tbody>
  </table>
</div>
<div class="container">
  <ul class="pagination">
     <li class="page-item"><button  class="page-link previous <?php //echo $disabled;?>"><<</button></li>
     <li class="page-item"><button  class="page-link next <?php //echo $disabled;?>">>></button></li>
  </ul>
</div>
<script src="js/search.js"></script>
</body>
</html>
