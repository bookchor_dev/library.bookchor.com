<?php
if(isset($_POST['submit'])){
 foreach ($_POST['data'] as $key => $value) {
    $_POST['data'][$key]['answer']=array_values($_POST['data'][$key]['answer']);
  }
  foreach ($_POST['data'] as $key => $value) {
    foreach ($value['answer'] as $k => $v) {
      $_POST['data'][$key]['answer'][$k]=(int)$v;
    }
  }
  $url='http://library.bookchor.com/webservices/library.bookchor.com/Product/quiz.php?type=addQuiz';
  $data=array('product_id'=>$_POST['product_id'],'json'=>json_encode($_POST['data']));
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL,$url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS,urldecode(http_build_query($data)));
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $json_response = curl_exec($ch);
}
$url=file_get_contents('http://library.bookchor.com/webservices/library.bookchor.com/Product/quiz.php?type=booksToAdd');
$isbn_data=json_decode($url,TRUE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Add Questions</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/custom.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php include 'navbar.php';?>
<div class="container mt-3">
  <h1>Quiz</h1>
  <form id="quiz_data" method="POST" action="">
    <div class="row mb-3">
  <?php
  $selected="";
   for ($i=0; $i<18;$i++) {
    if($i==0){$selected="selected";$checked="checked";}else{$selected="";$checked="";}
    ?>
  <div p_id="<?php echo $isbn_data[$i]['product_id'];?>" class="chip <?php echo $selected;?>">
      <p><b><?php echo $isbn_data[$i]['ean'];?></b></p>
      <input type="checkbox" class="isbn_checkbox" title="<?php echo $isbn_data[$i]['title'];?>"  name="isbn" value="<?php echo $isbn_data[$i]['ean'];?>"<?php echo $checked;?>>
   </div>
  <?php } ?>
  <input type="hidden" id="product_id" name="product_id" value="<?php echo $isbn_data[0]['product_id'];?>">
  </div>
  <div class="alert alert-success">
    <p id="add_question_message"></p>
  </div>
    <div class="quiz_block">
      <div class="form-group" id="1">
        <div class="card shadow">
          <div class="card-header">Question 1</div>
          <div class="card-body question_block">
        <div class="input-group mb-3 col-md-12 question">
          <input type="text" class="form-control" name="data[0][question]" placeholder="Enter your question" required>
      	  <div class="input-group-append">
            <button class="btn btn-success add_question" type="button">+</button>  
           </div>
        </div>
        <div class="input-group mb-3 col-md-10 answers">
          <input type="text" class="form-control" name="data[0][options][0]" placeholder="Answer" required>
          <div class="input-group-append">
            <div class="input-group-append">
              <span class="input-group-text"><input type="checkbox" class="check_0" name="data[0][answer][0]" value="0"></span>
            </div>
            <button class="btn btn-success add_answer" type="button">+</button>  
            <button class="btn btn-danger remove_answer" type="button">-</button>  
           </div>
        </div>
      </div>
    </div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary submit_quiz_form" name="submit">Submit</button>
    </form>
</div>
</body>
<script src="js/custom.js" type="text/javascript"></script>
</html>
