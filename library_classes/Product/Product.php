<?php 
	class Product{
		public $category;
		private $pass;
		public function __construct(){
			$con = new Connections();
			$this->mongo_bookchor_library = $con->mongo_bookchor_library();
			$this->mysql_library = $con->main_db();
			$this->timestamp = (new DateTime())->getTimestamp();
			$this->category = $this->get_category_info([]);
			$this->pass = urlencode('Success^1218#');
		}
		public function booklist($user_info,$doc_info,$bc_db_access=1){
			$user_id = $user_info['user_id'];
			$library_id = $user_info['library_id'];
			$start_id = isset($doc_info['doc_id'])?$doc_info['doc_id']:0;
			$limit = isset($doc_info['limit'])?$doc_info['limit']:10;
			if(isset($doc_info['type'])){
				$fq1 = '';
				$fq = '';
				$count = 0;
				$mm = urlencode('50%');
				if(isset($doc_info['ebook'])&&$doc_info['ebook']==1){
					$fq = urlencode('ebook:[* TO *]');
					$stock = 0;
				}else{
					$fq = '{!join%20fromIndex=bookchor_library%20from=product_id%20to=id}stock:1';
					$fq1 = '{!join%20fromIndex=bookchor_library%20from=product_id%20to=id}library_id:'.$user_info['library_id'];
					$stock = 1;
				}
				$bc = 0;
				while($count<$limit){
					if(($doc_info['type']=='search')&&(isset($doc_info['query']))){
						/*Search Product*/
						$search = urlencode($doc_info['query']);
						$page_title = $doc_info['query'];
						/* if(is_numeric($doc_info['query'])&& strlen($doc_info['query'])>10){
							$bc=1;
						} */
						if($bc==0){
							$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?defType=dismax&fq=".$fq."&fq=".$fq1."&indent=on&mm=".$mm."&q=".$search."&start=".$start_id."&rows=".$limit."&wt=json";
							$stock = 1;
						}else if($bc==1){
							$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?indent=on&q=".$page_title."&start=".$start_id."&rows=".$limit."&wt=json";	
						}else if($bc==2){
							$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?&defType=dismax&fq=-".$fq."&indent=on&mm=".$mm."&q=".$search."&start=".$start_id."&rows=".$limit."&wt=json";
							$stock = 0;
						}
					}else if(($doc_info['type']=='category')&&(isset($doc_info['id']))){
						//$cat_id = explode('/',$doc_info['id'])[0];
						$cat_arr = ['_id'=> new MongoDB\BSON\ObjectId($doc_info['id'])];
						$page_data_raw = $this->get_category_info($cat_arr);
						if(isset($page_data_raw[0])&&$page_data_raw[0]){
							$page_data = $page_data_raw[0];
							if(isset($doc_info['ebook'])&&$doc_info['ebook']==1){
								$page_title = 'E-Books';
								$search = '*:*';
							}else{
								$page_title = $page_data['category'];
								$category_id = $page_data['solr_category_id'];
								$search = 'category:'.$category_id;
							}
							
							/*Category*/	
							
							
							$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?fq=".$fq."&fq=".$fq1."&indent=on&q=".$search."&start=".$start_id."&rows=".$limit."&wt=json";
							//$stock = 1;
						}
					}
					//echo $url;die;
					$productlist = [];
					$count = 0;
					$json = file_get_contents($url);
					$data = json_decode($json);
					$responses = $data->response->docs;
					if($responses){
						foreach($responses as $response){
							$start_id++;
							$product_id = $response->id;
							$title = $response->title[0];
							$author = $response->author[0];
							$image = $response->image[0];
							$ebook = isset($response->ebook[0])?(string)$response->ebook[0]:0;
							if(strpos($image,'https') !== false){
								
								}else{
								$image = str_replace("http","https",$image);
							}
							$isbn = $response->isbn[0];
							$mrp = $response->mrp[0];
							$goodreads_total_reviews = $response->goodread_reviews[0];
							$goodreads_avg_rating = $response->goodreads_avg_rating[0];
							$productlist[] = array(
							"product_id" => $product_id,
							"title" => utf8_encode($title),
							"author" => utf8_encode($author),
							"isbn" => $isbn,
							"image" => $image,
							"description" => isset($result_description['description'])?$result_description['description']:'',
							"rating" => $goodreads_avg_rating,
							"reviews" => $goodreads_total_reviews,
							"wishlist" => isset($wishlist)?$wishlist:0,
							"read" => isset($read)?$read:0,
							"social" => isset($social)?$social:'',
							"link"=> '/book/'.$isbn.'/'.str_replace(" ","-",$title),
							"ebook"=>$ebook,
							"stock"=>$stock
							);
							$count++;
						}
					}
					if($count<$limit){
						$start_id = 0;
						if($bc_db_access==1){
							if($bc==0){
								$bc = 2;
							}else{
								break;
							}
						}else{
							break;
						}
					}
				}
			}
			$result['result'] = $productlist;
			//$result['docid'] = $start_id;
			$result['title'] = $page_title;
			$result['issue'] = 1;
			$result['pdoc'] = isset($doc_info['doc_id'])?$doc_info['doc_id']:0;
			$result['ndoc'] = $start_id;
			return $result;
		}
		public function recommendedBooks($user_info,$doc_info){
			$user_id = $user_info['user_id'];
			$library_id = $user_info['library_id'];
			$start_id = isset($doc_info['doc_id'])?$doc_info['doc_id']:0;
			$limit = isset($doc_info['limit'])?$doc_info['limit']:10;
			$mm = urlencode('50%');
			$fq = '{!join%20fromIndex=bookchor_library%20from=product_id%20to=id}stock:1';
			$fq1 = '{!join%20fromIndex=bookchor_library%20from=product_id%20to=id}library_id:'.$user_info['library_id'];
			$stock = 1;
			$bc = 0;
			$count = 0;
			while($count<$limit){
				$search_arr = [];
				if($user_info['category']){
					$cat_data = [];
					foreach($user_info['category'] as $cat_id){
						$cat_data[] = new MongoDB\BSON\ObjectId($cat_id);
					}
					$cat_arr = ['_id'=>['$in'=>$cat_data]];
					$page_data_raw = $this->get_category_info($cat_arr);
				}
				if(isset($page_data_raw[0])&&$page_data_raw[0]){
					foreach($page_data_raw as $page_data){
						/*Category*/
						$category_id = $page_data['solr_category_id'];
						$search_arr[] = 'category:'.$category_id;
					}
					if($bc==0){
						$search = urlencode(implode('AND ',$search_arr));
					}else{
						$search = urlencode(implode('OR ',$search_arr));
					}
				}else{
					$bc = 2;
					$search = 'category:*';
				}
				$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?fq=".$fq."&fq=".$fq1."&indent=on&q=".$search."&start=".$start_id."&rows=".$limit."&wt=json";
				
				$productlist = [];
				$json = file_get_contents($url);
				$data = json_decode($json);
				$responses = $data->response->docs;
				if($responses){
					foreach($responses as $response){
						$start_id++;
						$product_id = $response->id;
						$title = $response->title[0];
						$author = $response->author[0];
						$image = $response->image[0];
						$ebook = isset($response->ebook[0])?$response->ebook[0]:0;
						if(strpos($image,'https') !== false){
						}else{
							$image = str_replace("http","https",$image);
						}
						$isbn = $response->isbn[0];
						$mrp = $response->mrp[0];
						$goodreads_total_reviews = $response->goodread_reviews[0];
						$goodreads_avg_rating = $response->goodreads_avg_rating[0];
						$productlist[] = array(
							"product_id" => $product_id,
							"title" => utf8_encode($title),
							"author" => utf8_encode($author),
							"isbn" => $isbn,
							"image" => $image,
							"description" => isset($result_description['description'])?$result_description['description']:'',
							"rating" => $goodreads_avg_rating,
							"reviews" => $goodreads_total_reviews,
							"wishlist" => isset($wishlist)?$wishlist:0,
							"read" => isset($read)?$read:0,
							"social" => isset($social)?$social:'',
							"link"=> '/book/'.$isbn.'/'.str_replace(" ","-",$title),
							"ebook"=>$ebook,
							"stock"=>$stock
						);
						$count++;
					}
				}
				if($count<$limit){
					$start_id = 0;
					if($bc==0){
						$bc = 1;
					}else{
						break;
					}
				}
			}
			$result['result'] = $productlist;
			$result['title'] = 'Recommended Based on your choice';
			$result['issue'] = 1;
			$result['pdoc'] = isset($doc_info['doc_id'])?$doc_info['doc_id']:0;
			$result['ndoc'] = $start_id;
			return $result;
		}
		
		public function user_booklist($user_info,$doc_info){
			$user_id = $user_info['user_id'];
			$pid_arr = [];
			$userbooklist = [];
			$productlist_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['_id'=>$user_id]],['$unwind'=>$doc_info['userlist']],['$replaceRoot'=>['newRoot'=>$doc_info['userlist']]],['$match'=>['status'=>['$gt'=>0]]],['$skip'=>$doc_info['doc_id']],['$limit'=>$doc_info['limit']],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>['path'=>'$details','preserveNullAndEmptyArrays'=>true]]]);
			$productlist = iterator_to_array($productlist_query);
			foreach($productlist as $plist){
				if(!isset($plist['details'])){
					$pid_arr[] = $plist['product_id'];	
				}
			}
			if($pid_arr){
				$pquery = urlencode(implode(' OR ',$pid_arr));
				$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?indent=on&q=".$pquery."&wt=json";	
				$json = file_get_contents($url);
				$data = json_decode($json,true);
				$responses = $data['response']['docs'];
				//print_r($responses);die;
				if($responses){
					foreach($productlist as $plist){
						foreach($responses as $response){
							if($plist['product_id']==$response['product_id']){
								$details = [];
								$details = [
									'title'=>$response['title'][0],
									'author'=>$response['author'][0],
									'goodreads_total_reviews'=>$response['goodread_reviews'][0],
									'goodreads_avg_rating'=>$response['goodreads_avg_rating'][0],
									'image'=>$response['image'][0],
									'isbn'=>$response['isbn'][0],
									'ebook'=>isset($response['ebook'][0])?$response['ebook'][0]:0,
								];
								$plist['details'] = $details;
							}
						}
						$userbooklist[] = $plist;
					}
				}
			}else{
				$userbooklist = $productlist;
			}
			$count = count($userbooklist);
			$productlists = [
				'result'=>$userbooklist,
				'ndoc'=>$doc_info['doc_id']+$count,
				'pdoc'=>$doc_info['doc_id'],
			];
			return (array) $productlists;
		}
		public function product_description($pid){
			//$result_description['description'] = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa quos eos quis est nobis reiciendis officia voluptatum, et eligendi ipsa....';
			$query_description = 'SELECT p.*,pub.name as publisher FROM product as p,publisher as pub WHERE p.publisher_id=pub.id AND p.id=?';
			$stmt_description = $this->mysql_library->prepare($query_description);
			$stmt_description->bind_param('s',$pid);
			$stmt_description->execute();
			$row_description = $stmt_description->get_result();
			$result_description = $row_description->fetch_array();
			return $result_description['description'];
		}
		public function get_product_category($pid){
			$category = [];
			$category_arr = [];
			$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?q=id:".$pid."&wt=json";
			$json = file_get_contents($url);
			$data = json_decode($json);
			$responses = $data->response->docs;
			if($responses){
				foreach($responses as $response){
					$product_id = $response->id;
					$category = isset($response->category)?$response->category:[];
				}
			}
			foreach($category as $cat){
				foreach($this->category as $cat_info){
					if($cat_info['solr_category_id']==$cat){
						$cat_info['tags'] = $response->{'category'.$cat};
						$category_arr[] = $cat_info;
					}
				}
			}
			return $category_arr;
		}
		public function get_category_info($cat_arr){
			$category_query = $this->mongo_bookchor_library->category->find($cat_arr);
			$page_data_raw  = iterator_to_array($category_query);
			return $page_data_raw;
		}
		public function add_wishlist($pid,$user_id,$status){
			$user_product = $this->library_product_info($user_id,$pid);
			if($status=='-1'){
				if($user_product['wishlist']==1){
					$wishlist_query = $this->mongo_bookchor_library->users->updateOne(['_id'=>new MongoDB\BSON\ObjectId($user_id),'wishlist'=>['$elemMatch'=>['product_id'=>$pid,'status'=>1]]],['$set' =>['wishlist.$.status'=>0]]);
				}
				
			}else{
				$preference_val = 1/10;
				$user_category = $this->userCategoryPreference($user_id,$pid,$preference_val);
				if($user_product['wishlist']==0){
					$wishlist_query = $this->mongo_bookchor_library->users->updateOne(['_id'=>new MongoDB\BSON\ObjectId($user_id)],['$push' =>['wishlist'=>['_id'=> new MongoDB\BSON\ObjectId(),'product_id'=>$pid,'read'=>(int) $status,'date_added'=>$this->timestamp,'status'=>1]],'$set'=>['user_preference'=>$user_category]]);
				}else{
					if($user_product['read']==0&&$status==1){
						$wishlist_query = $this->mongo_bookchor_library->users->updateOne(['_id'=>new MongoDB\BSON\ObjectId($user_id),'wishlist'=>['$elemMatch'=>['product_id'=>$pid,'read'=>0,'status'=>1]]],['$set' =>['wishlist.$.read'=>(int) $status,'wishlist.$.date_read'=>$this->timestamp]]);
					}
				}
			}
			if($wishlist_query->getModifiedCount()){
				return 1;
			}else{
				return 0;
			}
		}
		public function library_product_info($user_id,$product_id){
			$info = [
			'wishlist'=>0,
			'read'=>0,
			'social'=>'Be the First one to Like this Book',
			];
			$social = 'Be the First one to Like this Book';
			$social_query = $this->mongo_bookchor_library->users->count(['wishlist.product_id'=>(string) $product_id,'wishlist.status'=>1]);
			if($social_query){
				$social = $social_query.' of your Schoolmates liked this book';
			}
			$info['social'] = $social;
			$wishlist_query =  $this->mongo_bookchor_library->users->aggregate([['$match'=>['_id'=>$user_id,'wishlist.status'=>1]],['$project'=>['wishlist'=>1]],['$unwind'=>'$wishlist'],['$replaceRoot'=>['newRoot'=>'$wishlist']],['$match'=>['product_id'=>(string) $product_id,'status'=>1]]]);
			$wishlist_raw_data  = iterator_to_array($wishlist_query);
			if(isset($wishlist_raw_data)){
				if(isset($wishlist_raw_data[0])){
					$wishlist_data = $wishlist_raw_data[0];
					$info['wishlist'] = 1;
					$info['read'] = $wishlist_data['read'];
				}
			}
			return $info;
		}
		public function book_div($user_info,$doc_info){
			$column_id = $doc_info['column_id'];
			$library_id = $user_info['library_id'];
			$limit = $doc_info['limit'];
			$user_id = $user_info['user_id'];
			if($column_id==1){
				//most_liked
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['wishlist'=>1]],['$unwind'=>'$wishlist'],['$group'=>['_id'=>'$wishlist.product_id','likes'=>['$sum'=>1],'product_id'=>['$first'=>'$wishlist.product_id']]],['$sort'=>['likes'=>-1]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==2){
				//recently liked
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['wishlist'=>1]],['$unwind'=>'$wishlist'],['$replaceRoot'=>['newRoot'=>'$wishlist']],['$sort'=>['date_added'=>-1]],['$group'=>['_id'=>'$product_id','product_id'=>['$first'=>'$product_id']]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==3){
				//newly added books
				$column_query = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=>$library_id]],['$unwind'=>'$stock'],['$project'=>['product_id'=>1]],['$sort'=>['stock.date_added'=>-1]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==4){
				//recently_issued
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['issue'=>1]],['$unwind'=>'$issue'],['$replaceRoot'=>['newRoot'=>'$issue']],['$sort'=>['date_issued'=>-1]],['$group'=>['_id'=>'$product_id','product_id'=>['$first'=>'$product_id']]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==5){
				//most_viewed
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['view'=>1]],['$unwind'=>'$view'],['$group'=>['_id'=>'$view.product_id','likes'=>['$sum'=>1],'product_id'=>['$first'=>'$view.product_id']]],['$sort'=>['likes'=>-1]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==6){
				//recently_viewed
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['view'=>1]],['$unwind'=>'$view'],['$replaceRoot'=>['newRoot'=>'$view']],['$sort'=>['date_added'=>-1]],['$group'=>['_id'=>'$product_id','product_id'=>['$first'=>'$product_id']]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==7){
				//most_issued
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['issue'=>1]],['$unwind'=>'$issue'],['$group'=>['_id'=>'$issue.product_id','issued'=>['$sum'=>1],'product_id'=>['$first'=>'$issue.product_id']]],['$sort'=>['issued'=>-1]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==8){
				//your wishlist
				$column_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'_id'=>$user_id]],['$project'=>['wishlist'=>1]],['$unwind'=>'$wishlist'],['$match'=>['wishlist.status'=>1]],['$sort'=>['wishlist.date_added'=>-1]],['$project'=>['product_id'=>'$wishlist.product_id']],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}else if($column_id==9){
				//top books
				$column_query = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=>$library_id]],['$unwind'=>'$stock'],['$group'=>['_id'=>'$product_id','book_count'=>['$sum'=>1],'product_id'=>['$first'=>'$product_id']]],['$sort'=>['book_count'=>-1]],['$limit'=>$limit],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'details']],['$unwind'=>'$details']]);
			}
			$div_data = iterator_to_array($column_query);
			return (array) $div_data;
		}
		public function add_review($doc_info,$user_info){
			$review_query = $this->mongo_bookchor_library->review->updateOne(['user_id'=>$user_info['user_id'],'library_id'=>$user_info['library_id'],'product_id'=>$doc_info['pid']],['$set'=>['rating'=>$doc_info['rating'],'review'=>$doc_info['review']]],['upsert'=>true]);
			$review = iterator_to_array($review_query);
			if(($review->getModifiedCount())||($review->getUpsertedCount())){
				return 1;
			}else{
				return 0;
			}
		}
		public function delete_review($doc_info,$user_info){
			$delete_review_query = $this->mongo_bookchor_library->review->deleteOne(['user_id'=>$user['user_id'],'product_id'=>$doc_info['pid']]);
			if($delete_review_query->getDeletedCount()){
				return 1;
			}
		}
		public function view_reviews($query){
			$review_query = $this->mongo_bookchor_library->review->aggregate([['$match'=>$query],['$lookup'=>['from'=>'users','localField'=>'user_id','foreignField'=>'_id','as'=>'user']],['$unwind'=>'$user'],['$project'=>['rating'=>1,'review'=>1,'name'=>'$user.name']]]);
			$review = iterator_to_array($review_query);
			return $review;
		}
		
		public function list_sku($pid,$library_id){
			$sku = [];
			$out_flag = 1;
			$list_sku_query = $this->mongo_bookchor_library->books->find(['library_id'=>$library_id,'product_id'=>(string) $pid],['projection'=>['stock.sku'=>1,'stock.status'=>1,'_id'=>0],'limit'=>5]);
			$list_sku = iterator_to_array($list_sku_query);
			if(isset($list_sku[0])){
				foreach($list_sku[0]['stock'] as $stock){
					if($stock['status']==1){
						if($stock['out']==0){
							$out_flag = 0;
							$sku[] = $stock['sku'];
						}
					}
				}
			}
			return $sku;
		}
		public function product_view($user_id,$product_id){
			$preference_val = 1/100;
			$user_category = $this->userCategoryPreference($user_id,$product_id,$preference_val);
			$product_view_query = $this->mongo_bookchor_library->users->updateOne(['_id'=>$user_id],['$push'=>['view'=>['product_id'=>$product_id,'date_added'=>$this->timestamp]],'$set'=>['user_preference'=>$user_category]]);
		}
		public function userCategoryPreference($user_id,$product_id,$preference_val){
			$query_user_category = $this->mongo_bookchor_library->users->aggregate([
				[
					'$match'=>['_id'=>$user_id],
				],
				[
					'$project'=>['user_preference'=>1]
				],
				[
					'$unwind'=>'$user_preference'
				],
				[
					'$replaceRoot'=>['newRoot'=>'$user_preference']
				]
			]);
			$user_category = iterator_to_array($query_user_category);
			$cat_arr = $this->get_product_category($product_id);
			foreach($cat_arr as $cat){
				$cat_exists = 0;
				$user_tags = [];
				foreach($user_category as $key=>$category){
					if($cat['solr_category_id']==$category['category_id']){
						$cat_exists = 1;
						$user_category[$key]['score'] = $user_category[$key]['score'] + $preference_val;
						foreach($cat['tags'] as  $tag_id){
							$tag_exists = 0;
							foreach($category['tags'] as $keys=>$tag){
								if($tag['tag_id']==$tag_id){
									$tag_exists = 1;
									$user_category[$key]['tags'][$keys]['score'] = $tag['score'] + $preference_val/4;
								}
							}
							if($tag_exists==0){
								$user_category[$key]['tags'][] = ['tag_id'=>(int)$tag_id,'score'=>$preference_val/4];
							}
						}
					}
				}
				if($cat_exists==0){
					foreach($cat['tags'] as  $tag_id){
						$user_tags[] = ['tag_id'=>(int)$tag_id,'score'=>$preference_val/4];
					}
					$user_category[] = ['category_id'=>(int)$cat['solr_category_id'],'score'=>$preference_val,'tags'=>$user_tags];
				}
			}
			return $user_category;
		}
		public function recommendedBooks1($user_info,$doc_info){
			$user_id = $user_info['user_id'];
			$library_id = $user_info['library_id'];
			$start_id = isset($doc_info['doc_id'])?$doc_info['doc_id']:0;
			$limit = isset($doc_info['limit'])?$doc_info['limit']:10;
			$query_user_category = $this->mongo_bookchor_library->users->aggregate([
				[
					'$match'=>['_id'=>$user_id],
				],
				[
					'$project'=>['user_preference'=>1,'user_category'=>1]
				],
			]);
			$user_category_raw = iterator_to_array($query_user_category);
			if(!(isset($user_category_raw[0]['user_preference'])&&isset($user_category_raw[0]['user_category']))){
					/**Will be Coded in Next Release**/
			}else{
				$user_category  = isset($user_category_raw[0]['user_preference'])?$user_category_raw[0]['user_preference']:[];
				$category_pref = $user_category_raw[0]['user_category'];
				$tag_arr = [];
				$tag_array = [];
				$all_tag_count = 0;
				$cumulative_category = 0;
				$cumulative_tag = 0;
				$cumulative_all_tags_score = 0;
				$unsorted_cat_arr = [];
				$sorted_cat_arr = [];
				$bc=0;
				$cat_arr = ['_id'=>['$in'=>$category_pref]];
				$category_pref = $this->get_category_info($cat_arr);
				foreach($category_pref as $pref_category_id){
					$pref_exists = 0;
					foreach($user_category as $key=>$category){
						if($category['category_id']==$pref_category_id['solr_category_id']){
							$pref_exists = 1;
							$user_category[$key]['score'] = $user_category[$key]['score'] + 1;
						}
					}
					if($pref_exists==0){
						$user_category[] = ['category_id'=>(int)$pref_category_id['solr_category_id'],'score'=>1,'tags'=>[]];
					}
				}
				foreach($user_category as $key=>$category){
					$cumulative_category = $cumulative_category + $category['score'];
					if(isset($category['tags'])&&$category['tags']){
						foreach($category['tags'] as $key_tag=>$tag){
							$skey = array_search($tag['tag_id'],$tag_arr);
							if($skey!==FALSE){
								$cumulative_all_tags_score = $cumulative_all_tags_score + $tag['score'];
								$tag_array[$skey]['score'] = $tag_array[$skey]['score'] + $tag['score'];
							}else{
								$tag_array[] = ['tag_id'=>$tag['tag_id'],'score'=>$tag['score']];
								$tag_arr[] = $tag['tag_id'];
								$all_tag_count++;
							}
							$cumulative_tag = $cumulative_tag + $tag['score'];
						}
						$user_category[$key]['tag_avg'] = $cumulative_tag/($key_tag+1);
					}
				}
				if($all_tag_count!=0){
					$all_tag_avg = ($cumulative_all_tags_score/$all_tag_count);
				}
				$category_avg = $cumulative_category/($key+1);
				foreach($user_category as $key=>$category){
					if($category['score']<$category_avg){
						unset($user_category[$key]);
					}
				}
				$user_category = array_values((array)$user_category);
				/*Sorting User Category Array*/	
				function cmp($a, $b){
					if ($a == $b) {
						return 0;
					}
					return ($a['score'] < $b['score']) ? 1 : -1;
				}
				usort($user_category, "cmp");

				
				$mm = urlencode('50%');
				$fq = '{!join%20fromIndex=bookchor_library%20from=product_id%20to=id}stock:1';
				$fq1 = '{!join%20fromIndex=bookchor_library%20from=product_id%20to=id}library_id:'.$user_info['library_id'];
				$stock = 1;
				$bc = 0;
				$count = 0;
				foreach($user_category as $key=>$category){
					while($count<$limit){
						$tag_arr = [];
						$separater = ' OR ';
						foreach($category['tags'] as $tag){
							if($bc==0||$bc==1){
								foreach($tag_array as $tkey=>$t_arr){
									if($t_arr['score']>$all_tag_avg){
										if($tag['tag_id']==$t_arr['tag_id']){
											$tag_arr[] = $tag['tag_id'];
										}
									}
								}
							}else{
								if($tag['score']>=$category['tag_avg']){
									$tag_arr[] = $tag['tag_id'];
								}
							}
						}
						if(($category['score']>2*$category_avg)&&($bc==0||$bc==1)){
							if($bc==0){
								$separater = ' AND ';
							}	
						}
						$tag_query = implode($separater,$tag_arr);
						if($tag_query){
							$raw_solr_query = 'category:'.$category['category_id'].' AND category'.$category['category_id'].':('.$tag_query.')';
						}else{
							$raw_solr_query = 'category:'.$category['category_id'];
						}
						$solr_query = urlencode($raw_solr_query);
						$url = "http://bcsolruser:".$this->pass."@school.bookchor.com:8983/solr/bookchor/select?fq=".$fq."&fq=".$fq1."&indent=on&q=".$solr_query."&start=".$start_id."&rows=".$limit."&wt=json";
						$productlist = [];
						$json = file_get_contents($url);
						$data = json_decode($json);
						$responses = $data->response->docs;
						if($responses){
							foreach($responses as $response){
								$start_id++;
								$product_id = $response->id;
								$title = $response->title[0];
								$author = $response->author[0];
								$image = $response->image[0];
								$ebook = isset($response->ebook[0])?$response->ebook[0]:0;
								if(strpos($image,'https') !== false){
								}else{
									$image = str_replace("http","https",$image);
								}
								$isbn = $response->isbn[0];
								$mrp = $response->mrp[0];
								$goodreads_total_reviews = $response->goodread_reviews[0];
								$goodreads_avg_rating = $response->goodreads_avg_rating[0];
								$productlist[] = array(
									"product_id" => $product_id,
									"title" => utf8_encode($title),
									"author" => utf8_encode($author),
									"isbn" => $isbn,
									"image" => $image,
									"description" => '',
									"rating" => $goodreads_avg_rating,
									"reviews" => $goodreads_total_reviews,
									"wishlist" => isset($wishlist)?$wishlist:0,
									"read" => isset($read)?$read:0,
									"social" => isset($social)?$social:'',
									"link"=> '/book/'.$isbn.'/'.str_replace(" ","-",$title),
									"ebook"=>$ebook,
									"stock"=>1
								);
								$count++;
							}
						}
						if($count<$limit){
							$start_id = 0;
							if($bc==0){
								$bc = 1;
							}else if($bc==1){
								$bc = 2;
							}else{
								break;
							}
						}
					}
				}
			}
			$result['result'] = $productlist;
			$result['title'] = 'Recommended Based on your choice';
			$result['issue'] = 1;
			$result['pdoc'] = isset($doc_info['doc_id'])?$doc_info['doc_id']:0;
			$result['ndoc'] = $start_id;
			return $result;
		}
		public function addCategoryUser($category_arr,$user_info){
			$this->mongo_bookchor_library->users->updateOne(['_id'=>$user_info['user_id']],['$set'=>['user_category'=>$category_arr]]);
		}
		public function getSelectedcategory($user_info){
			$query_category = $this->mongo_bookchor_library->users->aggregate([
				[
					'$match'=>['_id'=>$user_info['user_id']]
				],
				[
					'$project'=>['user_category'=>1]
				]
			]);
			$user_category = iterator_to_array($query_category);
			return $user_category[0]['user_category'];
		}
	}
?>