<?php
	class Cron{
		public function __construct(){
			$con = new Connections();
			$this->mongo_bookchor_library = $con->mongo_bookchor_library();
			$this->library = $con->main_db();
			$this->timestamp = (new DateTime())->getTimestamp();
		}
		public function fine_cron(){
			$timestamp = (new DateTime())->getTimestamp();
			$library_query = $this->mongo_bookchor_library->library->find(['status'=>1],['projection'=>['fine'=>1]]);
			$libraries = iterator_to_array($library_query);
			foreach($libraries as $library){
				foreach($library['fine'] as $fine){
					if($fine['_id']==new MongoDB\BSON\ObjectId('5b0196bd4089a103dc851957')){
						$fine_day_delay = $fine['fine_delay'];
						$fine_delay = $fine_day_delay*24*60*60;
						$fine_amount = $fine['fine_amount'];
					}
				}
				$library_id = $library['_id'];
				$user_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'issue'=>['$elemMatch'=>['status'=>1,'date_issued'=>['$lt'=>$timestamp-$fine_delay]]]]],['$project'=>['issue'=>1]],['$unwind'=>'$issue'],['$match'=>['issue'=>['$elemMatch'=>['status'=>1,'date_issued'=>['$lt'=>$timestamp-$fine_delay]]]]]]);
				$user_defaulters = iterator_to_array($user_query);
				foreach($user_defaulters as $user_defaulter){
					$issue = $user_defaulter['issue'][0];
					$issue_id = $issue['_id'];
					$total_fine = $fine_day_delay*floor(($timestamp-$issue['date_added'])/24*60*60);
					$fine[] = array(
						'fine_type_id'=> new MongoDB\BSON\ObjectId('5af409d76577193010d3f0c9'),
						'amount'=> $total_fine,
						'date_added'=> date('Y-m-d H:i:s'),
					);
					$insert_fine_query = $this->mongo_bookchor_library->users->updateOne(['issue'=>['$elemMatch'=>['_id'=>$issue_id]]],['$set'=>['issue.fine'=>$fine]]);
				}
			}
		}
		public function books_live($library_id=0){
			$temp_query = $this->mongo_bookchor_library->temp_stock->find(['print'=>1]);
			$temps = iterator_to_array($temp_query);
			foreach($temps as $temp){
				$isbn = $temp['isbn'];
				$library_id = $temp['library_id'];
				$sku = $temp['sku'];
				$barcode = $temp['barcode'];
				$print = $temp['print'];
				$condition = $temp['condition'];
				$pass = urlencode('Success^1218#');
				$url = "http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/select?indent=on&q=isbn:".$isbn."&wt=json";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
				$json_data = curl_exec($ch);
				curl_close($ch);
				$raw_data = json_decode($json_data,true)['response']['docs'];
				if(isset($raw_data)&&$raw_data[0]['id']){
					$product_data = $raw_data[0];
					$pid  = (int) $product_data['id'];
					if($pid){
						$books_in_query = $this->mongo_bookchor_library->books->updateOne(['product_id'=>(string) $pid,'library_id'=> $library_id,'isbn'=>(string) $isbn,'stock'=>['$not'=>['$elemMatch'=>['barcode'=>$barcode,'status'=>1]]]],['$addToSet'=>['stock'=>['sku'=>$sku,'barcode'=> sprintf("%08d",$barcode),'condition'=>$condition,'status'=>1,'out'=>0,'date_added'=>$this->timestamp]]],['upsert'=>true]);
						if($books_in_query->getUpsertedCount()||$books_in_query->getModifiedCount()){
							$delete_query = $this->mongo_bookchor_library->temp_stock->deleteOne(['barcode'=>(string) sprintf("%08d",$barcode),'library_id'=>$library_id]);
						}
					}
				}else{
					$query_temp = "INSERT INTO temp_isbn (isbn) VALUES (?)";
					$stmt_temp = $this->library->prepare($query_temp);
					$stmt_temp->bind_param("s",$temp_isbn);
					$temp_isbn = $temp['isbn'];  
					$stmt_temp->execute();	
				}
			}
		}
		public function solr_update(){
			$pass = urlencode('Success^1218#');
			$all_products_query = $this->mongo_bookchor_library->books->find(['stock.status'=>1],['projection'=>['stock'=>0]]);
			$all_products = iterator_to_array($all_products_query);
			foreach($all_products as $all_product){
				$lib_id = $all_product['library_id'];
				$pid = $all_product['product_id'];
				$data = array(
					"add" => array(
						"doc" => array(
							'id'=> (string) $all_product['_id'],
							"library_id"  => (string) $lib_id,
							"product_id" => (int) $pid,
							"stock" => 1
						),
					),
				);
				$data_string = json_encode($data);
				do{
					$ch = curl_init("http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor_library/update?wt=json");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($ch, CURLOPT_POST, TRUE);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					$responses = curl_exec($ch);
					echo $responses." ".$pid;
				}while(!$responses);
			}
			$ch = curl_init("http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/admin/cores?action=RELOAD&core=bookchor_library");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POST, FALSE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			$reload = curl_exec($ch);
		}
		public function product_mongo(){
			$all_products_query = $this->mongo_bookchor_library->books->find(['stock.status'=>1,'stock.out'=>0],['projection'=>['stock'=>0]]);
			$all_products = iterator_to_array($all_products_query);
			foreach($all_products as $all_product){
				$isbn = $all_product['isbn'];
				$url = "https://www.bookchor.com/webservices/product_detail_new.php?id=".$isbn."&type=detail";
				$json_data = file_get_contents($url);
				$product_detail = json_decode($json_data,true)['bookData'][0];
				$query_update = $this->mongo_bookchor_library->products->updateOne(['_id'=>$product_detail['id']],['$set'=>$product_detail],['upsert'=>true]);
				if(($query_update->getModifiedCount)||($query_update->getUpsertedCount)){
				
				}else{
					echo $isbn.'<br>';
				}
			}
		}
		public function temp_correct(){
			$query_temp = $this->mongo_bookchor_library->temp_stock->aggregate([['$lookup'=>['from'=>'books','localField'=>'barcode','foreignField'=>'stock.barcode','as'=>'match']],['$match'=>['match'=>['$exists'=>true,'$not'=>['$size'=>0]]]],['$project'=>['barcode'=>1]]]);
			$temp_remove = iterator_to_array($query_temp);
			foreach($temp_remove as $remvd){
				$delete_query = $this->mongo_bookchor_library->temp_stock->deleteOne(['barcode'=>$remvd['barcode']]);	
			}
		}
		public function book_duplicate(){
			$query_temp = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=> new MongoDB\BSON\ObjectId('5af409d76577193010d3f0c9')]],['$project'=>['stock.barcode'=>1,'stock.status'=>1]],['$unwind'=>'$stock'],['$match'=>['stock.status'=>1]],['$group'=>['_id'=>'$stock.barcode','count'=>['$sum'=>1]]],['$match'=>['count'=>['$gt'=>1]]],['$sort'=>['count'=>-1]]]);
			$temp_remove = iterator_to_array($query_temp);
			print_r($temp_remove);
			
		}
		public function book_correct($library_id){
			$query_temp = $this->mongo_bookchor_library->books->aggregate([['$match'=>['product_id'=>'0']],['$unwind'=>'$stock'],['$lookup'=>['from'=>'temp_stock','localField'=>'isbn','foreignField'=>'isbn','as'=>'match']]]);
			$temp_remove = iterator_to_array($query_temp);
			foreach($temp_remove as $temp){
				if($temp['match']){
				}else{
					$sku = $temp['stock']['sku'];
					$isbn = $temp['isbn'];
					$barcode = $temp['stock']['barcode'];
					$condition = $temp['stock']['condition'];
					$temp_books_in_query = $this->mongo_bookchor_library->temp_stock->insertOne(['library_id'=>$library_id,'sku'=>$sku,'isbn'=>(string) $isbn,'barcode'=>(string) $barcode,'condition'=>$condition,'print'=>1,'date_added'=>$this->timestamp]);
				}
			}
			$delete_query = $this->mongo_bookchor_library->books->deleteMany(['product_id'=>'0']);
		}
		public function missing_barcode(){
			/* $i = 1;
				while($i<3800){
				$barcode = (string) sprintf("%08d",$i);
				$query_missed_barcode = $this->mongo_bookchor_library->books->find(['stock.barcode'=>$barcode],['limit'=>1]);
				$missed_barcode = iterator_to_array($query_missed_barcode);
				if(!isset($missed_barcode[0])){
				$query_missed_temp = $this->mongo_bookchor_library->temp_stock->find(['barcode'=>$barcode],['limit'=>1]);
				$missed_temp = iterator_to_array($query_missed_temp);
				if(!isset($missed_temp[0])){
				$this->mongo_bookchor_library->tempbarcode->insertOne(['barcode'=>$barcode]);
				}
				}
				$i++;
			}	 */
			$query_missed_barcode = $this->mongo_bookchor_library->books->aggregate([['$unwind'=>'$stock'],['$match'=>['stock.status'=>0]],['$project'=>['stock.barcode'=>1]]]);
			$missed_barcode = iterator_to_array($query_missed_barcode);
			if(isset($missed_barcode)&&$missed_barcode){
				foreach($missed_barcode as $stock){
					$barcode = (string) sprintf("%08d",$stock['stock']['barcode']);
					//echo $barcode;die;
					//$this->mongo_bookchor_library->tempbarcode->insertOne(['barcode'=>$barcode]);
					$this->mongo_bookchor_library->books->updateOne(array('stock' => array('$elemMatch' => array('barcode' => $barcode))),['$unset'=>['stock.$.sku'=>"",'stock.$.barcode'=>"",'stock.$.condition'=>"",'stock.$.status'=>"",'stock.$.out'=>"",'stock.$.date_added'=>"",'stock.$.print'=>"",'stock.$.date_out'=>""]]);
					echo $barcode;die;
				}
			}
		}
		public function cronEbooks(){
			//$abs_path = dirname(__DIR__);
			$abs_path = '/home/bchor/public_html/library.bookchor.com';
			$target_dir = "/tempisbn/";
			$folder_dir = scandir($abs_path.$target_dir);
			unset($folder_dir[0]);
			unset($folder_dir[1]);
			foreach($folder_dir as $key=>$isbn_file){
				$isbn = explode('.',$isbn_file)[0];
				$pass = urlencode('Success^1218#');
				$url = "http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/select?indent=on&q=".$isbn."&wt=json";	
				$json = file_get_contents($url);
				$data = json_decode($json);
				$responses = $data->response->docs;
				if($responses){
					foreach($responses as $response){
						$pid = $response->id;
					}
					if($i%1000==0){
						$i=0;
						$timestamp = (new DateTime())->getTimestamp();
						mkdir($abs_path.'/ebooks/'.$timestamp);
					}
					$i++;
					rename($abs_path.$target_dir.$isbn_file, $abs_path.'/ebooks/'.$timestamp."/".$isbn_file);
					$data = array(
						"add" => array(
							"doc" => [
								"id" => $pid,
								"ebook" => array(
									"set" => '/ebooks/'.$timestamp."/".$isbn_file,
								),
							],
						),
					);
					$data_string = json_encode($data);
					$pass = urlencode('Success^1218#');
					do{
						$ch = curl_init("http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/update?wt=json");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
						curl_setopt($ch, CURLOPT_POST, TRUE);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
						$responses = curl_exec($ch);
						echo $responses." ".$pid." ".$isbn;
					}while(!$responses);
					die;
				}
			}
		}
	}
?>