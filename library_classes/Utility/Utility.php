<?php
class Utility{

    private $agent = "";
    private $browser;

    function __construct(){
        $this->agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
    }
    function getBrowser(){
        $browser = array("Navigator"            => "/Navigator(.*)/i",
                         "Firefox"              => "/Firefox(.*)/i",
                         "Internet Explorer"    => "/MSIE(.*)/i",
                         "Google Chrome"        => "/chrome(.*)/i",
                         "MAXTHON"              => "/MAXTHON(.*)/i",
                         "Opera"                => "/Opera(.*)/i",
                         );
        foreach($browser as $key => $value){
            if(preg_match($value, $this->agent)){
                $this->browser = $key;
				$this->browser_agent = $value;
				break;
            }else{
                $this->browser = "UnKnown";
            }
        }
        return $this->browser;
    }

    function getOS(){
        $OS = array("Windows"   =>   "/Windows/i",
                    "Linux"     =>   "/Linux/i",
                    "Unix"      =>   "/Unix/i",
                    "Mac"       =>   "/Mac/i"
                    );

        foreach($OS as $key => $value){
            if(preg_match($value, $this->agent)){
                $info = $key;
                break;
            }
        }
        return $info;
    }

    function getVersion(){
        $version = "";
        $browser = strtolower($this->browser);
        preg_match_all($this->browser_agent,$this->agent,$match);
        switch($browser){
            case "firefox": $version = str_replace("/","",$match[1][0]);
            break;

            case "internet explorer": $version = substr($match[1][0],0,4);
            break;

            case "opera": $version = str_replace("/","",substr($match[1][0],0,5));
            break;

            case "navigator": $version = substr($match[1][0],1,7);
            break;

            case "maxthon": $version = str_replace(")","",$match[1][0]);
            break;

            case "google chrome": $version = substr($match[1][0],1,10);
        }
        return $version;
    }

    function showDeviceInfo($switch){
        $switch = strtolower($switch);
        switch($switch){
            case "browser": return $this->getBrowser();
            break;

            case "os": return $this->getOS();
            break;

            case "version": return $this->getVersion();
            break;

            case "all" : return array("Browser" => $this->getBrowser(),
				"OS" => $this->getOS(),
				"Version" => $this->getVersion()
			);
            break;
        }
    }
    static function get_ip_details($ip_address){
    	$url = 'https://ipinfo.io/'.$ip_address.'/json';
		$json_data = json_decode(file_get_contents($url),true);
		return $json_data;
    }
	public function get_solr_password(){
		$query_password = "SELECT value from global_variables where type=?";
		$stmt_pass = $this->mysql_global->prepare($query_password);
		$pass = urlencode($result_password['value']);
	}
}


?>