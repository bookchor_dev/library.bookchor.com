<?php
require_once('PHPMailer/PHPMailerAutoload.php');
	class Mailer {
		public $mail;
		public $server;
		public function __construct(){
			$this->mail = new PHPMailer(true);
			$this->mail->isSMTP();
			$this->mail->Host = 'antispam.serverplanner.com';  // Specify main and backup SMTP servers
			$this->mail->SMTPAuth = true;                               // Enable SMTP authentication
			$this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$this->mail->Username = "info@bookchor.com"; // SMTP username
			$this->mail->Password = "R0cK@s#$";
			$this->mail->Port = 587;                                    // TCP port to connect to
			$this->mail->setFrom('info@bookchor.com', 'BookChor');     // Add a recipient               // Name is optional
			$this->mail->addReplyTo('cs@bookchor.com', 'Reply to:');
			$this->mail->SMTPOptions = array(
				'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true)
			);
			$this->mail->isHTML(true);
			$this->server = "http://wholesale.bookchor.com/email/";
			
		}
		public function mailPassword($email,$password){
			$this->mail->Subject = 'Reset Your Bookchor Account Password';
			header('Content-type: text/plain');
			$str = file_get_contents($this->server."password_reset.php?pass=".$password);
			$this->mail->Body = $str;
			$this->mail->AltBody = 'Reset Password';
			$this->mail->addAddress($email);
			$this->mail->send();
		}
		public function mailConfirmOrder($viewable_order_id,$email){
			$this->mail->Subject = 'Order('.$viewable_order_id.') has been Confirmed';
			header('Content-type: text/plain');
			$str = file_get_contents($this->server."invoice.php?order_id=".$viewable_order_id."&email=".$email);
			$this->mail->Body = $str;
			$this->mail->AltBody = 'Order Confirmed';
			$this->mail->addAddress($email);
			$this->mail->send();	
		}
		public function mailContactUs($contact){
			$name = $contact['name'];
			$email = $contact['email'];
			$message =  $contact['message'];
			$phone = $contact['phone'];
			$this->mail->Subject = 'School Library Query';
			$this->mail->Body = "Name: ".$name."<br> Email: ".$email."<br> Phone: ".$phone."<br> Query: ".$message;
			$this->mail->AltBody = 'School Library Query';
			$this->mail->addAddress('bhavesh@bookchor.com');
			$this->mail->addCC('argo@bookchor.com');
			$this->mail->send();
		}
		/* public function mailCancelOrder($order_id){
			$this->mail->Subject = 'Order('$viewable_order_id') has been Cancelled';
			$file = file_get_contents("../../email/order_cancel.php?order_id=".$order_id);
			$this->mail->Body = $str;
			$this->mail->AltBody = 'Order Cancelled';
			$this->mail->addAddress($email);
			$this->mail->send();			
		}
		public function mailCancelProduct($order_id,$vendor_product_id){
			$this->mail->Subject = 'Product of Order('$viewable_order_id') has been Cancelled';
			$file = file_get_contents("../../email/product_cancel.php?order_id=".$order_id);
			$this->mail->Body = $str;
			$this->mail->AltBody = 'Order Confirmed';
			$this->mail->addAddress($email);
			$this->mail->send();
		} */
		
	}
?>