<?php
	require 'PHPMailerAutoload.php';
	class Mail {
		public function sendRegisterationMail($customer_email) {	
			$mail = new PHPMailer;
			$mail->Host = 'smtp.zoho.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = false;                               // Enable SMTP authentication
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Username = "cs@bookchor.com"; // SMTP username
			$mail->Password = "Success@1218";
			$mail->Port = 587;                                    // TCP port to connect to
			$mail->setFrom('info@bookchor.com', 'BookChor');     // Add a recipient               // Name is optional
			$mail->addReplyTo('info@bookchor.com', 'Reply to:');
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Welcome To BookChor';
			$mail->Body ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
				<title>Welcome to BookChor</title>
				<style type="text/css">
			.ReadMsgBody { width: 100%; background-color: #ffffff; }
			.ExternalClass { width: 100%; background-color: #ffffff; }
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
			html { width: 100%; }
			body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
			table { border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto; }
			table table table { table-layout: auto; }
			img { display: block !important; }
			table td { border-collapse: collapse; }
			.yshortcuts a { border-bottom: none !important; }
			a { color: #21b6ae; text-decoration: none; }
			.textbutton a { font-family: \'Open Sans\', arial, sans-serif !important; color: #ffffff !important; }
			.text-link a { color: #95a5a6 !important; }
			 @media only screen and (max-width: 640px) {
			body { width: auto !important; }
			table[class="table600"] { width: 450px !important; }
			table[class="table-inner"] { width: 90% !important; }
			table[class="table3-3"] { width: 100% !important; text-align: center !important; }
			}
			 @media only screen and (max-width: 479px) {
			body { width: auto !important; }
			table[class="table600"] { width: 290px !important; }
			table[class="table-inner"] { width: 82% !important; }
			table[class="table3-3"] { width: 100% !important; text-align: center !important; }
			}
			</style>

			</head>

			<body>
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#494c50">
					<tr>
						<td align="center" background="http://www.bookchor.com/images/mailer/register/bg.jpg" style="background-size:cover; background-position:top;">
							<table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
								
									<td height="50"></td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>

								<!-- logo -->
								<tr>
									<td align="center" style="line-height: 0px;">
										<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/book-chor-logo.png" width="256" height="58" alt="logo" />
									</td>
								</tr>
								<!-- end logo -->
								<tr>
									<td height="20"></td>
								</tr>

								<!-- content -->
								<tr>
									<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:11px; color:#ffffff; line-height:26px; text-transform:uppercase; letter-spacing:4px;">Robinhood of TextVille</td>
								</tr>
								<!-- end content -->

								<tr>
									<td height="40"></td>
								</tr>
								
								<tr>
									<td align="center">
										<table align="center" bgcolor="#FFFFFF" style="border-radius:4px; box-shadow: 0px -3px 0px #d4d2d2;" width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="50"></td>
											</tr>

											<tr>
												<td align="center">
													<table align="center" class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
														<!-- title -->
														<tr>
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:36px; color:#3b3b3b; font-weight: bold; letter-spacing:4px;">Welcome to BookChor</td>
														</tr>
														<!-- end title -->

														<tr>
															<td align="center">
																<table width="25" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td height="20" style="border-bottom:2px solid #21b6ae;"></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td height="20"></td>
														</tr>
														<!-- content -->
														<tr>
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																At BookChor, we cater to the need of your every book.
															</td>
														</tr>
														<tr>
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																<b>Please click 
																<a href="http://www.bookchor.com/activate.php?csid='.md5($customer_email).'" target="_BLANK">here</a>
																to confirm your email address.
																</b>
															</td>
														</tr>
														<tr>	
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																We aim to provide you with an easy, convenient and hassle free shopping experience with:
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;">Free Shipping Over ? 200 all over India</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;">Cash On Delivery</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;">Free 15-Day Replacement</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;"><a href="http://www.bookchor.com/Request-A-Book">Request Book</a>
																		| <a href="http://www.bookchor.com/Sell-A-Book">Sell Book</a></td>
																	</tr>
																</table>
															</td>
														</tr>
														
														<!-- end content -->
													</table>
												</td>
											</tr>

											<tr>
												<td height="40"></td>
											</tr>

											<!-- button -->
											<tr>
												<td align="center">
													<table class="textbutton" align="center" bgcolor="#21b6ae" border="0" cellspacing="0" cellpadding="0" style=" border-radius:30px; box-shadow: 0px 1px 0px #d4d2d2;">
														<tr>
															<td height="55" align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:18px; color:#7f8c8d; line-height:30px; font-weight: bold;padding-left: 25px;padding-right: 25px;">
																<a href="http://www.bookchor.com">Start Shopping</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<!-- end button -->

											<tr>
												<td height="45"></td>
											</tr>

											<!-- option -->
											<tr>
												<td align="center" bgcolor="#f3f3f3" style=" border-bottom-left-radius:4px; border-bottom-right-radius:4px;">
													<table align="center" class="table-inner" width="290" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td height="15"></td>
														</tr>
														<tr>
															<td class="text-link" align="center">

																<!-- left -->
																<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																			<a href="http://www.bookchor.com/Contact">Need Help?</a>
																		</td>
																	</tr>
																</table>
																<!-- end left -->

																<!--Space-->

																<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
																	<tr>
																		<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
																			<p style="padding-left: 24px;">&nbsp;</p>
																		</td>
																	</tr>
																</table>

																<!--End Space-->

																<!-- middle -->
																<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																			<a href="http://www.bookchor.com/About">About us</a>
																		</td>
																	</tr>
																</table>
																<!-- end middle -->

																<!--Space-->

																<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
																	<tr>
																		<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
																			<p style="padding-left: 24px;">&nbsp;</p>
																		</td>
																	</tr>
																</table>

																<!--End Space-->

																<!-- right -->
																<table class="table3-3" width="80" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																			<a href="#">Unsubscribe</a>
																		</td>
																	</tr>
																</table>
																<!-- end right -->

															</td>
														</tr>
														<tr>
															<td height="15"></td>
														</tr>
													</table>

												</td>
											</tr>
											<!-- end option -->

										</table>
									</td>
								</tr>
								<tr>
									<td align="center" style="line-height:0px;">
										<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/point.png" width="42" height="22" alt="img" />
									</td>
								</tr>

								<tr>
									<td height="30"></td>
								</tr>

								<!-- profile-img -->
								<tr>
									<td align="center" style="line-height:0px;">
										<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/profile.png" width="100" height="100" alt="img" />
									</td>
								</tr>
								<!-- end profile-img -->

								<tr>
									<td height="30"></td>
								</tr>

								<!-- social -->
								<tr>
									<td align="center">
										<table align="center" width="221" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="16" align="center" style="line-height:0xp;">
													<a href="http://www.facebook.com/bookchor">
														<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/facebook.png" width="8" height="16" alt="img" />
													</a>
												</td>
												<td width="25"></td>
												<td width="16" align="center" style="line-height:0xp;">
													<a href="https://twitter.com/bookchor">
														<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/twitter.png" width="16" height="13" alt="img" />
													</a>
												</td>
												<td width="25"></td>
												<td width="16" align="center" style="line-height:0xp;">
													<a href="https://plus.google.com/102635021069224001244">
														<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/googleplus.png" width="15" height="16" alt="img" />
													</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- end social -->

								<tr>
									<td height="30"></td>
								</tr>

								<!-- copyright -->
								<tr>
									<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#ffffff; line-height:30px;">� 2016 BookChor Literary Solutions Private Limited. All Rights Reserved.</td>
								</tr>
								<!-- end copyright -->

								<tr>
									<td height="30"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
			</html>';
				$mail->AltBody = 'Welcome to BookChor';
				$mail->addAddress($customer_email);
				$mail->send();
		}
	}
?>