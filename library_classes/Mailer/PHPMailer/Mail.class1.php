<?php
	require 'PHPMailerAutoload.php';
	class Mail {
		public function sendRegisterationMail($customer_email) {	
			$mail = new PHPMailer;
			$mail->Host = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = false;                               // Enable SMTP authentication
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Username = "bookchor"; // SMTP username
			$mail->Password = "Success@1218";
			$mail->Port = 587;                                    // TCP port to connect to
			$mail->setFrom('info@bookchor.com', 'BookChor');     // Add a recipient               // Name is optional
			$mail->addReplyTo('info@bookchor.com', 'Reply to:');
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Welcome To BookChor';
			$mail->Body ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
				<title>Welcome to BookChor</title>
				<style type="text/css">
			.ReadMsgBody { width: 100%; background-color: #ffffff; }
			.ExternalClass { width: 100%; background-color: #ffffff; }
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
			html { width: 100%; }
			body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
			table { border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto; }
			table table table { table-layout: auto; }
			img { display: block !important; }
			table td { border-collapse: collapse; }
			.yshortcuts a { border-bottom: none !important; }
			a { color: #21b6ae; text-decoration: none; }
			.textbutton a { font-family: \'Open Sans\', arial, sans-serif !important; color: #ffffff !important; }
			.text-link a { color: #95a5a6 !important; }
			 @media only screen and (max-width: 640px) {
			body { width: auto !important; }
			table[class="table600"] { width: 450px !important; }
			table[class="table-inner"] { width: 90% !important; }
			table[class="table3-3"] { width: 100% !important; text-align: center !important; }
			}
			 @media only screen and (max-width: 479px) {
			body { width: auto !important; }
			table[class="table600"] { width: 290px !important; }
			table[class="table-inner"] { width: 82% !important; }
			table[class="table3-3"] { width: 100% !important; text-align: center !important; }
			}
			</style>

			</head>

			<body>
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#494c50">
					<tr>
						<td align="center" background="http://www.bookchor.com/images/mailer/register/bg.jpg" style="background-size:cover; background-position:top;">
							<table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
								
									<td height="50"></td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>

								<!-- logo -->
								<tr>
									<td align="center" style="line-height: 0px;">
										<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/book-chor-logo.png" width="256" height="58" alt="logo" />
									</td>
								</tr>
								<!-- end logo -->
								<tr>
									<td height="20"></td>
								</tr>

								<!-- content -->
								<tr>
									<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:11px; color:#ffffff; line-height:26px; text-transform:uppercase; letter-spacing:4px;">Robinhood of TextVille</td>
								</tr>
								<!-- end content -->

								<tr>
									<td height="40"></td>
								</tr>
								
								<tr>
									<td align="center">
										<table align="center" bgcolor="#FFFFFF" style="border-radius:4px; box-shadow: 0px -3px 0px #d4d2d2;" width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="50"></td>
											</tr>

											<tr>
												<td align="center">
													<table align="center" class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
														<!-- title -->
														<tr>
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:36px; color:#3b3b3b; font-weight: bold; letter-spacing:4px;">Welcome to BookChor</td>
														</tr>
														<!-- end title -->

														<tr>
															<td align="center">
																<table width="25" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td height="20" style="border-bottom:2px solid #21b6ae;"></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td height="20"></td>
														</tr>
														<!-- content -->
														<tr>
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																At BookChor, we cater to the need of your every book.
															</td>
														</tr>
															
														<tr>	
															<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																We aim to provide you with an easy, convenient and hassle free shopping experience with:
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;">Free Shipping Over ? 200 all over India</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;">Cash On Delivery</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;">Free 15-Day Replacement</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="center" style="border-bottom:1px solid #ecf0f1;" height="40">
																<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td width="17" align="center">
																			<img src="http://www.bookchor.com/images/mailer/register/check.png" width="17" height="14" alt="check" />
																		</td>
																		<td width="15"></td>
																		<td style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#676767; line-height:24px;"><a href="http://www.bookchor.com/Request-A-Book">Request Book</a>
																		| <a href="http://www.bookchor.com/Sell-A-Book">Sell Book</a></td>
																	</tr>
																</table>
															</td>
														</tr>
														
														<!-- end content -->
													</table>
												</td>
											</tr>

											<tr>
												<td height="40"></td>
											</tr>

											<!-- button -->
											<tr>
												<td align="center">
													<table class="textbutton" align="center" bgcolor="#21b6ae" border="0" cellspacing="0" cellpadding="0" style=" border-radius:30px; box-shadow: 0px 1px 0px #d4d2d2;">
														<tr>
															<td height="55" align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:18px; color:#7f8c8d; line-height:30px; font-weight: bold;padding-left: 25px;padding-right: 25px;">
																<a href="http://www.bookchor.com">Start Shopping</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<!-- end button -->

											<tr>
												<td height="45"></td>
											</tr>

											<!-- option -->
											<tr>
												<td align="center" bgcolor="#f3f3f3" style=" border-bottom-left-radius:4px; border-bottom-right-radius:4px;">
													<table align="center" class="table-inner" width="290" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td height="15"></td>
														</tr>
														<tr>
															<td class="text-link" align="center">

																<!-- left -->
																<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																			<a href="http://www.bookchor.com/Contact">Need Help?</a>
																		</td>
																	</tr>
																</table>
																<!-- end left -->

																<!--Space-->

																<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
																	<tr>
																		<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
																			<p style="padding-left: 24px;">&nbsp;</p>
																		</td>
																	</tr>
																</table>

																<!--End Space-->

																<!-- middle -->
																<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																			<a href="http://www.bookchor.com/About">About us</a>
																		</td>
																	</tr>
																</table>
																<!-- end middle -->

																<!--Space-->

																<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
																	<tr>
																		<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
																			<p style="padding-left: 24px;">&nbsp;</p>
																		</td>
																	</tr>
																</table>

																<!--End Space-->

																<!-- right -->
																<table class="table3-3" width="80" border="0" align="right" cellpadding="0" cellspacing="0">
																	<tr>
																		<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
																			<a href="#">Unsubscribe</a>
																		</td>
																	</tr>
																</table>
																<!-- end right -->

															</td>
														</tr>
														<tr>
															<td height="15"></td>
														</tr>
													</table>

												</td>
											</tr>
											<!-- end option -->

										</table>
									</td>
								</tr>
								<tr>
									<td align="center" style="line-height:0px;">
										<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/point.png" width="42" height="22" alt="img" />
									</td>
								</tr>

								<tr>
									<td height="30"></td>
								</tr>

								<!-- profile-img -->
								<tr>
									<td align="center" style="line-height:0px;">
										<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/profile.png" width="100" height="100" alt="img" />
									</td>
								</tr>
								<!-- end profile-img -->

								<tr>
									<td height="30"></td>
								</tr>

								<!-- social -->
								<tr>
									<td align="center">
										<table align="center" width="221" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="16" align="center" style="line-height:0xp;">
													<a href="http://www.facebook.com/bookchor">
														<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/facebook.png" width="8" height="16" alt="img" />
													</a>
												</td>
												<td width="25"></td>
												<td width="16" align="center" style="line-height:0xp;">
													<a href="https://twitter.com/bookchor">
														<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/twitter.png" width="16" height="13" alt="img" />
													</a>
												</td>
												<td width="25"></td>
												<td width="16" align="center" style="line-height:0xp;">
													<a href="https://plus.google.com/102635021069224001244">
														<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://www.bookchor.com/images/mailer/register/googleplus.png" width="15" height="16" alt="img" />
													</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- end social -->

								<tr>
									<td height="30"></td>
								</tr>

								<!-- copyright -->
								<tr>
									<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#ffffff; line-height:30px;">� 2016 BookChor Literary Solutions Private Limited. All Rights Reserved.</td>
								</tr>
								<!-- end copyright -->

								<tr>
									<td height="30"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
			</html>';
				$mail->AltBody = 'Welcome to BookChor';
				$mail->addAddress($customer_email);
				$mail->send();
		}
		public function sendDealsMail($customer_email) {	
			$con=mysqli_connect("localhost","root","","buymebook_final");
			$mail = new PHPMailer;
			$mail->Host = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = false;                               // Enable SMTP authentication
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Username = "bookchor"; // SMTP username
			$mail->Password = "Success@1218";
			$mail->Port = 587;                                    // TCP port to connect to
			$mail->setFrom('info@bookchor.com', 'BookChor');     // Add a recipient               // Name is optional
			$mail->addReplyTo('info@bookchor.com', 'Reply to:');
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'Welcome To BookChor';
			$mail->body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>BookChor - Deals of The Day</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <meta content="text/html;charset=utf-8" http-equiv="content-type">
   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,500,700,400italic" rel="stylesheet" type="text/css"> 
   <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

<style type="text/css">

    @charset "utf-8";
    /* CSS Document */
    
    @media screen and (max-width: 768px) and (min-width: 381px) {
	    body {
		    margin: 0 auto !important;
		    width: 600px !important;
	    }
	    .wrapper_table {
		    width: 600px !important;
		    margin:0 auto;
		    text-align:center;
	    }
	    .padding {
		    width: 20px !important;
	    }
	    .logo{
		    width: 200px !important;
	    }
	    .navbar{
		    width: 400px !important;
	    }
	    .content{
		    width: 600px !important;
	    }
	    .content_row {
		    width: 560px !important;
	    }
	    .content_row img {
		    max-width: 560px;
		    height: auto !important;
	    }	
	    .content_row_inner {
		    width: 520px !important;
	    }
	    .one_third {
		    width: 170px !important;
		    margin-bottom: 30px;
	    }
	    .one_third img {
	    		max-width: 170px !important;
	    }
	    .one_half {
		    width: 270px !important;
	    }
	    .one_half img {
		    max-width: 270px !important;
		    height: auto !important;
	    }
	    .noresponsive {
		    display: none;
	    }
	    
	    .fullwidth_image {
		    max-width: 600px;
	    }
	    .one_fourth {
		    width: 140px !important;
	    }
	    .header_third_space {
		    width: 20px !important;
	    }
	    .hero_musician {
		    width: 560px !important;
		    margin: 0 20px !important;
		    color: #ffffff !important;
		    border-color: #ffffff !important;
	    }
	    .hero_musician a {
		    color: #ffffff !important; 
	    }
	    .one_quarter {
		    width: 140px !important;
		    float: left;
	    }
    }
    @media screen and (max-width: 381px) {
	    
	    body {
		    margin: 0 auto !important;
		    width: 320px !important;
	    }
	    
	    table[class="wrapper_table"] {
		    width: 320px !important;
	    }
	    
	    img[class="fullwidth_image"] {
		    max-width: 320px !important;
		    height: auto !important
	    }
	    
	    td[class="padding"] {
		    width: 10px !important;
	    }
	    
	    td[class="content"] {
		    width: 320px !important;
	    }
	    
	    td[class="content"] img {
	    		max-width: 320px !important;
			height: auto !important;
	    }
	    
	    td[class="content_row"] {
		    width: 300px !important;
	    }
	    
	    td[class="content_row_inner"] {
		    width: 280px !important;
	    }
	    
	    td[class="logo"] img {
		    margin: 0 auto !important;
	    }
	    
	    td[class="logo"] {
		    margin: 0 auto;
		    width: 300px !important;
		    float: left;
		    margin-bottom: 15px;
	    }
	    
	    td[class="navbar"] {
		    float: left;
		    width: 300px !important;
		    margin-bottom: 15px;
		    text-align: center !important;
	    }
	    
	    td[class="header_right"] {
		    width: 300px !important;
		    float: left;
		    margin-bottom: 10px;
		    text-align: center;
	    }
	    
	    table[class="mobile_centered_table"] {
		    margin: 0 auto !important;
		    text-align: center !important;
	    }
	    
	    td[class="one_third"] {
		    display: block;
		    width: 300px !important;
		    margin-bottom: 30px;
		    float: left;
		    text-align: center !important;
	    }
	    
	    td[class="one_third"] img {
		    margin: 0 auto;
		    max-width: 300px !important;
		    height: auto !important;
	    }
	    
	    td[class="one_third"] td[class="noresize"] {
		    margin: 0 auto;
	    }
	    
	    td[class="one_third_spacer"] {
		    display: none;
	    }
	    
	    td[class="one_half"] {
		    width: 300px !important;
		    display: block;
		    margin-bottom: 20px;
		    text-align: center;
	    }
	    
	    td[class="one_quarter"] {
		    width: 140px !important;
	    }
	    
	    td[class="one_quarter"] img {
		    max-width: 140px !important;
		    text-align: center;
	    }
	    
	    td[class="one_half_spacer"] {
		    display: none;
	    }
	    
	    td[class="one_half"] img{
		    margin: 0 auto;
		    max-width: 300px !important;
		    height: auto !important;
	    }
	    
	    td[class="text_center"] {
		    text-align: center !important;
	    }
	    td[class="round_social_button"] img{
		    width: 45px !important;
		    height: auto !important;
	    }
	    table[id="social_bg"]{
		    padding: 0 10px;
	    }
	    td[class="header_third_space"]{
		    display: none;
	    }
	    span[class="mobile_black_text"]{
	    		color: #464646 !important;
	    }
    }
    
    @media screen and (max-width: 381px) {
	    td[class="nomobile"] {
		    display: none !important;
	    }
	    td[class="noresponsive"] {
		    display: none;
	    }
	    span[class="huge_h"]{
		    font-size: 60px;
	    }
	    td[class="aligncenter"]{
		    text-align: center !important;
		    margin: 0 auto !important;
	    }
	    td[class="section_h"]{
		    height: 25px !important;
	    }
	    td[class="padding_top_twenty"]{
	    		padding-top: 20px !important;
	    }
	    span[class="big_h"]{
	    		font-size: 16px;
	    }
	    table[id="bg1"]{
	    		background: none !important;
	    }
	    td[class="hero_musician"]{
		    width: 300px !important;
	    }
	    td[class="spacing_h"]{
	    		height: 20px !important;
	    }
	    table[id="bg2"]{
	    		background: none !important;
			color: #464646 !important;
	    }
	    table[id="bg4"]{
	    		background: none !important;
	    }
	    table[id="bg3"]{
	    		background-size: 100% 350px!important;
	    }
	    table[id="bg5"]{
	    		background-size: 100% 470px!important;
	    }
	    td[class="one_half_nomargins"]{
	    		width: 320px !important;
			float: left;
	    }
	    td[class="one_fourth"]{
	    		width: 160px !important;
			float: left;
	    }
	    td[class="one_half_nomargins"] img {
	    		max-width: 320px !important;
			height: auto !important;
	    }
	    td[class="two_thirds"]{
	    		width: 300px !important;
			float: left;
			display: block;
			margin-bottom: 20px;
	    }
	    td[class="two_thirds"] img {
	    		max-width: 300px !important;
			height: auto !important;
	    }
	    td[class="inst_text"]{
			text-align:left;
	    }
	    td[class="one_third_nomargins"] {
	    		width:300px !important;
			display:block !important;
	    }
	    td[class="one_quarter"] {
		    width:300px !important;
		    display:block !important;
	    }
	    td[class="one_quarter"] img {
		    margin-bottom:20px !important;
	    }
	    td[class="full_span"] {
		    width:300px !important;
		    display:block !important;
	    }
	    td[class="three_quarters"] {
		    width:300px !important;
		    display: table-footer-group !important;
	    }
	    td[class="one_quarter_head"] {
		    width:300px !important;
		    display: table-header-group !important;
		    
	    }
	    td[class="one_quarter_head"] img {
		    margin-bottom:20px !important;
		    
	    }
	    td[class="display_table"] {
		    display:table !important;
		    margin:0 auto !important;
	    }
	    td[class="one_half_header"] {
		    width:300px !important;
		    display: table-header-group !important;
		    
	    }
	    td[class="one_half_header"] img {
		    margin-bottom:20px !important;
	    }
		    
	    td[class="one_half_footer"] {
		    width:300px !important;
		    display: table-footer-group !important;
		    
	    }
	    td[class="stack_img"] {
		    width:300px !important;
		    display:block !important;
	    
	    }
	    td[class="stack_img"] img {
		    padding-bottom:10px !important;
	    
	    }
	    td[class="full_span"] img {
		    max-width:300px !important;
		    height:auto !important;
	    
	    }
	    td[class="one_quarter_nomargins"] {
	    		width:300px !important;
			display:block !important;
			padding: 0 10px !important;
			float:left !important;
	    }
	    td[class="one_fifth"] {
	    		width:104px !important;
			display:block;
			margin-bottom:20px !important;
	    }
	    table[id="face1"]{
		    background-image: none !important;
		    background-color: #08a9fa !important;
	    }
	    table[id="face2"]{
		    background-image: none !important;
		    background-color: #08a9fa !important;
	    }
	    
	    
    }
    
    @media screen and (max-width: 768px) and (min-width: 381px) {
	    .section_h{
		    height: 30px !important
	    }
	    #bg2 {
		    background-size: 100% 200px !important;
	    }
	    .spacing_h {
	    		height: 20px !important;
	    }
	    #bg3 {
	    		background-size: 100% 300px !important;
	    }
	    #bg {
	    		background-size: 100% 300px !important;
	    }
	    #bg1 {
	    		background-size: 100% 380px !important;
	    }
	    .padding_phone {
	    		display: none;
	    }
	    .one_half_nomargins {
	   		width: 300px !important;
	    }
	    .one_half_nomargins img {
	    		max-width: 300px !important;
	    }
	    .one_fourth {
	    		width: 150px !important;
	    }
	    .two_thirds {
	    		width: 374px !important;
	    }
	    .two_thirds img {
	    		width: 374px !important;
			height: 140px !important;
	    }
	    .big_h {
	    		font-size: 18px !important;
			line-height: 22px !important;
	    }
	    .one_half img {
	    		max-width: 270px !important;
	    }
	    .one_quarter img {
	    		max-width: 130px !important;
			height: auto !important;
	    }
	    .one_fifth {
	    		width:104px !important;
			display:block;
	    }
	    .one_third_nomargins{
	    		width:186px !important;
		 	display:block !important;
			float:left !important;
	    }
	    .one_third_nomargins img{
		    max-width:186px !important;
		    height:auto !important;
	    }
	    .full_span {
	    		width:560px !important;
			display:block !important;
			margin-bottom:15px !important;
	    }
	    .three_quarters{
	    		width:400px !important;
			display:block !important;
	    }
	    .one_half_footer{
		    width:270px !important;
		    display:block !important;
		    float:left !important;
	    }
	    .one_half_header{
		    width:270px !important;
		    display:block !important;
		    float:left !important;
	    }
	    .one_half_header img{
		    max-width:270px !important;
		    height:auto !important;
	    }
	    .no_tablet {
		    display:none !important;
	    }
	    .full_button{
	    		width:100% !important;
			display:block !important;
			margin-bottom:20px !important;
	    }
	    .one_quarter_nomargins {
	    		width: 150px !important;
			display:block;
			float:left;
	    }
	    #face1 {
	    		background-size: 100% 400px !important;
	    }
	    #face2 {
	    		background-size: 100% 400px !important;
	    }
    }

</style>

</head>

<body style="background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #393939; margin: 0 auto; width: 800px;">		
	
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td align="center">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td height="25">
							</td>
						</tr>
						<tr>
							<td class="content" style="width: 800px;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<td class="padding" style="width: 20px;">
										</td>
										<td class="content_row" style="width: 760px;" align="center">
											<table class="mobile_centered_table" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
												<tbody><tr>
													<td class="one_half" align="left" width="365">
														<a href="#" target="_blank" style="text-decoration:none;">
															<img src="http://www.bookchor.com/database/mailer/book-chor-logo-black.png" style="display:block;" alt="" height="17" border="0" width="74">
														</a>
													</td>
													<td class="nomobile" width="30">
													</td>
													<td class="one_half" data-editable="" align="right" width="365">
														<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
															<tbody><tr>
																<td>
																	<a href="#" target="_blank" style="text-decoration:none">	
																		<img src="http://www.bookchor.com/database/mailer/tlf.png" style="display:block;" alt="" height="10" border="0" width="11">
																	</a>	
																</td>
																<td width="10">
																</td>
																<td class="black_text" data-editable="" style="font-family: Montserrat, sans-serif; font-size:12px; line-height:24px; color:#3b3f40;">
																	+91-9466501218
																</td>
																<td width="15">
																</td>
																<td>
																	<a href="#" target="_blank" style="text-decoration:none">	
																		<img src="http://www.bookchor.com/database/mailer/mail.png" style="display:block;" alt="" height="10" border="0" width="12">
																	</a>	
																</td>
																<td width="10">
																</td>
																<td class="black_text" data-editable="" style="font-family: Montserrat, sans-serif; font-size:12px; line-height:24px; color:#3b3f40;">
																	info@bookchor.com	
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
											</tbody></table>
										</td>
										<td class="padding" style="width: 20px;">
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
						<tr>
							<td height="25">
							</td>
						</tr>
					</tbody></table>
				</td>
			</tr>
			
			<tr>
				<td class="content_row" align="center">
					<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td class="blue_bg" height="1" bgcolor="#08a9fa" width="800">
							</td>
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody>
		</table>
	
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody>
			
			<tr>
				<td align="center">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody>
						
						<tr>
							<td height="25">
							</td>
						</tr>
						<tr>
							<td class="content" style="width: 800px;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<td class="padding" style="width: 20px;">
										</td>
										<td class="content_row" style="width: 760px;" align="center">
											<table class="mobile_centered_table" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
												<tbody><tr>
													<td class="one_half" data-editable="" align="left" width="550">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody><tr>
																<td data-editable="" style="font-family: Montserrat, sans-serif; font-size: 12px; color: #3b3f40; font-weight:400; line-height:24px; text-transform:uppercase;">
																	<a class="black_text" href="http://www.bookchor.com" target="_blank" style="color: #3b3f40; text-decoration: none; font-weight:400;">
																	Home &nbsp;&nbsp;
																	</a>
																	<a class="black_text" href="http://www.bookchor.com/Featured-Books/1" target="_blank" style="color: #3b3f40; text-decoration: none; font-weight:400;">
																	Featured &nbsp;&nbsp;
																	</a>
																	<a class="black_text" href="http://www.bookchor.com/Newly-Added-Books/1" target="_blank" style="color: #3b3f40; text-decoration: none; font-weight:400; text-align:center;">
																	Newly Added &nbsp;&nbsp;
																	</a>
																	<a class="black_text" href="http://www.bookchor.com/Sell-A-Book" target="_blank" style="color: #3b3f40; text-decoration: none; font-weight:400; text-align:center;">
																	Sell &nbsp;&nbsp;
																	</a>
																	<a class="black_text" href="http://www.bookchor.com/Donate-A-Book" target="_blank" style="color: #3b3f40; text-decoration: none; font-weight:400; text-align:center;">
																	Donate
																	</a>
																</td>
															</tr>
														</tbody></table>
													</td>
													<td class="one_half" data-editable="" align="right" width="365">
														<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
															<tbody><tr>
																<td width="10">
																</td>
																<td width="15">
																</td>
																<td>
																	<a href="http://www.bookchor.com" target="_blank" style="text-decoration:none">	
																		<img src="http://www.bookchor.com/database/mailer/magn.png" style="display:block;" alt="" height="11" border="0" width="11">
																	</a>	
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
												
											</tbody></table>
										</td>
										<td class="padding" style="width: 20px;">
										</td>
									</tr>
									
								</tbody></table>
							</td>
						</tr>
						<tr>
							<td height="25">
							</td>
						</tr>
						<tr>
							<td class="content_row" align="center">
								<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
									<tbody><tr>
										<td class="blue_bg" height="1" bgcolor="#08a9fa" width="800">
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
			<!--header end-->
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td align="center" bgcolor="#ffffff" width="100%">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td class="content" style="width: 800px;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<td class="padding" width="60">
										</td>
										<td class="content_row" style="width: 680px;" align="center">
											<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
												<tbody><tr>
													<td class="content_row" align="center" valign="top" width="100%">
														<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
															<tbody><tr>
																<td height="30">
																</td>
															</tr>
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tbody><tr>
																			<td class="black_text" data-editable="" style="font-family: Montserrat, sans-serif;font-size: 20px; color: #3b3f40; line-height: 22px; text-align:center; font-weight:700; text-transform:uppercase; " align="center">
																			    Deals of the day
																			</td>
																			<td width="20">
																			</td>
																			<td class="blue_text" style="color:#08a9fa;">
																				|
																			</td>
																			<td width="20">
																			</td>
																			<td class="grey_text" data-editable="" style="font-family: Montserrat, sans-serif; font-size:12px; line-height:12px; color:#88898a; text-transform:capitalize;">
																				 Books for all your reading interests!
																			</td>
																		</tr>
																	</tbody></table>
																</td>
															</tr>
															<tr>
																<td height="30">
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
											</tbody></table>
										</td>
										<td class="padding" width="60">
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
			<!-- product 1-->
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody>';
				$query = "SELECT p.isbn,p.title,a.name,v.condition_id,p.mrp,v.selling_price,p.image from product as p,vendor_products as v,author as a where p.id=v.product_id and 
				p.author_id=a.id and v.id in (SELECT vendor_product_id from deals_of_the_day)";
				$row = mysqli_query($con,$query);
				$counter = 2;
				while($result = mysqli_fetch_array($row)){
					$title = $result['title'];
					$author = $result['name'];
					$condition_id = $result['condition_id'];
					if($condition_id == '2'){
						$condition = "Old";
					}else{
						$condition = "New";
					}
					$mrp = $result['mrp'];
					$selling_price = $result['selling_price'];
					$image = "http://www.bookchor.com/images/".$result['image'];
					$isbn = $result['isbn'];
					$link = "http://www.bookchor.com/book/".$isbn."/".str_replace(' ','-',$title);
					if($counter%2 == 0){
						$mail->body .= '<tr>';
					}
					$mail->body .= '<td align="center">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td class="content" style="width: 800px;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<td class="padding" style="width: 20px;">
										</td>
										<td class="content_row" style="width: 760px;" align="left">
											<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
												<tbody><tr>
													<td class="one_half" align="center" valign="middle" width="240">
														<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
															<tbody><tr>
																<td>
																	<a href="'.$link.'" target="_blank" style="text-decoration: none;">
																		<img src="'.$image.'" style="display: block;" alt="" height="180" border="0" width="120">
																	</a>
																</td>
															</tr>
														</tbody></table>
													</td>
													<td class="nomobile" width="20">
													</td>
													<td class="one_half" valign="top" width="500">
														<table border="0" cellpadding="0" cellspacing="0">
															<tbody>
															<tr>
																<td class="grey_text" data-editable="" style="font-size:14px; color:#9e9e9e; line-height:22px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">
																	<a href="'.$link.'">'.$title.'</a>
																</td>
															</tr>
															<tr>
																<td height="5">
																</td>
															</tr>
															<tr>
																<td class="grey_text" data-editable="">
																	<a href="#" style="text-decoration:none; font-size:14px; color:#9e9e9e; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">'.$author.'</a>
																</td>
															</tr>
															<tr>
																<td height="5">
																</td>
															</tr>
															<tr>
																<td class="grey_text" data-editable="" style="font-size:14px; color:#9e9e9e; line-height:22px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">
																	Condition: <span style="font-size:14px; color:#59c4bc; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">'.$condition.'</span>
																</td>
															</tr>
															<tr>
																<td height="10">
																</td>
															</tr>
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0">
																		<tbody>
																		<tr>
																		
																			<td class="grey_text" data-editable="" style="font-size:12px; color:#9e9e9e; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">
																				MRP: <span style="font-size:12px; color:#59c4bc; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">Rs '.$mrp.'.00</span>
																			</td>
																		</tr>
																		<tr>
																			<td colspan="3" height="5">
																			</td>
																		</tr>
																		<tr>
																		
																			<td class="grey_text" data-editable="" style="font-size:12px; color:#9e9e9e; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">
																				Price: <span style="font-size:12px; color:#59c4bc; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">Rs '.$selling_price.'.00</span>
																			</td>
																		</tr>
																		<tr>
																			<td colspan="3" height="5">
																			</td>
																		</tr>
																		<tr>
																		
																			<td class="grey_text" data-editable="" style="font-size:12px; color:#9e9e9e; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">
																				You Save: <span style="font-size:12px; color:#bf360c; line-height:14px; font-weight:400; font-family: Montserrat, sans-serif; text-align:left;">Rs '.($mrp-$selling_price).'.00</span>
																			</td>
																		</tr>
																		
																	</tbody></table>
																</td>
															</tr>
															<tr>
																<td height="15">
																</td>
															</tr>
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0">
																		<tbody><tr>
																			
																			<td>
																				<table border="0" cellpadding="0" cellspacing="0">
																					<tbody><tr>
																						<td class="button_bg_primary" style=" border-radius: 14px;" align="center" bgcolor="#08a9fa">
																							<table border="0" cellpadding="0" cellspacing="0">
																								<tbody><tr>
																									<td data-editable="" class="button blue_button" style="font-family: Montserrat, sans-serif;font-size: 12px; line-height: 30px; color: #ffffff; text-align:center;" height="30" align="center" width="85">
																										<a class="white_text" href="'.$link.'" target="_blank" style="text-decoration: none; color: #ffffff; width:100%; display: block; line-height: 30px;">
																											Buy Now
																										</a>
																									</td>
																								</tr>
																							</tbody></table>
																						</td>
																					</tr>
																				</tbody></table>
																			</td>
																		</tr>
																	</tbody></table>
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
											</tbody></table>
										</td>
										<td class="padding" style="width: 20px;">
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</tbody></table>
				</td>';
					if($counter%2 != 0){
						$mail->body .= '
						</tr>
						</tbody>
						</table>
						<!-- product 1 end-->
						<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody><tr>
								<td height="20" align="center" width="100%">
								</td>
							</tr>
						</tbody></table>
						<!-- product 2-->
						<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>';
					}
					$counter++;
				}
		$mail->body .='		
		</tbody>
		</table>
		<!-- product 1 end-->
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td height="20" align="center" width="100%">
				</td>
			</tr>
		</tbody></table>
	
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td height="60" align="center" width="100%">
				</td>
			</tr>
		</tbody></table>	
		<table class="wrapper_table" style="width: 800px; max-width: 800px;" align="center" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td "center"="" class="grey_bg" bgcolor="#3b3f40">
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td class="content" style="width: 800px;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody><tr>
										<td class="padding" style="width: 30px;">
										</td>
										<td class="content_row" style="width: 740px;" align="center">
											<table class="mobile_centered_table" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
												<tbody><tr>
													<td height="25">
													</td>
												</tr>
												<tr>
													<td class="content_row" style="width: 740px;" align="center" valign="top">
														<table class="mobile_centered_table" border="0" cellpadding="0" cellspacing="0">
															<tbody><tr>
																<td align="center">
																	<a href="#" target="_blank" style="text-decoration: none;">
																		<img src="http://www.bookchor.com/database/mailer/book-chor-logo-white.png" style="display:block;" alt="" height="13" border="0" width="74">						 </a>
																</td>
															</tr>
															<tr>
																<td height="15">
																</td>
															</tr>
															<tr>
																<td class="white_text" data-editable="" style="font-family: Montserrat, sans-serif; font-size:12px; line-height:48px; text-align:center; color:#ffffff; text-transform: capitalize; text-align:center; font-weight:400;" align="center">
																	Copyright 2017 BookChor Literary Solutions Private Limited
																</td>
															</tr>
															<tr>
																<td height="15">
																</td>
															</tr>
															<tr>
																<td align="center">
																	<table border="0" cellpadding="0" cellspacing="0">
																		<tbody><tr>
																			<td>
																				<a href="http://www.facebook.com/bookchor" target="_blank" style="text-decoration:none;">
																					<img src="http://www.bookchor.com/database/mailer/fb_w.png" style="display:block;" alt="" height="15" border="0" width="8">
																				</a>
																			</td>
																			<td width="25">
																			</td>
																			<td>
																				<a href="http://www.twitter.com/bookchor" target="_blank" style="text-decoration:none">	
																					<img src="http://www.bookchor.com/database/mailer/tw_w.png" style="display:block;" alt="" height="12" border="0" width="15">
																				</a>	
																			</td>
																			<td width="25">
																			</td>
																			<td>
																				<a href="http://www.instagram.com/book.chor" target="_blank" style="text-decoration:none;">
																					<img src="http://www.bookchor.com/database/mailer/inst_w.png" style="display:block;" alt="" height="15" border="0" width="15">
																				</a>						
																			</td>
																		</tr>
																	</tbody></table>
																</td>
															</tr>
															<tr>
																<td height="15">
																</td>
															</tr>
															<tr>
																<td class="white_text" data-editable="" style="font-family: Montserrat, sans-serif; font-size:12px; line-height:48px; font-weight:400; text-align:center;">
																	<a id="unsubscribe" href="#" target="_blank" style="text-decoration:none; color:#e2e2e2;"> 
																		<center>Unsubscribe</center>
																	</a>
																</td>
															</tr>
															<tr>
																<td height="15">
																</td>
															</tr>
															
														</tbody></table>
													</td>
												</tr>
											</tbody></table>
										</td>
										<td class="padding" style="width: 30px;">
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>		
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
	
		</body>
</html>';
			$mail->AltBody = 'Welcome to BookChor';
			$mail->addAddress($customer_email);
			echo $mail->send();
		}
	}
?>