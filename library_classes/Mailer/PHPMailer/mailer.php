<?php
include('mysql.php');
require 'PHPMailerAutoload.php';
$mail = new PHPMailer;
$mail->Host = 'mail.bookchormail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Username = ""; // SMTP username
$mail->Password = "";
$mail->Port = 25;                                    // TCP port to connect to
$mail->setFrom('info@bookchormail.com', 'BookChor');     // Add a recipient               // Name is optional
$mail->addReplyTo('info@bookchormail.com', 'Reply to:');
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Super Sunday at BookChor!';
$query = "SELECT id,name,email from customer";
$row = mysqli_query($con,$query);
$counter = 0;
while($result = mysqli_fetch_array($row)){
	$customer_name = $result['name'];
	$customer_id = $result['id'];
	$customer_email = $result['email'];
	$mail->Body ='
		<html>
		Hey '.$customer_name.',<br><br>
		BookChor Sunday Market is now live!! Grab them all!!<br><br>	
		<img src="http://www.bookchor.com/phpmailer/mail_data.php?customer_id='.$customer_id.'&mail_id=10" style="display: none;"/>
		<center>
		<a href="http://www.bookchor.com/category/7/58-Store"><img src="http://www.bookchor.com/assets/frontend/pages/img/layerslider/mail_banner.jpg" width="600" height="900"/></a>
		</center>
		<table align="center" class="table-inner" width="290" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="15"></td>
			</tr>
			<tr>
				<td class="text-link" align="center">

					<!-- left -->
					<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
								<a href="http://www.bookchor.com/Contact">Need Help?</a>
							</td>
						</tr>
					</table>
					<!-- end left -->

					<!--Space-->

					<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
						<tr>
							<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
								<p style="padding-left: 24px;">&nbsp;</p>
							</td>
						</tr>
					</table>

					<!--End Space-->

					<!-- middle -->
					<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
								<a href="http://www.bookchor.com/About">About us</a>
							</td>
						</tr>
					</table>
					<!-- end middle -->

					<!--Space-->

					<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
						<tr>
							<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
								<p style="padding-left: 24px;">&nbsp;</p>
							</td>
						</tr>
					</table>

					<!--End Space-->

					<!-- right -->
					<table class="table3-3" width="80" border="0" align="right" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
								<a href="http://www.bookchor.com/unsubscribe?csid='.md5('$customer_id').'" target="_BLANK">Don\'t need it?</a>
							</td>
						</tr>
					</table>
					<!-- end right -->

				</td>
			</tr>
			<tr>
				<td height="15"></td>
			</tr>
		</table>
	</html>
	';
	$mail->AltBody = 'BookChor Sunday Market is now live!!';
	$mail->addAddress($customer_email);
	if($counter==1000){
		$counter = 0;
		sleep(15);
	}
	if(!$mail->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo."<br>";
	} else {
		echo 'Message has been sent to '.$customer_email;
	}
	$mail->clearAllRecipients(); 
	$counter++;
}
?>