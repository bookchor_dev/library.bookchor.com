<?php
include('mysql.php');
require 'PHPMailerAutoload.php';
$mail = new PHPMailer;
$mail->Host = 'mail.bookchormail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Username = ""; // SMTP username
$mail->Password = "";
$mail->Port = 25;                                    // TCP port to connect to
$mail->setFrom('info@bookchormail.com', 'BookChor');     // Add a recipient               // Name is optional
$mail->addReplyTo('info@bookchormail.com', 'Reply to:');
$mail->isHTML(true);                                  // Set email format to HTML
$query = "SELECT c.id,c.name,c.email from customer as c,cart where c.id=cart.customer_id and cart.status=1 and c.id not in 
(SELECT DISTINCT(customer_id) from orders where order_status_id=1 or order_status_id=2 or order_status_id=4 or order_status_id=5) 
and c.id>20000 and c.id<=50000 group by c.id order by c.id desc";
//$query = "SELECT id,name,email from customer where id=138 or id=22 or id=2";
$row = mysqli_query($con,$query);
while($result = mysqli_fetch_array($row)){
	$customer_name = $result['name'];
	$customer_id = $result['id'];
	$customer_email = $result['email'];
	$mail->Subject = $customer_name.', BookChor misses you!';
	$mail->Body ='
		<html>
		Hey '.$customer_name.',<br><br>	
		<img src="http://www.bookchor.com/phpmailer/mail_data.php?customer_id='.$customer_id.'&mail_id=3" style="display: none;"/>
			We dont really do this often. However there is no shame in saying we miss you. Its the feeling of packing your books and writing a 
			beautiful note for you and waiting for you to open it and smile.<br> 
			Although you have been apart for the past few months or days, but it feels like ages.<br>
			And, a lot has changed since then. <br>
			For starters,  you can choose amongst the deals we put up everyday, also you can get yourself a blind date with a book.
			Its only the tip of the iceberg We hope to see you soon. <br>
			Just for you! Use coupon code: <b>EXTRA7</b> to get extra 7% discount on everything. Offer valid only till 12th April, 2017.
			<center><br><a href="http://www.bookchor.com">Visit us!</a><br><br></center>
		<table align="center" class="table-inner" width="290" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="15"></td>
			</tr>
			<tr>
				<td class="text-link" align="center">

					<!-- left -->
					<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
								<a href="http://www.bookchor.com/Contact">Need Help?</a>
							</td>
						</tr>
					</table>
					<!-- end left -->

					<!--Space-->

					<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
						<tr>
							<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
								<p style="padding-left: 24px;">&nbsp;</p>
							</td>
						</tr>
					</table>

					<!--End Space-->

					<!-- middle -->
					<table class="table3-3" width="80" border="0" align="left" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
								<a href="http://www.bookchor.com/About">About us</a>
							</td>
						</tr>
					</table>
					<!-- end middle -->

					<!--Space-->

					<table width="1" height="15" border="0" cellpadding="0" cellspacing="0" align="left">
						<tr>
							<td height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">
								<p style="padding-left: 24px;">&nbsp;</p>
							</td>
						</tr>
					</table>

					<!--End Space-->

					<!-- right -->
					<table class="table3-3" width="80" border="0" align="right" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;">
								<a href="http://www.bookchor.com/unsubscribe?csid='.md5('$customer_id').'" target="_BLANK">Don\'t need it?</a>
							</td>
						</tr>
					</table>
					<!-- end right -->

				</td>
			</tr>
			<tr>
				<td height="15"></td>
			</tr>
		</table>
	</html>
	';
	$mail->AltBody = 'BookChor Misses you!!';
	$mail->addAddress($customer_email);
	//$mail->addBCC($customer_email);
	if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent to '.$customer_email.'<br>';
    }
	$mail->clearAllRecipients(); 
}
?>