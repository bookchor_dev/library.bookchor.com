<?php
		class Login{
			public function __construct(){
				$con = new Connections();
				$this->mongo_bookchor_library = $con->mongo_bookchor_library();
			}
			public function login($credentials,$token,$auth_flag=0){
				if($auth_flag==1){
					$url = 'https://library.bookchor.com/api_login?auth_key='.$credentials;
					$dec_credentials = file_get_contents($url);
					$credentials = (array) json_decode($dec_credentials);
				}
				$login_data = $this->mongo_bookchor_library->users->find(['username'=>$credentials['username'],'password'=>$credentials['password'],'status'=>1],["limit"=>1,'projection'=>['name'=>1,'user_type_id'=>1,'library_id'=>1,'category'=>1,'user_category'=>1]]);
				$user_data_raw = iterator_to_array($login_data);
				if($user_data_raw[0]){
					$user_data = $user_data_raw[0];
					$library_id = $user_data['library_id'];
					$library_query = $this->mongo_bookchor_library->library->find(['_id'=>$library_id],['projection'=>['logo'=>1,'issue_flag'=>1,'solr_library_id'=>1,'name'=>1,'module_access'=>1]]);
					$library_data = iterator_to_array($library_query)[0];
					$user_data['logo'] = $library_data['logo'];
					if($user_data['user_type_id']==2){
						$user_data['module_access'] = $library_data['module_access']['admin'];
					}
					$user_data['logged_in'] = 1;
					$user_data['school'] = $library_data['name'];
					$user_data['bc_db_access'] = isset($library_data['bc_db_access'])?$library_data['bc_db_access']:0;
					$user_data['issue_flag'] = isset($library_data['issue_flag'])?$library_data['issue_flag']:1;
					$user_data['user_id'] = $user_data['_id'];
					unset($user_data['_id']);
					if((string)$library_id=='5c8207cbf3818e22ec186940'){
						$user_data['library_id'] = new MongoDB\BSON\ObjectId('5af409d76577193010d3f0c9');
					}
					if($this->updateSession($user_data,$token)){
						if($user_data['user_type_id']==2){
							header("Location: /admin");
						}else{
							if($user_data['user_category']){
								header("Location: /");
							}else{
								header("Location: /?recommend=1");
							}
							
						}
					}
				}else{
					return 0;
				}
			}
			public function login_biometric($user_id,$token){
				$login_data = $this->mongo_bookchor_library->users->find(['uid'=>$user_id],["limit"=>1,'projection'=>['name'=>1,'user_type_id'=>1,'library_id'=>1]]);
				$user_data = iterator_to_array($login_data)[0];
				if($user_data){
					$library_id = $user_data['library_id'];
					$library_query = $this->mongo_bookchor_library->library->find(['_id'=>$library_id],['projection'=>['logo'=>1,'issue_flag'=>1,'solr_library_id'=>1,'fp_flag'=>1]]);
					$library_data = iterator_to_array($library_query)[0];
					if(isset($library_data['fp_flag'])&&$library_data['fp_flag']==1){
						$user_data['logo'] = $library_data['logo'];
						$user_data['logged_in'] = 1;
						$user_data['solr_library_id'] = $library_data['solr_library_id'];
						$user_data['issue_flag'] = isset($library_data['issue_flag'])?$library_data['issue_flag']:1;
						$user_data['user_id'] = $user_data['_id'];
						unset($user_data['_id']);
						if($this->updateSession($user_data,$token)){
							if($user_data['user_type_id']==2){
								return '/admin';
							}else{
								return '/';
							}
						}
					}else{
						return '/login';
					}
				}
			}
			public function get_fingerprint_hash($username){
				$query_hash = $this->mongo_bookchor_library->users->find(['$or'=>[['uid'=>$username],['username'=>$username]]],['limit'=>1,'projection'=>['hash'=>1]]);
				$user_data = iterator_to_array($query_hash);
				if(isset($user_data[0]['hash'])){
					return $user_data[0]['hash'];
				}else{
					return 0;
				}
			}
			private function updateSession($user_data,$token=0){
				$update_session = $this->mongo_bookchor_library->sessions->UpdateOne(['token'=>$token],['$set'=>$user_data]);
				if($update_session->getModifiedCount()){
					return true;
				}else{
					return false;
				}
			}
			public function logout($token){
				$logout_data = ['user_id'=>0,'user_name'=>'','logged_in'=>0,'user_type_id'=>0];
				if($this->updateSession($logout_data,$token)){
						header("Location: /login"); 
				}
			}
		}
?>