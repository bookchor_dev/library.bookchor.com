<?php
	class User{
		public $user;
		public $cache_version;
		public $token;
		public function __construct(){
			$con = new Connections();
			$this->mongo_bookchor_library = $con->mongo_bookchor_library();
			$this->timestamp = (new DateTime())->getTimestamp();
			$this->cache_version = 1;
			if($this->getSession()){
				if($_SERVER['PHP_SELF']=='/login.php'){
					header("Location: /admin");
				}
			}else{
				if($_SERVER['PHP_SELF']!='/login.php'){
					header("Location: /login");
				}
			}
		}
		public function getSession(){
			$notoken = 0;
			$user_logged = 0;
			if(isset($_COOKIE['bluid'])){
				if($_COOKIE['bluid']){
					$result = $this->mongo_bookchor_library->sessions->find(array("token"=>$_COOKIE['bluid'],"status" => 1),array("limit"=>1));
					$user_result = iterator_to_array($result);
					if(isset($user_result)&&$user_result){
						$user = $user_result[0];
						$this->token = $_COOKIE['bluid'];
						if(isset($user['logged_in'])){
							$user_logged = $user['logged_in'];
							if($user_logged==1){
								/*Remove this*/
								setcookie("bluid",$this->token,time()+300000,"/");
								setcookie("blln",'1',time()+300000,"/");
								/*Queryfor inactivity and activity update query*/
								$this->user = (array) $user;
								$this->user['cache'] = 4;
								if($user['user_type_id']==2){
									if(explode('/',$_SERVER['SCRIPT_NAME'])[1]!='admin'){
										header("Location: /admin");
									}
									}else{
									if(explode('/',$_SERVER['SCRIPT_NAME'])[1]=='admin'){
										header("Location: /");
									}
								}
							}
						}
						}else{
						$notoken = 1;
					}
					}else{
					$notoken = 1;
				}
				}else{
				$notoken = 1;
			}
			if($notoken==1){
				$utility = new Utility();
				$device = $utility->showDeviceInfo('all');
				$ip_address = $_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'];
				$this->token = sha1($ip_address.''.$this->timestamp);
				$token_create = $this->mongo_bookchor_library->sessions->InsertOne([
				'token' => $this->token,
				'status' => 1,
				'device' => $device,
				'date_added' => $this->timestamp,
				]);
				setcookie("blip",sha1($ip_address),time()+300000000,"/");
				setcookie("bluid",$this->token,time()+30000,"/");
				setcookie("blln",'0',time()+30000,"/");
			}
			//$user_log = $this->user_activity();
			return $user_logged;
		}
		public function user_activity($insert_activity=0){
			if($this->activity_id){
				if(sha1($_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT']) != $_COOKIE['blip']){
					$insert_activity = 1;
				}
				}else{
				$insert_activity = 1;
			}
			if($insert_activity!=0){
				$json_data = Utility::get_ip_details($_SERVER['REMOTE_ADDR']);
				if($this->user_id){
					$logout = [];
					if($insert_activity==2){
						$activity_data = array(
						'$set' => array(
						'end_timestamp' => $this->timestamp
						),
						'$setOnInsert' => array(
						'user_id' => $this->user_id,
						'token_id' => $this->token,
						'ip_details' => $json_data,
						'start_timestamp' => $this->timestamp
						),
						);
						}else{
						$activity_data = array(
						'$setOnInsert' => array(
						'user_id' => $this->user_id,
						'token_id' => $this->token,
						'ip_details' => $json_data,
						'start_timestamp' => $this->timestamp
						),
						);
					}
					
					}else{
					$activity_data = array(
					'$setOnInsert' => array(
					'token_id' => $this->token,
					'ip_details' => $json_data,
					'start_timestamp' => $this->timestamp
					),
					);
				}
				//print_r($activity_data);die;
				$result_insert_activity = $this->mongo_bookchor_library->user_activity->UpdateOne(['_id'=> new MongoDB\BSON\ObjectId($this->activity_id)],$activity_data,['upsert'=>true]);
				$this->activity_id = $result_insert_activity->getUpsertedId();
				$this->mongo_bookchor_library->sessions->UpdateOne(['token'=> $this->token],['$set'=>['activity_id'=>$this->activity_id]]);
			}
		}
		public function user_info($user_id){
			$user_info_query = $this->mongo_bookchor_library->users->find(['_id'=>$user_id]); 
			$user_info =  iterator_to_array($user_info_query)[0];
			return $user_info;
		}
		public function update_users($user_data){
			if(isset($user_data['old_password'])&&isset($user_data['password'])){
				$filter = ['_id'=>$this->user_id,'password'=>$user_data['old_password']];
				$new_data = [
				'password'=> $user_data['password']
				];
				}else{
				$filter = ['_id'=>$this->user_id];
				$new_data = [
				'name'=> $user_data['name'],
				'email'=>  $user_data['email'],
				'phone'=>  $user_data['phone']
				];
			}
			$update_user_query = $this->mongo_bookchor_library->users->updateOne($filter,['$set'=>$new_data]);
			if($update_user_query->getModifiedCount()){
				return 1;
				}else{
				return 0;
			}
		}
		public function update_user_details($user_data,$admin_flag=0){
			/* [
				'name'=>$user_data['name'],
				'user_type_id'=>$user_data['user_type_id'],
				'email'=>$user_data['email'],
				'class'=>$user_data['class'],
				'gender'=>$user_data['gender'],
				'dob'=>$user_data['dob'],
				'contact'=>$user_data['contact'],
				'address'=>$user_data['address'],
				'father_name'=>$user_data['father_name'],
				'mother_name'=>$user_data['mother_name'],
				'fp_hash'=>$user_data['fp_hash'],
			] */
			if($admin_flag === 0){
				if(isset($user_data['name'])&&isset($user_data['class'])&&isset($user_data['email'])&&isset($user_data['gender'])&&isset($user_data['dob']))
				{
					$result_update = $this->mongo_bookchor_library->users->updateOne(
					['_id'=> $this->user_id],
					['$set' => [
					'name'=>$user_data['name'],
					'email'=>$user_data['email'],
					'class'=>$user_data['class'],
					'gender'=>$user_data['gender'],
					'dob'=>$user_data['dob']
					]
					]
					);
					if($result_update->getModifiedCount()){
						return 1;
						}else{
						return 0;
					}
				}
			}else if($admin_flag === 1){
				$username = explode(' ',$user_data['name'])[0].'_'.$user_data['uid'];
				$password = explode('-',(string)$user_data['dob']);
				if(isset($user_data['name'])&&isset($user_data['gender'])&&isset($user_data['dob'])&&isset($user_data['contact'])&&isset($user_data['address'])&&isset($user_data['user_type_id'])&&isset($user_data['uid'])){
					$result_update = $this->mongo_bookchor_library->users->updateOne(['uid'=> $user_data['uid'],'library_id'=>$this->user['library_id']],['$set' => $user_data,'$setOnInsert'=>['username'=>$username,'date_added'=>$this->timestamp,'password'=>$password,'status'=>1]],['upsert'=>true]);
					if($result_update->getModifiedCount()){
						return 1;
					}else if($result_update->getUpsertedCount()){
						$username = $username;
						$password = $password;
						$phone  = $user_data['contact'][0];
						$apikey = "fPKIQEAuh0q6D3JeZBTENw";
						$apisender = "BKCHOR";
						$sms_message = "Greetings from BookChor.com!We're glad to annouce that ".$this->user['school']." Library is now active.To remotely access your account and ebooks, please follow the link and use the credentials given below:https://library.bookchor.com/login Username: ".$username." Password: ".$password." Happy Reading!";
						$sms_message = rawurlencode($sms_message);
						$url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey='.$apikey.'&senderid='.$apisender.'&channel=2&DCS=0&flashsms=0&number='.$phone.'&text='.$sms_message.'&route=1';
						$ch=curl_init($url);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch,CURLOPT_POST,1);
						curl_setopt($ch,CURLOPT_POSTFIELDS,"");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,2);
						$data = curl_exec($ch);
						return 1;
					}else{
						return 0;
					}
				}
			}
			
		}
	}
?>