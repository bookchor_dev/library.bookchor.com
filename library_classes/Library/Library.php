<?php 
	class Library{
		public function __construct(){
			$con = new Connections();
			$this->mongo_bookchor_library = $con->mongo_bookchor_library();
			$this->timestamp = (new DateTime())->getTimestamp();//strtotime(date('Y-m-d H:i:s'));
		}
		public function return_book($barcode,$library_id,$fine=[]){
			$issue_query = $this->mongo_bookchor_library->books->updateOne(array('stock' => array('$elemMatch' => array('barcode' => $barcode))),array('$set' =>array('stock.$.out' => 0)));
			if($issue_query->getModifiedCount()){
				if($fine){
					$this->mongo_bookchor_library->users->updateOne(array('library_id'=>$library_id,'issue' => array('$elemMatch' => array('barcode' => (string) $barcode,'status'=>1))),array('$set' =>array('issue.$.status' => 2,'issue.$.date_returned'=>$this->timestamp,'issue.$.fine' => $fine)));
				}else{
					$this->mongo_bookchor_library->users->updateOne(array('library_id'=>$library_id,'issue' => array('$elemMatch' => array('barcode' => (string) $barcode,'status'=>1))),array('$set' =>array('issue.$.status' => 2,'issue.$.date_returned'=>$this->timestamp)));
				}
			}
		}
		public function issue_book($issue_info,$library_id){
			$user_id = $issue_info['user_id'];
			$barcode = $issue_info['barcode'];
			$issue_query = $this->mongo_bookchor_library->books->updateOne(array('library_id'=> $library_id,'stock' => array('$elemMatch' => array('barcode' => $barcode))),array('$set' =>array('stock.$.out' => 1)));
			if($issue_query->getModifiedCount()){
				$pid_query = $this->mongo_bookchor_library->books->find(['library_id'=> $library_id,'stock.barcode'=>$barcode],['projection'=>['product_id'=>1],'limit'=>1]);
				$pid_out = iterator_to_array($pid_query)[0];
				$pid = $pid_out['product_id'];
				$this->mongo_bookchor_library->users->updateOne(array('_id' => new MongoDB\BSON\ObjectId($user_id)),array('$addToSet' =>['issue'=>['_id'=> new MongoDB\BSON\ObjectId(),'product_id'=>$pid,'barcode'=>$barcode,'date_issued'=>$this->timestamp,'status'=>1]]));
			}
		}
		public function search_out_book($search,$library_id,$search_type){
			$search_products = [];
			if($search_type==1){
				/* if((is_numeric($search)&&strlen((string)$search)>7)||(preg_match('/[A-Za-z].*[a-zA-Z0-9._-]|[0-9].*[a-zA-Z0-9._-]/', $search))){ */
					/*ISBN-BARCODE-SKU*/
					$options = [['isbn'=>$search],['stock.barcode'=>$search],['stock.sku'=>$search]];	
					$query = ['library_id'=>$library_id,'$or'=>$options,'stock.status'=>1];	
					$search_product_query = $this->mongo_bookchor_library->books->aggregate([['$match'=>$query],['$unwind'=>'$stock'],['$match'=>$query],['$lookup'=>['from'=>'users','let'=>['barcode'=>'$stock.barcode','sku'=>'$stock.sku'],'pipeline'=>[['$project'=>['name'=>1,'uid'=>1,'issue'=>1,'library_id'=>1]],['$match'=>['library_id'=>$library_id,'issue.status'=>1]],['$unwind'=>'$issue'],['$match'=>['issue.status'=>1,'$expr'=>['$eq'=>['$issue.barcode','$$barcode']]]],['$project'=>['name'=>1,'uid'=>1,'_id'=>0,'issue'=>1]],],'as'=>'user']],['$unwind'=>['path'=>'$user','preserveNullAndEmptyArrays'=>true]],['$lookup'=>['from'=>'products','let'=>['pid'=>'$product_id'],'pipeline'=>[['$match'=>['$expr'=>['$eq'=>['$id','$$pid']]]]],'as'=>'product']],['$unwind'=>'$product'],['$project'=>['user'=>1,'product'=>1,'stock'=>'$stock']]]);
			}else if($search_type==3){
					$search = new MongoDB\BSON\Regex($search);
					$regex_search = ['$regex'=>$search,'$options'=>'i'];
					$options = [['title'=>$regex_search],['author'=>$regex_search]];	
					$query = ['$or'=>$options];
					//$query = ['library_id'=>$library_id,'$or'=>$options,'stock.status'=>1];	
					$search_product_query = $this->mongo_bookchor_library->products->aggregate([['$match'=>$query],['$project'=>['product'=>['id'=>'$id','author'=>'$author','description'=>'$description','isbn'=>'$isbn','image'=>'$image','mrp'=>'$mrp','title'=>'$title']]],['$lookup'=>['from'=>'books','let'=>['pid'=>'$product.id'],'pipeline'=>[['$match'=>['library_id'=>$library_id,'$expr'=>['$eq'=>['$product_id','$$pid']]]],['$unwind'=>'$stock'],['$replaceRoot'=>['newRoot'=>'$stock']]],'as'=>'stock']],['$unwind'=>'$stock'],['$lookup'=>['from'=>'users','let'=>['pid'=>'$product.id'],'pipeline'=>[['$project'=>['name'=>1,'uid'=>1,'issue'=>1,'library_id'=>1]],['$match'=>['library_id'=>$library_id,'issue.status'=>1]],['$unwind'=>'$issue'],['$match'=>['issue.status'=>1,'$expr'=>['$eq'=>['$issue.product_id','$$pid']]]],['$project'=>['name'=>1,'uid'=>1,'_id'=>0]],],'as'=>'user']],['$unwind'=>['path'=>'$user','preserveNullAndEmptyArrays'=>true]]]);
			}else if($search_type==2){
				/*ID & EMAIL*/
				$regex_search = new MongoDB\BSON\Regex($search);
				$options = [['uid'=>['$regex'=>$search,'$options'=>'i']],['email'=> ['$regex'=>$regex_search,'$options'=>'i']],['username'=> ['$regex'=>$regex_search,'$options'=>'i']]];	
				$search_product_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'$or'=>$options,'issue.status'=>1]],['$project'=>['name'=>1,'uid'=>1,'issue'=>1]],['$unwind'=>'$issue'],['$match'=>['issue.status'=>1]],['$lookup'=>['from'=>'books','let'=>['product_id'=>'$issue.product_id','barcode'=>'$issue.barcode'],'pipeline'=>[['$match'=>['library_id'=>$library_id,'$expr'=>['$eq'=>['$product_id','$$product_id']]]],['$unwind'=>'$stock'],['$match'=>['$expr'=>['$eq'=>['$stock.barcode','$$barcode']]]],['$replaceRoot'=>['newRoot'=>'$stock']]],'as'=>'stock']],['$unwind'=>'$stock'],['$lookup'=>['from'=>'products','let'=>['pid'=>'$issue.product_id'],'pipeline'=>[['$match'=>['$expr'=>['$eq'=>['$id','$$pid']]]]],'as'=>'product']],['$unwind'=>['path'=>'$product','preserveNullAndEmptyArrays'=>true]],['$project'=>['user'=>['name'=>'$name','uid'=>'$uid','issue'=>'$issue'],'product'=>1,'stock'=>1,'_id'=>0]]]);
			}
			$search_products = iterator_to_array($search_product_query);
			//print_r($search_products);die;
			return $search_products;
		}
		public function books_in($sku,$isbn,$library_id,$condition){
			$bar_query =  $this->mongo_bookchor_library->tempbarcode->find(['library_id'=>$library_id],['sort'=>['barcode'=>1],'limit'=>1]);
			$bar_missed = iterator_to_array($bar_query);
			if(isset($bar_missed[0])){
				$pbarcode = (int) $bar_missed[0]['barcode'];
				$query_delete = $this->mongo_bookchor_library->tempbarcode->deleteOne(['barcode'=>sprintf("%08d",$pbarcode),'library_id'=>$library_id]);
				if($query_delete->getDeletedCount()){
					$barcode = (int) $bar_missed[0]['barcode'];
				}
			}
			if(!isset($barcode)||(!$barcode)){
				$max_bar_query =  $this->mongo_bookchor_library->books->find(['library_id'=>$library_id],['sort'=>['stock.barcode'=>-1],'limit'=>1]);
				$max_bar = iterator_to_array($max_bar_query);
				if(isset($max_bar[0]['stock'])){
					foreach($max_bar[0]['stock'] as $stock){
						$b_arr[] = (int) $stock['barcode'];
					}
					$prev_barcode = max($b_arr);
				}else{
					$prev_barcode = 0;
				}
				$max_bar_query = $this->mongo_bookchor_library->temp_stock->find(['library_id'=>$library_id],['projection'=>['barcode'=>1],'sort'=>['barcode'=>-1],'limit'=>1]);
				$max_bar_temp = iterator_to_array($max_bar_query);
				if(isset($max_bar_temp[0]['barcode'])){
					if($prev_barcode<$max_bar_temp[0]['barcode']){
						$prev_barcode = (int) $max_bar_temp[0]['barcode'];
					}
				}
				$barcode = $prev_barcode+1;
			}
			$temp_books_in_query = $this->mongo_bookchor_library->temp_stock->insertOne(['library_id'=>$library_id,'sku'=>$sku,'isbn'=>$isbn,'barcode'=>sprintf("%08d",$barcode),'condition'=>$condition,'print'=>0,'date_added'=>$this->timestamp]);
			
			return $barcode;
		}
		public function books_temp_delete($barcode,$library_id){
			$temp_delete_query = $this->mongo_bookchor_library->temp_stock->deleteOne(['library_id'=> $library_id,'barcode' => $barcode]);
			if($temp_delete_query->getDeletedCount()){
				$temp_insert_query = $this->mongo_bookchor_library->tempbarcode->insertOne(['library_id'=> $library_id,'barcode' => $barcode]);		
				return 1;
			}else{
				return 0;
			}
		}
		public function books_out($barcode,$library_id){
			$issue_query = $this->mongo_bookchor_library->books->updateOne(array('library_id'=> $library_id,'stock' => array('$elemMatch' => array('barcode' => $barcode,'status'=>1))),array('$set' =>array('stock.$.status' => 0,'stock.$.date_out' => $this->timestamp)));
			if($issue_query->getModifiedCount()){
				return 1;
			}else{
				return 0;
			}
		}
		public function barcode_print($library_id,$status=0){
			$unprinted_temp_query = $this->mongo_bookchor_library->temp_stock->find(['library_id'=>$library_id,'print'=>$status]);
			$book_unprinted_temp = (array) iterator_to_array($unprinted_temp_query);
			return $book_unprinted_temp;
		}
		public function barcode_reprint($library_id,$count){
			$book_unprinted = [];
			$unprinted_temp_query = $this->mongo_bookchor_library->temp_stock->find(['library_id'=>$library_id],['sort'=>['barcode'=>-1],'limit'=>$count]);
			$book_unprinted = (array) iterator_to_array($unprinted_temp_query);	
			return $book_unprinted;
		}
		public function print_flag($barcode,$library_id){
			$print_flag_query_temp  = $this->mongo_bookchor_library->temp_stock->updateOne(['library_id'=>$library_id,'barcode'=>$barcode],['$set' =>['print'=>1]]);
		}
		public function issue_logs($library_id){
			$issue_logs = [];
			$issue_log_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'status'=>1]],['$project'=>['issue'=>1,'name'=>1,'uid'=>1]],['$unwind'=>'$issue'],['$lookup'=>['from'=>'products','localField'=>'issue.product_id','foreignField'=>'id','as'=>'product']],['$unwind'=>'$product'],['$lookup'=>['from'=>'books','let'=>['barcode'=>'$issue.barcode'],'pipeline'=>[['$match'=>['library_id'=>$library_id,'stock.status'=>1]],['$unwind'=>'$stock'],['$match'=>['stock.status'=>1,'$expr'=>['$eq'=>['$stock.barcode','$$barcode']]]]],'as'=>'stock']],['$unwind'=>'$stock'],['$project'=>['name'=>1,'uid'=>1,'title'=>'$product.title','author'=>'$product.author','isbn'=>'$product.isbn','sku'=>'$stock.stock.sku','barcode'=>'$stock.stock.barcode','date_issued'=>'$issue.date_issued','date_returned'=>'$issue.date_returned','status'=>'$issue.status']]]);
			$issue_logs = (array) iterator_to_array($issue_log_query);
			//print_r($issue_logs);die;
			return $issue_logs;
		}
		public function defaulters($library_id){
			$query = ['issue.fine' => ['$exists' => 'true','$not'=>['$size'=>0]]];
			$default_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'status'=>1,'issue.fine' => ['$exists' => 'true','$not'=>['$size'=>0]]]],['$project'=>['issue'=>1,'name'=>1,'uid'=>1]],['$unwind'=>'$issue'],['$match'=>$query],['$lookup'=>['from'=>'products','localField'=>'issue.product_id','foreignField'=>'id','as'=>'product']],['$unwind'=>'$product'],['$lookup'=>['from'=>'books','let'=>['barcode'=>'$issue.barcode'],'pipeline'=>[['$match'=>['library_id'=>$library_id,'stock.status'=>1]],['$unwind'=>'$stock'],['$match'=>['stock.status'=>1,'$expr'=>['$eq'=>['$stock.barcode','$$barcode']]]]],'as'=>'stock']],['$unwind'=>'$stock'],['$project'=>['name'=>1,'uid'=>1,'title'=>'$product.title','author'=>'$product.author','isbn'=>'$product.isbn','sku'=>'$stock.stock.sku','barcode'=>'$stock.stock.barcode','date_issued'=>'$issue.date_issued','status'=>'$issue.status','fine'=>'$issue.fine.amount','date_returned'=>'$issue.date_returned']]]);
			$defaulter_logs = iterator_to_array($default_query);
			return $defaulter_logs;
		}
		public function library_stats($library_id){
			$library_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id]],['$project'=>['issue'=>1]],['$unwind'=>'$issue'],['$replaceRoot'=>['newRoot'=>'$issue']],['$project'=>['date_added'=>['$ceil'=>['$divide'=>['$date_issued',86400]]]]],['$group'=>['_id'=>'$date_added','issue_count'=>['$sum'=>1]]],['$sort'=>['_id'=>-1]],['$limit'=>30]]);
			$library_data['graph'] = iterator_to_array($library_query);
			$books_issued = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'issue'=>['$exists'=>true]]],['$unwind'=>'$issue'],['$count'=>'issued']]);
			$library_data['books_issued'] = iterator_to_array($books_issued)[0]['issued'];
			$books_viewed = $this->mongo_bookchor_library->users->aggregate([['$match'=>['library_id'=>$library_id,'view'=>['$exists'=>true]]],['$unwind'=>'$view'],['$count'=>'views']]);
			$library_data['books_viewed'] = iterator_to_array($books_viewed)[0]['views'];
			/**/$library_data['fine_due'] = 0;
			$library_data['fine_collected'] = 0;
			$total_books = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=>$library_id,'stock.status'=>1]],['$unwind'=>'$stock'],['$match'=>['stock.status'=>1]],['$count'=>'books']]);
			$total_books_temp = $this->mongo_bookchor_library->temp_stock->aggregate([['$match'=>['library_id'=>$library_id]],['$count'=>'books']]);
			$library_data['total_books'] = iterator_to_array($total_books)[0]['books']+iterator_to_array($total_books_temp)[0]['books'];
			$total_titles = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=>$library_id,'stock.status'=>1]],['$count'=>'titles']]);
			//$total_titles_temp = $this->mongo_bookchor_library->temp_stock->aggregate([['$match'=>['library_id'=>$library_id]],['$count'=>'books']]);
			$library_data['total_titles'] = iterator_to_array($total_titles)[0]['titles'];;
			return $library_data;
		}
		public function barcode_history($barcode,$library_id,$doc_id=0,$limit=0){
			if($barcode!=''){
				$query = ['library_id'=>$library_id,'issue.barcode'=>(string) $barcode];
			}else{
				$query = ['library_id'=>$library_id];
			}
			$barcode_query = $this->mongo_bookchor_library->users->aggregate([['$match'=>$query],['$project'=>['library_id'=>1,'name'=>1,'uid'=>1,'issue'=>1]],['$unwind'=>'$issue'],['$match'=>$query],['$sort'=>['issue.date_issued'=>-1]],['$skip'=>(int) $doc_id],['$limit'=>$limit]]);
			$barcode_history = iterator_to_array($barcode_query);
			return $barcode_history;
		}
		public function search_users($search,$library_id){
			if(is_numeric($search)){
				$search = (string)$search;
			}
			$regex_search = new MongoDB\BSON\Regex($search);
			$result_detail = $this->mongo_bookchor_library->users->find(['library_id'=>$library_id,'$or'=>[['name'=>['$regex'=>$regex_search,'$options'=>'i']],['uid'=>$search],['email'=>['$regex'=>$regex_search,'$options'=>'i']]]]);
			$result = iterator_to_array($result_detail);
			return $result;
		}
		public function all_users($library_id,$user_type){
			$user_query = $this->mongo_bookchor_library->users->find(['user_type_id'=>$user_type,'library_id'=>$library_id],['projection'=>['username'=>1,'password'=>1,'uid'=>1,'contact'=>1,'username'=>1,'password'=>1,'_id'=>0]]);
			$result = iterator_to_array($user_query);
			return $result;
		}
		public function all_books_data($library_id,$doc_id,$limit){
			$search_product_query = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=>$library_id,'stock.status'=>1]],['$project'=>['library_id'=>0,'_id'=>0]],['$unwind'=>'$stock'],['$match'=>['stock.status'=>1]],['$lookup'=>['from'=>'products','localField'=>'product_id','foreignField'=>'id','as'=>'products']],['$unwind'=>'$products'],['$skip'=>(int) $doc_id],['$project'=>['product_id'=>'$product_id','sku'=>'$stock.sku','barcode'=>'$stock.barcode','title'=>'$products.title','isbn'=>'$products.isbn','author'=>'$products.author','mrp'=>'$products.mrp','out'=>'$stock.out','condition'=>'$stock.condition']]]);
			$search_products = iterator_to_array($search_product_query);
			//print_r($search_products);die;
			
			$total_books = $this->mongo_bookchor_library->books->aggregate([['$match'=>['library_id'=>$library_id,'stock.status'=>1]],['$unwind'=>'$stock'],['$match'=>['stock.status'=>1]],['$count'=>'books']]);
			$total_books_temp = $this->mongo_bookchor_library->temp_stock->aggregate([['$match'=>['library_id'=>$library_id]],['$count'=>'books']]);
			$library_data['total_books'] = iterator_to_array($total_books)[0]['books']+iterator_to_array($total_books_temp)[0]['books'];
			$library_data['product'] = (array) $search_products;
			return $library_data;
		}
		public function users_api($user_data){
			if(isset($user_data['roll_no'])){
				$filter = ['roll_no'=>$user_data['roll_no'],'library_id'=>$user_data['library_id']];
			}if(isset($user_data['emp_id'])){
				$filter = ['emp_id'=>$user_data['emp_id']];
			}
			$insert_data = ['date_added'=>$this->timestamp];
			$update_user_query = $this->mongo_bookchor_library->users->updateOne($filter,['$set'=>$user_data,'$setOnInsert'=>$insert_data],['upsert'=>true]);
			if(($update_user_query->getModifiedCount())||($update_user_query->getUpsertedCount())){
				$user_credentials = ['uid'=>$user_data['uid'],'name'=>$user_data['name'],'username'=>$user_data['username'],'password'=>$user_data['password']];
				//$user_credentials = [$user_data['uid'],$user_data['name'],$user_data['username'],$user_data['password']];
				return $user_credentials;
			}else{
				return 0;
			}
		}
		
		public function search_user($search,$library_id){
			if($search){
				$regex_search = new MongoDB\BSON\Regex($search);
				$search_query = $this->mongo_bookchor_library->users->find(['library_id'=> $library_id,'$or'=>[['name'=>['$regex'=>$search,'$options'=>'i']],['username'=> ['$regex'=>$search,'$options'=>'i']],['email'=>['$regex'=>$search,'$options'=>'i']],['roll_no'=>['$regex'=>$search,'$options'=>'i']]]],['projection'=>['name'=>1,'uid'=>1]]);
				$search_data  = iterator_to_array($search_query);
				return $search_data;
			}
		}
		public function book_data_sku(){
			$sku_list = ['AF1','AF2','BF4','BF6','BF5','CF1','CF3','CF6','DF6','EF2','EF4','EF5'];
			$books = [];
			$msg = '<table><tr><th>Title</th><th>Author</th><th>ISBN</th><th>MRP</th></tr>';
			foreach($sku_list as $skuid){
				$query_find = $this->mongo_bookchor_library->books->aggregate([['$match'=>['stock.sku'=>$skuid,'stock.status'=>1]],['$unwind'=>'$stock'],['$match'=>['stock.sku'=>$skuid,'stock.status'=>1]]]);
				$data_find = iterator_to_array($query_find);
				$pass = urlencode('Success^1218#');
				foreach($data_find as $find){
					$book = [];
					$strsearch = $find['isbn'];
					$url = "http://bcsolruser:".$pass."@school.bookchor.com:8983/solr/bookchor/select?indent=on&q=isbn:".$strsearch."&rows=1&wt=json";
					$json = file_get_contents($url);
					$data = json_decode($json);
					$responses = $data->response->docs;
					if($responses){
						foreach($responses as $response){
							$book['title'] = $response->title[0];
							$book['author'] = $response->author[0];
							$book['isbn'] = $response->isbn[0];
							$book['mrp'] = $response->mrp[0];
							$msg .= '<tr><td>'.$book['title'].'</td><td>'.$book['author'].'</td><td>'.$book['isbn'].'</td><td>'.$book['mrp'].'</td></tr>';
						}
					}
					//$books[] = $book;
				}
			}
			$msg .= '</table>';
			$file = fopen("skulist".$this->timestamp.".xls","w");
			fwrite($file,$msg);
			fclose($file);
			//return $books;
		}
		public function about_us($info){
			$this->mongo_bookchor_library->query_lead->insertOne($info);
		}
	}
?>