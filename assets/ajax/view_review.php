<?php
	if(isset($_REQUEST['pid'])){
		require_once '../../vendor/autoload.php';
		require_once('../../library_classes/Connection/Connection.php');
		require_once('../../library_classes/Utility/Utility.php');
		require_once('../../library_classes/User/User.php');
		require_once('../../library_classes/Product/Product.php');
		$user = new User();
		$product = new Product();
		$pid = $_REQUEST['pid'];
		$request = [
			'product_id'=>$_REQUEST['pid'],
			'library_id'=>$user->user['library_id'],
		];
		$reviews = $product->view_reviews($request);
		$request['user_id'] = $user->user['user_id'];
		$user_reviews = $product->view_reviews($request);
		echo json_encode(['user_review'=>(array) $user_reviews[0],'reviews'=>(array) $reviews]);
	}
?>