<?php
	if(isset($_REQUEST['pid'])){
		require_once '../../vendor/autoload.php';
		require_once('../../library_classes/Connection/Connection.php');
		require_once('../../library_classes/Utility/Utility.php');
		require_once('../../library_classes/User/User.php');
		require_once('../../library_classes/Product/Product.php');
		$user = new User();
		$product = new Product();
		$pid = $_REQUEST['pid'];
		$status  = $_REQUEST['status'];
		$wishlist = $product->add_wishlist($pid,$user->user['user_id'],$status);
		echo json_encode($wishlist);
	}
?>