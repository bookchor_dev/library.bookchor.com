<?php
	error_reporting(0);
	if(isset($_REQUEST['pid'])){
		require_once '../../vendor/autoload.php';
		require_once('../../library_classes/Connection/Connection.php');
		require_once('../../library_classes/Utility/Utility.php');
		require_once('../../library_classes/User/User.php');
		require_once('../../library_classes/Product/Product.php');
		$user = new User();
		$product = new Product();
		$pid = $_REQUEST['pid'];
		$library_id = $user->user['library_id'];
		$sku = $product->list_sku($pid,$library_id);
		echo json_encode($sku);
	}
?>