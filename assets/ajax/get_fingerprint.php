<?php
		require_once '../../vendor/autoload.php';
		require_once('../../library_classes/Connection/Connection.php');
		require_once('../../library_classes/Utility/Utility.php');
		require_once('../../library_classes/User/Login.php');
		$login = new Login();
		$hash = $login->get_fingerprint_hash($_REQUEST['username']);
		if($hash){
			$response = [
				'fp'=>1,
				'hash'=>$hash
			];
		
		}else{
			$response = [
				'fp'=>0
			];
		}
		echo json_encode($response);
?>