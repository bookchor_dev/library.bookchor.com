<?php
	require_once '../../vendor/autoload.php';
	require_once('../../library_classes/Connection/Connection.php');
	require_once('../../library_classes/Utility/Utility.php');
	require_once('../../library_classes/User/User.php');
	require_once('../../library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$categories = $product->category;
	$category_data = $_REQUEST['category'];
	$category_arr = [];
	foreach($categories as $category){
		if(in_array((string)$category['_id'],$category_data)){
			$category_arr[] = $category['_id'];
		}
	}
	$product->addCategoryUser($category_arr,$user->user);
?>