<?php
	if(isset($_REQUEST['pid'])){
		require_once '../../vendor/autoload.php';
		require_once('../../library_classes/Connection/Connection.php');
		require_once('../../library_classes/Utility/Utility.php');
		require_once('../../library_classes/User/User.php');
		require_once('../../library_classes/Product/Product.php');
		$user = new User();
		$product = new Product();
		$pid = $_REQUEST['pid'];
		$description = substr($product->product_description($pid),0,150).'...';
		
		$user_product_data = $product->library_product_info($user->user['user_id'],$pid);
		$user_product_data['description'] = $description;
		echo json_encode($user_product_data);
	}
?>