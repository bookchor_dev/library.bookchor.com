<?php
	if(isset($_REQUEST['review'])&&isset($_REQUEST['rating'])&&isset($_REQUEST['pid'])){
		require_once '../../vendor/autoload.php';
		require_once('../../library_classes/Connection/Connection.php');
		require_once('../../library_classes/Utility/Utility.php');
		require_once('../../library_classes/User/User.php');
		require_once('../../library_classes/Product/Product.php');
		$user = new User();
		$product = new Product();
		$review = $product->add_review($_REQUEST,$user->user);
		echo $review;
	}
?>