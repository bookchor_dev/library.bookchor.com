<?php
	require_once '../../vendor/autoload.php';
	require_once('../../library_classes/Connection/Connection.php');
	require_once('../../library_classes/Utility/Utility.php');
	require_once('../../library_classes/User/User.php');
	require_once('../../library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$categories = $product->category;
	$selected_category = $product->getSelectedcategory($user->user);
	$server = 'http://'.$_SERVER['SERVER_NAME'];
?>
<div class="modal fade" id="categoryModal">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h5 class="modal-title">Select your Favourite Genre</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form action="" id="category_form" method="post">
					<div class="row">
						<?php 
							foreach($categories as $category){
								$boolean = 'F';
								$active = '';
								if(isset($selected_category)){
									foreach($selected_category as $selected){
										if((string)$selected==(string)$category['_id']){
											$boolean = 'T';
											$active = 'bc-active';
										}
									}
								}
								if(isset($category['img_link'])){
						?>
						<div class="col-6 col-md-4 col-lg-3 p-1">
							<div class="square btn  p-0 position-relative content-margin d-block mx-auto rounded w-100" bc-category="<?php echo $category['_id'];?>" bc-boolean="<?php echo $boolean;?>">
								<img src="<?php echo $server.$category['img_link'];?>" class="img-fluid d-block mx-auto rounded img-square w-100 <?php echo $active;?>">
								<p class="mb-0 font-weight-bold position-absolute text-white category-title"><?php echo $category['category'];?></p>
								<img src="/assets/images/checkbox.svg" class="position-absolute checkbox_icon <?php echo $active;?>">
							</div>
						</div>
						<?php	 }}?>
						<div class="col-12 offset-md-3 col-md-6 mt-5">
							<button class="btn submit-button btn-block text-white rounded shadow-lg" type="submit" id="submit">Submit</button>
						</div>
					</div>
				</form>
			</div>
			<!-- Modal footer -->
			<!-- <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div> -->
		</div>
	</div>
</div>
<script src="/assets/js/recommend_category.js"></script>