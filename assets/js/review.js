$(document).ready(function() {
	var pid = $('.post-review').attr('data-pid');
	$.post("/assets/ajax/view_review.php",{pid:pid},function(data){
		if(data){
			var reviewdata = '';
			var response= JSON.parse(data);
			var reviews = response.reviews;
			for(var i=0;i<reviews.length;i++){
				reviewdata += '<div class="col"><div class="col-4"><button class="student_profile_logo float-left mt-0">'+reviews[i].name.charAt(0)+'</button></div><div class="col-8"><p class="font-weight-bold">'+reviews[i].name+' ('+reviews[i].rating+'&nbsp;<i class="bc-bc-star star_icon "></i>)</p><small>'+reviews[i].review+'</small></div></div>';
			}
			if(reviewdata){
				$('#review-div').empty();
				$('#review-div').toggleClass('row');
				$('#review-div').append(reviewdata);
				$('button[data-target="#reviewModal"]').text('Edit Your Review');
			}
			var user_review = response.user_review;
			var stars = $('li.star');
			for(var i=0;i<user_review.rating;i++){
				$(stars[i]).addClass('selected');
			}
			$('input[name=review]').val(user_review.review);
		}
	});
	$('.post-review').on('click',function(){
		var review = $('input[name=review]').val();
		var pid = $(this).attr('data-pid');
		if(review.length<100){
				 $('.error-msg-review').fadeOut(200);
				$.post("/assets/ajax/add_review.php",{rating:ratingValue,review:review,pid:pid},function(data){
					if(data){
						$('#reviewModal').modal('toggle');
					}
				});
		}else{
			 $('.error-msg-review').fadeIn(200);  
		}
	});

});