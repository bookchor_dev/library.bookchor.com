
	function logout(){
		$(".log-out").toggleClass("log-display");
	};
	$('.mob-search').click(function() {
		var elem = $(this).closest('div.header-div');
		if(elem.attr('bc-search-open')=='close'){
			elem.attr('bc-search-open','open');
			elem.find('div.logo-div').toggleClass('d-none');
			elem.find('div.search-mob-div').toggleClass('col-12 px-0');
			elem.find('div.search-mob-div').find('div.input-group-prepend').toggleClass('d-none');
			elem.find('div.account-div').toggleClass('d-none');
		}
	});
	$(document).click(function(e) {
		var container = $('div.header-div');
		if(container.attr('bc-search-open')=='open'){
			if (!container.is(e.target) && container.has(e.target).length === 0){
				container.attr('bc-search-open','close');
				container.find('div.logo-div').toggleClass('d-none');
				container.find('div.search-mob-div').toggleClass('col-12 px-0');
				container.find('div.search-mob-div').find('div.input-group-prepend').toggleClass('d-none');
				container.find('div.account-div').toggleClass('d-none');
			} 
		}
		
	});
	$('.bc-options').click(function(e) {
		var option = $(this).text();
		var ebook = $(this).attr('href').substr(1);
		$('input[name=ebook]').val(ebook);
		$('button.search-type').text(option);
	});
	
	/********************Recommend Category************************/
	function selectCategory(){
		$.post("/assets/ajax/recommend_category.php",function(data){
			$('body').append(data);
			$("#categoryModal").modal();
		});
	}
$(document).ready(function() {			
	if(category_flag==1){
		selectCategory();
	}
});	