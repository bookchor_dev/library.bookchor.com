var categoryArray = new Array();
$(document).ready(function(){
    $('div.square').click(function(){
        if($(this).find(".img-square").hasClass("bc-active")){
            $(this).find(".checkbox_icon").removeClass("bc-active");
            $(this).find(".img-square").removeClass("bc-active");
            $(this).attr("bc-boolean","F");
        }
        else{
            $(this).find(".checkbox_icon").addClass("bc-active");
            $(this).find(".img-square").addClass("bc-active");
            $(this).attr("bc-boolean","T");
        }
    });
    $("#category_form").submit(function(event){
        event.preventDefault();
		$('.overlay').addClass('test3');
        $('div.square').each(function() {
            var category_flag = $(this).attr('bc-boolean');
            if(category_flag=="T"){
                var category = $(this).attr("bc-category");
                categoryArray.push(category);
                //console.log(categoryArray);
            }
        });
		$.post("/assets/ajax/select-category.php",{category:categoryArray},function(data){
			$("#categoryModal").modal('toggle');
			window.location = '/recommended-books';
		});
    });
});
