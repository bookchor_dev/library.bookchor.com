wish_ajax_check = true;
$(document).ready(function() {
	$('[bc-href]').click(function(){ var location = $(this).attr('bc-href'); window.open(location,'_blank'); });
	$("img").on('error',function(){
		$(this).attr('src','https://www.bookchor.com/images/cover.jpg');
	});
	/* $(document).click(function(e){
		$(".test3").each(function( index, element) {
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0){
				$(this).toggleClass('test3');
			}
		});
		 
	}); */
	$('.product_user').each(function( index, element) {
		var pid = $(this).attr('data-pid');
		var ebook_url = $(this).attr('url-ebook');
		var ebook  = '';
		$.post("/assets/ajax/get_product_info.php",{pid:pid},function(data){
			var data_pro = JSON.parse(data);
			//alert(data_pro.wishlist);
			if(data_pro.wishlist!=1){
				var wishlist_info = '<button class="btn add-to-wishlist" data-pid="'+pid+'">Add to Wishlist&nbsp;&nbsp;<span><i class="fas fa-heart"></i></span></button>';
				}else{
				var wishlist_info = '<button class="btn add-to-wishlist btn-danger" data-pid="'+pid+'">Added to Wishlist&nbsp;&nbsp;<span><i class="fas fa-heart"></i></span></button>';
			}
			if(ebook_url!='0'){
				ebook = '<a class="ml-3 btn btn-info" target="_blank" href="https://library.bookchor.com/web/viewer.html?file=/'+ebook_url+'">View E-book<span><i class="fas fa-eye"></i></span></a>';
			}
			$(element).empty();
			$(element).append('<small>'+data_pro.description+'</small><br><br>'+wishlist_info+ebook);
		});
	});
	$('.product_user_wishlist').each(function( index, element) {
		var pid = $(this).attr('data-pid');
		$.post("/assets/ajax/get_product_category.php",{pid:pid},function(data){
			var data_pro = JSON.parse(data);
			var cat_data = '';
			var data_cat = data_pro.category;
			for(var i=0;i<data_cat.length;i++){
				cat_data += '<a class="btn btn-'+data_cat[i].color+' rounded ml-1 p-2 " style="font-size: 11px;" href="/category/'+data_cat[i]._id.$oid+'/'+data_cat[i].category+'">'+data_cat[i].category+'</a>';	
			}
			$(element).empty();
			$(element).append('<small>'+data_pro.description+'</small><br><br>');
			$(element).append('<div class="row">'+cat_data+'</div>');
		});
	});
	$(".find").click(function() {
		$('.test3').removeClass("test3");
		var elem = $(this).closest("div.card").find("div.book-availability");
		elem.toggleClass("test3");
		var elem1 = elem.find('div.grid4');
		var skulist = '';
		if((!elem1.attr('bc-sku-load'))===true){
			var pid = elem1.attr('data-pid');
			$.post("/assets/ajax/get_sku.php",{pid:pid},function(data){
				var data_sku = JSON.parse(data);
				for(var i=0;i<data_sku.length;i++){
					skulist += '<button class="btn1 font-weight-bold" >'+data_sku[i]+'</button>';
				}
				elem1.empty();
				elem1.attr('bc-sku-load',true);
				elem1.append(skulist);
			});
		}
	});
	$('.delwishlist').click(function(){
		var delelem = $(this);
		var pid = delelem.closest('div.card-body').find('[data-pid]').attr('data-pid');
		status = '-1';
		$('.overlay').addClass('test3');
		$.post("/assets/ajax/wishlist.php",{pid:pid,status:status},function(data){
			$('.overlay').removeClass('test3');
			delelem.closest('div.wishlist-div').remove();
		});
	});
	$(".issue").click(function() {
		$('.test3').removeClass("test3");
		var elem = $(this).closest("div.card").find("div.bar-code");
		elem.toggleClass("test3");
	});
	$('.barcode-input').on('keyup',function(e){
		var barcode = $(this).val();
		if(barcode.length==8){
			alert('Library Authentication Required');
		}
	});
	$(document).on('click',".add-to-wishlist",function() {
		if(wish_ajax_check==true){
			$('.overlay').addClass('test3');
			wish_ajax_check=false;
			var pid = $(this).attr('data-pid');
			var status_flag = $(this).hasClass('btn-danger');
			if(status_flag){
				var status = '-1';	
			}else{
				var status = '1';
			}
			var elem_wish = $(this);
			$.post("/assets/ajax/wishlist.php",{pid:pid,status:status},function(data){
				elem_wish.text('Added to Wishlist');
				elem_wish.toggleClass("btn-danger");
				wish_ajax_check = true;
				$('.overlay').removeClass('test3');
			});
		}
	});
	$(".owl-carousel").owlCarousel({
        loop: true,
        addClassActive: true,
        margin: 10,
        responsiveClass: true,
        autoplay: true,
        nav: true,
		rewind:true,
        navText: ["<i class='bc-bc-left-arrow-1 black_color_type'></i>",
        "<i class='bc-bc-right-arrow black_color_type'></i>"],
        smartSpeed :900,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            600: {
                items: 5,
                nav: false
            },
            1000: {
                items: 6,
                nav: true,
                loop: false
            }
        },
    });
    $(".play").click(function() {
        owl.trigger("owl.play", 2000); //owl.play event accept autoPlay speed as second parameter
    });
	
});
