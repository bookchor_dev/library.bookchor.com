<?php
	
	require_once 'vendor/autoload.php';
	require_once('library_classes/Connection/Connection.php');
	require_once('library_classes/Utility/Utility.php');
	require_once('library_classes/User/User.php');
	require_once('library_classes/User/Login.php');
	$user = new User();
	if(isset($_REQUEST['credentials'])){
		$login = new Login();
		$login->login($_REQUEST['credentials'],$user->token);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
		<link rel="stylesheet" href="/assets/css/login.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row ">
				<div class="col-md-6 col-lg-7 col-xl-7">
					<img src="/assets/images/Login-Desktop(new).svg" class="display_img mx-auto img-fluid w-100" alt="logo">
					<h5 class="title">MY SMART LIBRARY</h5>
				</div>
				<div class=" col-md-6 col-lg-5 col-xl-5 ">
					<div class="mobile">
						<img src="/assets/images/Tab_iMAGE.svg" class="tab_img mx-auto img-fluid" alt="logo" style="width:100%;">
					</div>
					<div class="main-content">
						<div class="card ">
							<div class="card-body">
								<div class="cotainer">
									<div class="mt-3 mb-3 toggle_option">
										<ul class="nav nav-pills" role="tablist">
											<li class="nav-item ml-0">
												<a class="nav-link active" data-toggle="pill" href="#signin_form">Login</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="pill" href="#signup_form">Signup</a>
											</li>
										</ul>
									</div>
									<div class="tab-content">
										<div class="container tab-pane active" id="signin_form">
											<form class="form" method="POST">
												<div class="form-group">
													<label for="exampleInputEmail1">Username</label>
													<input type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name='credentials[username]' placeholder="Enter username...">
												</div>
												<br>
												<div class="form-group">
													<label for="exampleInputPassword1">Password</label>
													<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name='credentials[password]'>
													<a href="#">
														<small id="emailHelp" class="form-text ">FORGET PASSWORD ?</small>
													</a>
												</div>
												<br>
												<div class="text-center">
													<button type="submit" class="btn ">Login</button>
												</div>
												<br>
											</form>
										</div>
										<div class="container tab-pane" id="signup_form">
											<form class="form" name="sign-up">
												<div class="form-group mt-5">
													<label for="exampleInputusername">Username</label>
													<input type="name" class="form-control input_inline" id="exampleInputusername" name="user_name" aria-describedby="emailHelp" placeholder="Enter username...">
												</div>
												<br />
												<div class="form-group mt-5">
													<img src="/assets/images/Touch_ID.png" alt="Touc ID" name="fingerprint" width="48" height="48" id="exampleInputfid" class="fingerprint" accept="image/*" onclick="Match();">
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<p class="align-center">Powered by Bookchor</p>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/popper.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/login.js"></script>
	
	
	
	
	
	
	
	<!-- <script src="" async defer></script> -->
	
	<script src="/assets/js/jquery.js"></script>
	<script src="mfs100-9.0.2.6.js"></script>
	<script>
		var isotemplate = '';
		var ajaxcheck = true;
		$('#exampleInputusername').on('keyup',function(){
			var username = $('#exampleInputusername').val();
			if((username.length>2)&&(ajaxcheck==true)){
				ajaxcheck = false;
				$.post("/assets/ajax/get_fingerprint.php",{username:username},function(data){
					ajaxcheck = true;
					isotemplate = data;
					Match();
				});
			}
		});
		
		function Match() {
			var quality = 60; //(1 to 100) (recommanded minimum 55)
			var timeout = 60; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
			try {
				var res = MatchFinger(quality, timeout, isotemplate);
				
				if (res.httpStaus) {
					if (res.data.Status) {
						alert("Finger matched");
					}
					else {
						if (res.data.ErrorCode != "0") {
							alert(res.data.ErrorDescription);
						}
						else {
							alert("Finger not matched");
						}
					}
				}
				else {
					alert(res.err);
				}
			}
			catch (e) {
				alert(e);
			}
			return false;
			
		}
	</script>
</body>

</html>