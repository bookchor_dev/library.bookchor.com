<?php
	if(isset($_REQUEST['school-submit'])){
		include 'library_classes/Mailer/Mailer.php';
		require('vendor/autoload.php');
		require('library_classes/Connection/Connection.php');
		require('library_classes/Library/Library.php');
		$library = new Library();
		$contact = ['name'=>$_REQUEST['name'],'phone'=>$_REQUEST['phone'],'message'=>$_REQUEST['message'],'email'=>$_REQUEST['email']];
		$library->about_us($contact);
		/* $mail = new Mailer();
		$mail->mailContactUs($contact); */
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bookchor - Smart Library</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://assets.bookchor.in/global/vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/about-smart-lib/bookchor_icons/styles.css">
    <link rel="stylesheet" href="assets/about-smart-lib/css/color.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins'>
    <link rel="stylesheet" href="assets/about-smart-lib/css/home.css">
</head>
<body>
    <div class="container-fluid outer mb-5">
        <div class="row addon mb-2 px-2">
			<div class="col-12">
			<a href="/login"><img class="img-fluid d-none d-sm-block float-right position-relative login-img-addon" src="assets/about-smart-lib/images/LOGIN.png" alt="img"></a>
			</div>
            <div class="col-12 col-sm-6 align-self-center">
                    <!-- New Addition -->
                    <div class="row my-3">
                        <div class="col-8">
                            <img class="img-fluid mb-1" src="assets/about-smart-lib/images/bc-logo.png" alt="img">
                        </div>
                        <div class="col-4">
                            <a href="/login"><img class="img-fluid d-sm-none float-right position-absolute login-mob-addon" src="assets/about-smart-lib/images/LOGIN.png" alt="img"></a>
                        </div>
                    </div>
                    <h1 class="my-4 text-uppercase text-white font-weight-bold ">smart library</h1>
                    <p class="text-white">
						Proven solution to making your library more <br>
						efficient and inculcate quality reading <br>
						habits amongst your students.
					</p>
                    <button class="btn btn-sm btn-addon text-uppercase dark-yellow text-white float-left  mb-2" data-toggle="modal" data-target="#exampleModalCenter">get in touch</button>
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <button type="button" class="close close-btn-addon" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="small bc-bc-close"></i></span>
                            </button>
                            <img class="img-fluid mx-auto d-block popup_img" src="assets/about-smart-lib/images/popup-img.png" alt="img">
                            <div class="modal-content modal-addon">
                                <div class="px-3 py-3 border-bottom-0 text-center">
                                    <h5 class="text-uppercase mb-0">enter your details</h5>
                                </div>
								<form method="POST">
									<div class="modal-form px-4 py-2">
											<div class="form-group mb-0">
												<input type="text" class="form-control input-field mb-2 " name="name" id="org_name"  placeholder="Organisation Name">
												<input type="email" class="form-control input-field mb-2 " name="email" id="email_id"  placeholder="Email">
												<input type="text" class="form-control input-field mb-2 " name="phone" id="ph_no"  placeholder="Contact Number">
												<textarea rows="4" class="form-control input-field " name="message" id="userr_query"  placeholder="Your Query"></textarea>
											</div>
										
									</div>
									<div class="px-3 py-3 border-top-0 text-center">
										<button type="submit" name="school-submit" class="btn dark-yellow px-5 text-white submit-btn-addon">Submit</button>
									</div>
								</form>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="col-12 col-sm-6 pt-2">
                <img class="img-fluid mx-auto d-block position-relative banner-img-addon" src="assets/about-smart-lib/images/banner.png" alt="img" style="height:350px;">
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-3 d-none d-sm-block">
                <div class="card card-addon">
                    <div class="card-body">
                        <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/header_img/001.png" alt="img" style="height:200px">
                        <p class="mb-0 text-center">Advanced Library Management System</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-3 d-none d-sm-block">
                <div class="card card-addon">
                    <div class="card-body">
                        <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/header_img/002.png" alt="img" style="height:200px">
                        <p class="mb-0 text-center">Intuitive Students' Library Interface</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-3 d-none d-sm-block">
                <div class="card card-addon">
                    <div class="card-body">
                        <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/header_img/003.png" alt="img" style="height:200px">
                        <p class="mb-0 text-center">State-of-the-art Devices</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 py-3 d-none d-sm-block">
                <div class="card card-addon">
                    <div class="card-body">
                        <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/header_img/004.png" alt="img" style="height:200px">
                        <p class="mb-0 text-center">Reading & Purchase Recommendations</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="zig-zag-top"></div>
    <div class="container-fluid outer">
        <div class="row pt-2 gradient-addon">
            <div class="col-12 col-sm-6 py-3 pl-4 align-self-center">
                <img class="img-fluid my-3" src="assets/about-smart-lib/images/circle_green.png" alt="img">
                <p class="h4 font-weight-bold ">Self Issue / Return</p>
                <p >A smarter way for students to interact with their School Library, powered by Digital Infrastructure (Thumbprint Scanners, Kiosks, Touch Panels, Kindles, etc.)</p>
                <button class="btn btn-sm text-uppercase text-white pull-left dark-yellow" data-toggle="modal" data-target="#exampleModalCenter">learn more</button>
            </div>
            <div class="col-12 col-sm-6">
                <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/self_issue.png" alt="img">
            </div>
        </div>
        <!-- Row ends -->
        <div class="row py-3 row-addon">
            <div class="col-12 col-sm-6 py-3">
                <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/central_elibrary.png" alt="img">
            </div>
            <div class="col-12 col-sm-6 py-2 pl-4 align-self-center">
                <img class="img-fluid mb-3" src="assets/about-smart-lib/images/circle_red.png" alt="img">
                <p class="h4 font-weight-bold">Central E-Library</p>
                <p class="mb-1 font-weight-bold">One world. Many Stories.</p>
                <p >Remote access to the hand-picked collection of trending novels, legendary classics, updated text and reference</p>
                <button class="btn btn-sm text-uppercase text-white pull-left dark-yellow" data-toggle="modal" data-target="#exampleModalCenter">learn more</button>
            </div>
        </div>
    </div>
    <div class="zig-zag-top"></div>
    <div class="container-fluid outer">
        <div class="row pt-3 gradient-addon">
            <div class="col-12 col-sm-6  pl-4 align-self-center">
                <img class="img-fluid my-3" src="assets/about-smart-lib/images/circle_red.png" alt="img">
                <p class="h4 font-weight-bold ">Virtual Reader's Club</p>
                <p class="mb-1 font-weight-bold">Get onboard and Read! </p>
                <p >Give your students the unique opportunity of being a part of something bigger. They receive recommendations on the basis of their reading interests and hence compete for rewards with other school students to become the ultimate reader.</p>
                <button class="btn btn-sm text-uppercase text-white pull-left dark-yellow" data-toggle="modal" data-target="#exampleModalCenter">learn more</button>
            </div>
            <div class="col-12 col-sm-6">
                <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/virtual_reader.png" alt="img">
            </div>
        </div>
        <!-- Row ends -->
        <div class="row py-3 row-addon">
            <div class="col-12 col-sm-6 py-3">
                <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/lms.png" alt="img">
            </div>
            <div class="col-12 col-sm-6 py-2 pl-4 align-self-center">
                <img class="img-fluid my-3" src="assets/about-smart-lib/images/circle_green.png" alt="img">
                <p class="h4 font-weight-bold">Library Management System</p>
                <p class="mb-1">- Librarian’s Control Panel </p>
                <p class="mb-1">- Advanced level controls on inventory </p>
                <p class="mb-1">- On-system and SMS notifications </p>
                <p class="mb-1">- Performance, defaulters and other analytical reports</p>

                <button class="btn btn-sm text-uppercase text-white pull-left dark-yellow mt-1" data-toggle="modal" data-target="#exampleModalCenter">learn more</button>
            </div>
        </div>
    </div>
    <div class="zig-zag-top"></div>
    <div class="container-fluid outer">
        <div class="row pt-3 gradient-addon">
            <div class="col-12 col-sm-6 py-3 pl-4">
                <img class="img-fluid my-3" src="assets/about-smart-lib/images/circle_green.png" alt="img">
                <p class="h4 font-weight-bold ">Supplies & Rotation</p>
                <p class="mb-1">- Make 100% use of your shelf spaces with our analytics based recommendations for periodic purchases </p>
                <p class="mb-1">- Sell your old inventory to us and get benefits on new books </p>
                <p class="mb-1">- Keep your library always up to date with the perfect combination of must-read and trending books</p>
                <button class="btn btn-sm text-uppercase text-white pull-left dark-yellow" data-toggle="modal" data-target="#exampleModalCenter">learn more</button>
            </div>
            <div class="col-12 col-sm-6 py-2 align-self-center">
                <img class="img-fluid mx-auto d-block mt-2" src="assets/about-smart-lib/images/supplies.png" alt="img">
            </div>
        </div>
        <div class="container">
            <div class="row py-2">
                <div class="col-12 text-center py-2">
                    <p class="h4 text-uppercase font-weight-bold">about us</p>
                    <p>BookChor is a leading seller of pre-loved (pre-owned) books in India, with presence
					over android, iOS and Web. Our ever growing reader base already spans over 400,000
					from all over the nation. We are also distinguished by our offline presence with more
					than 550 events to our credit.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-5 align-self-center pl-4">
                <p class="mb-2 font-weight-bold">Introducing Bookchor</p>
                <p class="mb-2">We believe that the sweet smell of paper and the battle marks of paper cuts make us more human, bit by bit. Each letter adding another layer to our personalities. We source our inspiration from our mission to make reading more democratic and knowledge more accessible and we will never stop in this beautiful journey that we share with our readers.</p>
            </div>
            <div class="col-12 col-sm-2 py-1 text-center">
                <img class="img-fluid mx-auto d-block" src="assets/about-smart-lib/images/bc-man.png" alt="img">
            </div>
            <div class="col-12 col-sm-5 align-self-center pl-4">
                <p class="mb-2 font-weight-bold">What we do</p>
                <p class="mb-2">With our services spanning over both internet and the actual physical world, we offer a range of innovative literary solutions. From mobile and web based E-commerce Platforms to Mega Book Fairs. From Smart Libraries in schools to customized Book Shelves for Corporate Offices. Even marking our presence in your favorite Cafe and that fancy hotel you love. We are EVERYWHERE!</p>
            </div>
        </div>
    </div>
    <!-- Container-fluid ends -->
</div>
</div>
<div class="zig-zag-top"></div>
<div class="section gradient-addon">
    <div class="container">
        <div class="row py-3 pt-2">
            <div class="col-12 text-center py-2">
                <p class="h4 text-uppercase font-weight-bold">how it works</p>
                <p>Proven solution to making your library more efficient and inculcate quality reading habits amongst your students.</p>
            </div>
            <div class="col-12 col-sm-4 py-2 text-center">
                <img class="img-fluid mb-3" src="assets/about-smart-lib/images/connect_device.png" alt="img">
                <p class="mb-2 font-weight-bold">Connect Device</p>
            </div>
            <div class="col-12 col-sm-4 py-2 text-center">
                <img class="img-fluid mb-3" src="assets/about-smart-lib/images/configure.png" alt="img">
                <p class="mb-2 font-weight-bold">Configure It</p>
            </div>
            <div class="col-12 col-sm-4 py-2 text-center">
                <img class="img-fluid mb-3" src="assets/about-smart-lib/images/successful.png" alt="img">
                <p class="mb-2 font-weight-bold">Yay! Done</p>
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://assets.bookchor.in/global/vendor/bootstrap-4/js/jquery.js?v=<?php echo $plugin_cache;?>"></script>
<script src="https://assets.bookchor.in/global/vendor/bootstrap-4/js/popper.js?v=<?php echo $plugin_cache;?>"></script>
<script src="https://assets.bookchor.in/global/vendor/bootstrap-4/js/bootstrap.min.js?v=<?php echo $plugin_cache;?>"></script>
</html>
