<?php
	require_once 'vendor/autoload.php';
	require_once('library_classes/Connection/Connection.php');
	require_once('library_classes/Utility/Utility.php');
	require_once('library_classes/User/User.php');
	require_once('library_classes/Product/Product.php');
	$user = new User();
	$product = new Product();
	$user_info = [
	'user_id' =>$user->user['user_id'],
	'library_id'=>$user->user['library_id']
	];
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Library @ Bookchor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" >
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/OwlCarousel2/dist/assets/owl.carousel.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/OwlCarousel2/dist/assets/owl.theme.default.min.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/header.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/footer.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/bookchor_icons/styles.css?v=<?php echo $user->user['cache'];?>">
		<link rel="stylesheet" href="/assets/css/landing_page.css?v=<?php echo $user->user['cache'];?>">
	</head>
	<body>
		<?php include "header.php";?>
		<div class="container-fluid">
			<section>
				<div id="demo" class="carousel " data-ride="carousel">
					<div class="carousel-item active">
						<img src="/assets/images/web banner 1.png" class="img-fluid w3-animate-fading bc-full banner_image" alt="Artful Storytelling"> 
					</div>
					<div class="carousel-item">
						<img src="/assets/images/web banner 2.png" class="img-fluid w3-animate-fading bc-full banner_image" alt="Artful Storytelling">
					</div>
				</div>
			</section>
			<section>
				<!-- <div class="container-fluid"> -->
				<div class="card">
					<div class="card-body books">
						<p class="font-weight-bold float-left title1">
						Trending Books</p>
					</div>
				</div>
				<div id="owl-demo" class="owl-carousel owl-theme">
					<?php 
						$doc_info = [
							'column_id'=>7,
							'limit'=>10,
						];
						$productlists = $product->book_div($user_info,$doc_info);	
						foreach($productlists as $productlist){
							if(strlen($productlist['details']['title'])>20){
								$title = substr($productlist['details']['title'],0,20).'...';
							}else{
								$title = $productlist['details']['title'];
							}	
						?>
						<div class="item">
							<div>
								<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>">
									<img src="<?php echo $productlist['details']['image'];?>" alt="<?php echo $productlist['details']['title'];?>" class="img-fluid mx-auto round" style="height:300px;">
								</a>
								<div class="card-body slide-content p-0">
									<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>" title="<?php echo $productlist['details']['title'];?>">
										<p class="font-weight-bold book-title"><?php echo $title;?></p>
									</a>
									<p class="text-muted " title="<?php echo $productlist['details']['author'];?>">by <?php echo substr($productlist['details']['author'],0,15).'..';?></p>
									<div class="stars">
										<?php for($i=0;$i<$productlist['details']['goodreads_avg_rating'];$i++){?>
											<i class="bc-bc-star star_icon"></i>&nbsp;
										<?php } ?>
										<small class="font-weight-bold">(<?php echo $productlist['details']['goodreads_total_reviews'];?>)</small>
										</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<!-- </div> -->
			</section>
			<section>
				<!-- <div class="container-fluid"> -->
				<div class="card">
					<div class="card-body books">
						<p class="font-weight-bold float-left title1">
						Recently Viewed Books</p>
					</div>
				</div>
				<div id="owl-demo" class="owl-carousel owl-theme">
					<?php 
						$doc_info = [
							'column_id'=>6,
							'limit'=>10,
						];
						$productlists = $product->book_div($user_info,$doc_info);	
						foreach($productlists as $productlist){
							if(strlen($productlist['details']['title'])>20){
								$title = substr($productlist['details']['title'],0,20).'...';
							}else{
								$title = $productlist['details']['title'];
							}	
						?>
						<div class="item">
							<div>
								<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>">
									<img src="<?php echo $productlist['details']['image'];?>" alt="<?php echo $productlist['details']['title'];?>" class="img-fluid mx-auto round" style="height:300px;">
								</a>
								<div class="card-body slide-content p-0">
									<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>" title="<?php echo $productlist['details']['title'];?>">
										<p class="font-weight-bold book-title"><?php echo $title;?></p>
									</a>
									<p class="text-muted " title="<?php echo $productlist['details']['author'];?>">by <?php echo substr($productlist['details']['author'],0,15).'..';?></p>
									<div class="stars">
										<?php for($i=0;$i<$productlist['details']['goodreads_avg_rating'];$i++){?>
											<i class="bc-bc-star star_icon"></i>&nbsp;
										<?php } ?>
										<small class="font-weight-bold">(<?php echo $productlist['details']['goodreads_total_reviews'];?>)</small>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<!-- </div> -->
			</section>
			<section>
				<!-- <div class="container-fluid"> -->
				<div class="card">
					<div class="card-body books">
						<p class="font-weight-bold float-left title2">
						Most Liked Books</p>
						<!--<button class="btn1 float-right"><a href="/wishlist">More</a></button>-->
					</div>
				</div>
				<div id="owl-demo" class="owl-carousel owl-theme">
					<?php 
						$doc_info = [
						'column_id'=>1,
						'limit'=>10,
						];
						$productlists = $product->book_div($user_info,$doc_info);	
						foreach($productlists as $productlist){
							if(strlen($productlist['details']['title'])>20){
								$title = substr($productlist['details']['title'],0,20).'...';
							}else{
								$title = $productlist['details']['title'];
							}	
						?>
						<div class="item">
							<div>
								<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>"><img src="<?php echo $productlist['details']['image'];?>" alt="<?php echo $productlist['details']['title'];?>" class="img-fluid mx-auto round" style="height:300px;"></a>
								<div class="card-body slide-content p-0">
									<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>" title="<?php echo $productlist['details']['title'];?>">
										<p class="font-weight-bold book-title"><?php echo $title;?></p>
									</a>
									<p class="text-muted " title="<?php echo $productlist['details']['author'];?>">by <?php echo substr($productlist['details']['author'],0,15).'..';?></p>
									<div class="stars">
										<?php for($i=0;$i<$productlist['details']['goodreads_avg_rating'];$i++){?>
											<i class="bc-bc-star star_icon"></i>&nbsp;
										<?php } ?>
										<small class="font-weight-bold">(<?php echo $productlist['details']['goodreads_total_reviews'];?>)</small>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<!-- </div> -->
			</section>
			
			<section>
				<!-- <div class="container-fluid"> -->
				<div class="card">
					<div class="card-body books">
						<p class="font-weight-bold float-left title2">
						Your Wishlist</p>
						<!--<button class="btn1 float-right"><a href="/wishlist">More</a></button>-->
					</div>
				</div>
				<div id="owl-demo" class="owl-carousel owl-theme">
					<?php 
						$doc_info = [
						'column_id'=>8,
						'limit'=>10,
						];
						$productlists = $product->book_div($user_info,$doc_info);	
						foreach($productlists as $productlist){
							if(strlen($productlist['details']['title'])>20){
								$title = substr($productlist['details']['title'],0,20).'...';
							}else{
								$title = $productlist['details']['title'];
							}	
						?>
						<div class="item">
							<div>
								<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>"><img src="<?php echo $productlist['details']['image'];?>" alt="<?php echo $productlist['details']['title'];?>" class="img-fluid mx-auto round" style="height:300px;"></a>
								<div class="card-body slide-content p-0">
									<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>" title="<?php echo $productlist['details']['title'];?>">
										<p class="font-weight-bold book-title"><?php echo $title;?></p>
									</a>
									<p class="text-muted " title="<?php echo $productlist['details']['author'];?>">by <?php echo substr($productlist['details']['author'],0,15).'..';?></p>
									<div class="stars">
										<?php for($i=0;$i<$productlist['details']['goodreads_avg_rating'];$i++){?>
											<i class="bc-bc-star star_icon"></i>&nbsp;
										<?php } ?>
										<small class="font-weight-bold">(<?php echo $productlist['details']['goodreads_total_reviews'];?>)</small>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<!-- </div> -->
			</section>
			<section>
				<!-- <div class="container-fluid"> -->
				<div class="card">
					<div class="card-body books">
						<p class="font-weight-bold float-left title2">
						Most Viewed Books</p>
					</div>
				</div>
				<div id="owl-demo" class="owl-carousel owl-theme">
					<?php 
						$doc_info = [
						'column_id'=>5,
						'limit'=>10,
						];
						$productlists = $product->book_div($user_info,$doc_info);	
						foreach($productlists as $productlist){
							if(strlen($productlist['details']['title'])>20){
								$title = substr($productlist['details']['title'],0,20).'...';
							}else{
								$title = $productlist['details']['title'];
							}	
						?>
						<div class="item">
							<div>
								<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>"><img src="<?php echo $productlist['details']['image'];?>" alt="<?php echo $productlist['details']['title'];?>" class="img-fluid mx-auto round" style="height:300px;"></a>
								<div class="card-body slide-content p-0">
									<a href="/book/<?php echo $productlist['details']['isbn'];?>/<?php echo str_replace(' ','-',$productlist['details']['title']);?>" title="<?php echo $productlist['details']['title'];?>">
										<p class="font-weight-bold book-title"><?php echo $title;?></p>
									</a>
									<p class="text-muted " title="<?php echo $productlist['details']['author'];?>">by <?php echo substr($productlist['details']['author'],0,15).'..';?></p>
									<div class="stars">
										<?php for($i=0;$i<$productlist['details']['goodreads_avg_rating'];$i++){?>
											<i class="bc-bc-star star_icon"></i>&nbsp;
										<?php } ?>
										<small class="font-weight-bold">(<?php echo $productlist['details']['goodreads_total_reviews'];?>)</small>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<!-- </div> -->
			</section>
			<!--
			<section class="author-content">
				<p class="font-weight-bold author_heading align-middle">AUTHOR OF THE MONTH</p>
				<div class="row">
					<div class="col-sm-8 col-md-6 student-of-month">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-6"><img src="/assets/images/avatar4.jpg" class="img-fluid img-height" alt="logo"></div>
									<div class="col-6">
										<div class="row">
											<span class="float-left card_content">Book Name:<span class="card_content_data">Out of the Box</span></span>
										</div>
										<div class="row">
											<span class="float-left card_content">Author Name:<span class="card_content_data">JOHN DOE</span></span>
										</div>
										<div class="row">
											<span class="float-left card_content">Description:</span>
										</div>
										<div class="row">
											<span class="card_content_data">Lorem ipsumdolor sit amet consectetur adipisicing elit</span>
										</div>
									</div>
								</div>
								<div class="row float-right mt-3">
									<a href="#" class=" view-more-link" style=" ">View More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="reader-content">
				<div class="card">
					<div class="card-body books">
						<p class="font-weight-bold float-left title3">
						Club Reader</p>
                        <button class="btn1 float-right"><a href="#">More</a></button>
					</div>
				</div>
                <div id="owl-test" class="owl-carousel owl-theme">
                    <div class="item club-items">
                        <img src="/assets/images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 literature">Literature</button>
                        <p class="font-weight-bold grp-name" >
                            Group: A
						</p>
					</div>
                    <div class="item club-items">
                        <img src="/assets/images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 sci-fi" >Literature</button>
                        <p class="font-weight-bold grp-name" >Group: A</p>
					</div>
                    <div class="item club-items">
                        <img src="/assets/images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 romance" >Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
					</div>
                    <div class="item club-items">
                        <img src="/assets/images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 crime" >Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
					</div>
                    <div class="item club-items">
                        <img src="/assets/images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 sci-fi">Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
					</div>
                    <div class="item club-items">
                        <img src="/assets/images/img_avatar3.png" alt="logo" class="img-fluid club-image">
                        <button class="btn3 crime" >Literature</button>
                        <p class="font-weight-bold grp-name">Group: A</p>
					</div>
				</div>
			</section>
			<section class="last-content">
				<h6 class="font-weight-bold align-center title5">STUDENT OF THE MONTH</h6>
                <div class="row">
                    <div class="col-sm-8 col-md-6 student-of-month">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6"><img src="/assets/images/avatar4.jpg" class="img-fluid img-height" alt="logo"></div>
                                    <div class="col-6">
                                        <div class="row">
                                            <span class="float-left card_content">Student Name:<span class="card_content_data">John Smith</span></span>
										</div>
                                        <div class="row">
                                            <span class="float-left card_content">Read Books:<span class="card_content_data">145</span></span>
										</div>
                                        <div class="row">
                                            <span class="float-left card_content">Favourite Categories:</span>
										</div>
                                        <div class="row">
                                            <button class="category_button literature rounded">Literature</button>
                                            <button class="category_button romance rounded">Romance</button>
                                            <button class="category_button youth-age rounded">Youth Age</button>
                                            <button class="category_button crime rounded">Crime</button>
                                            <button class="category_button sci-fi rounded">Sci-Fi</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>-->
		</div>
		<?php include "footer.php";?>
	</body>
	<script src="/assets/js/jquery.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/popper.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/bootstrap.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/OwlCarousel2/dist/owl.carousel.min.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/main.js?v=<?php echo $user->user['cache'];?>"></script>
	<script src="/assets/js/header.js?v=<?php echo $user->user['cache'];?>"></script>
</html>
